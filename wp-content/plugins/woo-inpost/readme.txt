=== InPost for WooCommerce ===
Contributors: inspirelabs
Tags: inpost, woocommerce, woocommerce shipping, easypack
Requires at least: 4.1
Tested up to: 5.4
Stable tag: 1.3.6
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

This free InPost extension integrates WooCommerce store with the InPost system - create and generate parcels directly from your e-store.

== Description ==

InPost for WooCommerce plugin was developed for small and medium sized businesses which are looking for an easy way to integrate with InPost services.

Once installed the plugin allows the configuration of any services provided by InPost available in the target market. In Canada, InPost enables within WooCommerce e-store a new delivery method - InPost Lockers 24/7.

After installation of the plugin you need to configure the InPost Locker 24/7 delivery method and configure the pricing. In the e-store checkout, the user will be able to select InPost Lockers 24/7 as a delivery method for their order.  Additionally, the user will be able to specify the destination locker that their order will be delivered to.

After the order placement process, you will be able to manage the shipment through the administration panel. The plugin allows you to edit the data needed to create the shipment and generate an InPost label.

You will find more information in the [English User Guide](https://inpost.pl/file/180), [Polish User Guide](https://inpost.pl/file/177), [French User Guide](https://inpost.pl/file/179) and [Italian User Guide](https://inpost.pl/file/178).

If you are interested in partnering with InPost, or you have any question or concerns with the plugin activation process, please contact our dedicated plugin implementation team by email at: Canada: support@inpost24.ca, Poland: plugin@inpost.pl, France: plugin@inpost24.fr, Italy: plugin@inpost24.it.

== Installation	 ==

From your administration panel:

1. Visit "Plugins &rarr; Add New".

2. Search for "InPost for WooCommerce".

3. Activate plugin from your Plugins page.

== Frequently Asked Questions ==

= Why InPost? =

InPost is an international network of fully automated parcel lockers that are accessible 24/7, the means no more waiting in line-ups, and empower the customer with the ability to collect, send and return parcels at their convenience. InPost's lockers are located with safety and security in mind.  You will find us at supermarkets, gas stations and at public transportation stations.

Using an InPost Parcel Locker is very easy and intuitive. An online shopper selects the InPost Parcel Lockers 24/7 at the e-shop’s check-out and chooses their preferred locker, usually a location that falls in their usual travel routes - along the way to home, school or work. Once the parcel is delivered, the customer receives an automated text message and email that includes a secure opening code. When the customer arrives at the locker they simply scan or enter the code and the parcel is ready for collection, in less than 10 seconds the customer is on their way.

= I am customer of InPost however I do not have the required access data for plugin activation. What should I do? =

If you are already an InPost customer please contact your Account Manager to be provided your personalized access (token) and personal assistance with plugin activation for your e-store. If you are interested in partnering with InPost please contact us by email at:  plugin@inpost.pl.


= I have customized version of WooCommerce e-store. Whether is possible to customize plugin for our e-store? =

The InPost plugin is designed for direct installation.  If you have your own developers, you will be able to modify the plugin due to WooCommerce’s open source-code.

If you need help with development for the WooCommerce plugin, please contact with our official design - [Inspire Labs](mailto:inpost@inspirelabs.pl).

= Support =

Zgłoszenia supportowe należy kierować pod adres support@inspirelabs.co

Zgłoszenia dotyczące problemów z działaniem wtyczki powinny zawierać wszelkie istotne dane potrzebne do zlokalizowania błędu, np. opis akcji, która doprowadziła do błędu, screenshot z ekranu przeglądarki z komunikatem błędu i inne przydatne dane, które naprowadzą na źródło problemu. Czasem możemy poprosić o dane dostępowe do panelu admina lub FTP.

Zgłoszenia obsługujemy tylko za pomocą komunikacji w systemie Freshdesk / E-Mail. Nie prowadzimy supportu telefonicznego

== Screenshots ==

1. Main settings.
2. Generate parcel.

== Changelog ==

= 1.3.7 - 2020.08.31 =
* Bugfix - Wordpress 5.5 - Can't select parcel(s) to download label, fixes in warnings on empty machines and locations

= 1.3.6 - 2020.04.24 =
* Bugfix - Checkout - checking the selection of a parcel machine for virtual products

= 1.3.5 - 2019.11.21 =
* Bugfix - Geowidget mobile view, Geowidget closing, parcel locker translation

= 1.3.4 - 2019.06.24 =
* Compatibility tests

= 1.3.3 - 2018.12.19 =
* Documentation update

= 1.3.2 - 2018.11.13 =
* Bugfix - Geowidget minor issue fix

= 1.3.1 - 2018.10.26 =
* Bugfix - The machine ID was not passed to the COD order details

= 1.3 - 2018.10.23 =
* Added Geowidget locator and selector for wp-admin areas

= 1.2 - 2018.10.18 =
* Added Geowidget locator and selector
* Minor issues fixed

= 1.1 - 2017.12.21 =
* Cross Border service disabled (no longer supported by InPost)

= 1.0.2 - 2016.07.11 =
* Fixed multiple payment description (BACS) in the checkout and e-mails
* Added Avada compatibility templates (must be copied to the theme folder to work!)

= 1.0.1 - 2016.04.06 =
* Fixed deprecated notice in PHP 7

= 1.0 - 2016.03.25 =
* First Release!
