/* global jQuery */
/* global tracking_data */

jQuery(function($){
    window.AllaniTransactions = window.AllaniTransactions || [];
    window.AllaniTransactions.push(tracking_data.products, tracking_data.total, tracking_data.order_id);
})