/**
 * JS scripts processed in admin panel
 */

( function( $ ){
    $( document ).ready( function(){

        $( '#woocommerce_price_comparison_sites_integration' ).on( 'change', '#ceneo_property_types', function(){

            var select_option = $( this ).val();
            if ( select_option === 'none' ) {
                $( '.pcsi-section' ).hide();
            }
            else {
                var current_section = $( '#' + select_option );
                current_section.siblings( '.pcsi-section' ).hide();
                current_section.show( 200 );
            }
        });

        $( '#ceneo_property_types' ).change();

        if($.select2) {
            $("#tag-nokaut_categories").select2();
            $("#tag-domodi_categories").select2();
            $("#tag-ceneo_categories").select2();
        }
        
        $('#chbx_ceneo').click(function() {
            $("#wrp_ceneo").toggle(this.checked);
        });

        $('#chbx_nokaut').click(function() {
            $("#wrp_nokaut").toggle(this.checked);
        });
    });
})( jQuery );
