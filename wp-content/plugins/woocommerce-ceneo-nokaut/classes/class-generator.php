<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// add generator class cn_into previously registered cronjob hook
add_action( 'generate_price_comparison_sites_data', array( new Cn_Generator(), '_run' ) );

/**
 * Main class cn_used for generating XML files for all price comparison sites.
 * It runs in a cron.
 */
class Cn_Generator {

    /**
     * Flag indicates whether generator can be triggered
     * @var bool
     */
    private $can_run = false;

    /**
     * Contains a list of sites objects
     * @var array
     */
    private $sites = array();

    /**
     * A path to plugin file
     * @var string|null
     */
    private $plugin_file = null;

    /**
     * Default constructor
     */
    public function __construct() {

        $this->set_run_flag();
        $this->plugin_file = Woocommerce_Cn_Integration::get_plugin_file();
    }

    /**
     * Method checks whether files can be generated and then performs actions
     * @return bool
     */
    public function _run() {
        if ( $this->can_run === false ) {
            return false;
        }

        $this->create_objects();
        $this->generate_xml_files();
    }

	/**
	* Method tries to generate site specified xml file if possible.
	* @param string $site_id (ex. $site_id='ceneo')
	* @return bool
	*/
	public function _run_single($site_id) {
		if ( $this->can_run === false ) {
            return false;
        }

		$this->create_objects();
		if(array_key_exists($site_id,$this->sites)){
			$this->sites[$site_id]->generate_xml();
			return true;
		}

		return false;
	}

    /**
     * Method loads classes for sites and creates an objects
     */
    private function create_objects() {

        $this->sites['ceneo'] = $this->get_site_instance( 'ceneo' );
        $this->sites['nokaut'] = $this->get_site_instance( 'nokaut' );
        $this->sites['domodi'] = $this->get_site_instance( 'domodi' );
        $this->sites['allani'] = $this->get_site_instance('allani');
    }

    /**
     * Runs generate action for all sites
     */
    private function generate_xml_files() {

        foreach ( $this->sites as $site ) {
            $site->generate_xml();
        }
    }

    /**
     * Instanciate the site
     * @param string $site_name
     * @return Cn_Site
     * @throws \Exception
     */
    public function get_site_instance( $site_name ) {

        $full_class_name = 'cn_' . $site_name;
        if ( !class_exists( $full_class_name ) ) {
            throw new \Exception( 'There\'s no site class cn_with given name: ' . $site_name );
        }

        return new $full_class_name( $this->plugin_file );
    }

    /**
     * Method contains the logic which checks whether generator can be triggered.
     */
    private function set_run_flag() {

        $can_run = true;

        // TODO: implement licence manager and return adequate value here

        $this->can_run = $can_run;
    }
}
