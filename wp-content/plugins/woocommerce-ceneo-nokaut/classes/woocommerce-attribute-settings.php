<?php

add_action('woocommerce_after_add_attribute_fields', function() {
   ?>
   <div class="form-field">
	    <label for="ceneo_attribute_exclusion">
	        <input type="checkbox" name="ceneo_attribute_exclusion">
	        <?php _e( 'Ceneo exclusion', 'woocommerce-ceneo-nokaut-integration' ); ?>
	    </label>
		<p class="description">
		    <?php _e( "Select this checkbox if you don't want this attribute to be added to XML", 'woocommerce-ceneo-nokaut-integration' ); ?>
		</p>
	</div>
	<div class="form-field">
	    <label for="ceneo_name_mapping"><?php _e( 'Ceneo name mapping', 'woocommerce-ceneo-nokaut-integration' ); ?></label>
		<input type="text" name="ceneo_name_mapping">
		<p class="description">
		    <?php _e( "Fill if you want to change the name visible in XML. Leave empty if you don't want to change it.", 'woocommerce-ceneo-nokaut-integration' ); ?>
		</p>
	</div>
	<?php
});

add_action('woocommerce_after_edit_attribute_fields', function() {
    $editing = $_GET['edit'];
    $options = get_option('ceneo_attributes_settings', array());
    
    
    $exclusion = $options[$editing]['exclusion'] ?? false;
    $checked = $exclusion ? 'checked' : '';
    
    $mapping =  $options[$editing]['mapping'] ?? '';
    ?>
    <tr class="form-field">
		<th scope="row" valign="top">
		    <label for="ceneo_attribute_exclusion"><?php _e( 'Ceneo exclusion', 'woocommerce-ceneo-nokaut-integration' ); ?></label>
		</th>
		<td>
	        <input type="checkbox" <?= $checked ?> name="ceneo_attribute_exclusion">
    		<p class="description">
    		    <?php _e( "Select this checkbox if you don't want this attribute to be added to XML", 'woocommerce-ceneo-nokaut-integration' ); ?>
    		</p>
		</td>
	</tr>
	<tr class="form-field">
		<th scope="row" valign="top">
		    <label for="ceneo_name_mapping"><?php _e( 'Ceneo name mapping', 'woocommerce-ceneo-nokaut-integration' ); ?></label>
		</th>
		<td>
		    <input type="text" value="<?= $mapping ?>" name="ceneo_name_mapping">
    		<p class="description">
		        <?php _e( "Fill if you want to change the name visible in XML. Leave empty if you don't want to change it.", 'woocommerce-ceneo-nokaut-integration' ); ?>
    		</p>
		</td>
	</tr>
    <?php
});

add_action('woocommerce_attribute_added', 'Cn_save_attribute_settings');
add_action('woocommerce_attribute_updated', 'Cn_save_attribute_settings');

function Cn_save_attribute_settings($id) {
    if( !isset($_POST['ceneo_name_mapping']) ) return;
    
    $mapping = sanitize_text_field($_POST['ceneo_name_mapping']);
    $exclusion = isset($_POST['ceneo_attribute_exclusion']);
    
    $options = get_option('ceneo_attributes_settings', array());
    
    $options[$id] = array(
        'mapping' => $mapping,
        'exclusion' => $exclusion
    );
    
    update_option('ceneo_attributes_settings', $options);
}