<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * class cn_covers all the functionalities added to admin panel
 */
class Cn_Admin extends Cn_Common {

    /**
     * Object handling additional product categories settings
     * @var Cn_Woocommerce_Extended_Categories
     */
    private $category_settings;

    /**
     * Method sets up the hooks
     */
    public function _run() {

        $this->category_settings = new Cn_Woocommerce_Extended_Categories( $this->templater );
        $this->category_settings_add();
        $this->category_settings->_run();

        // product write panel
        add_filter( 'woocommerce_product_data_tabs', array( $this, 'product_tab' ) );
        add_action( 'woocommerce_product_data_panels', array( $this, 'product_write_panel' ) );
        add_action( 'woocommerce_process_product_meta', array( $this, 'product_save_data' ) );

        // add assets
        add_action( 'admin_enqueue_scripts', array( $this, 'add_assets_admin' ) );
        
        // Add variation fields
        add_action( 'woocommerce_product_after_variable_attributes', function($loop, $variation_data, $variation) {
            foreach(Cn_Ceneo::$variation_properties as $name => $key) {
                echo "<div class='form-row form-row-full'>";
                $ceneo_properties = (array) get_post_meta($variation->ID, 'ceneo_properties', true);
                woocommerce_wp_text_input( 
    				array( 
    					'id'          => "{$key}[{$variation->ID}]", 
    					'label'       => $name, 
    					'placeholder' => '',
    					'desc_tip'    => 'true',
    					'value'       => isset($ceneo_properties[$key]) ? $ceneo_properties[$key] : '',
    				)
    			);
                echo "</div>";
            }
        }, 10, 3 );
        add_action( 'woocommerce_save_product_variation', function($post_id){
            foreach(Cn_Ceneo::$variation_properties as $name => $key) {
                if( isset($_POST["$key"][$post_id]) ) {
                    $ceneo_properties = (array) get_post_meta($post_id, 'ceneo_properties', true);
                    $ceneo_properties[$key] = sanitize_text_field($_POST["$key"][$post_id]);
                    update_post_meta($post_id, 'ceneo_properties', $ceneo_properties);
                }
            }
        }, 10, 1 );
    }

    /**
     * Load assets for admin panel
     */
    public function add_assets_admin() {

        wp_enqueue_style( 'wc_pcsi_admin_styles', plugins_url( 'assets/css/admin.css', $this->plugin_file ) );
        $this->enqueue_script( 'wc_pcsi_admin_js', 'admin.js', $this->plugin_file, false );
        wp_enqueue_script( 'select2' ); // WC registers it
    }

    /**
     * Add some settings into product categories options
     */
    private function category_settings_add() {

        // ceneo exclusion checkbox
        $this->category_settings->register_checkbox(
            self::CENEO_EXCLUSION,
            $this->get_translation( 'ceneo_product_exclude' ),
            $this->get_translation( 'ceneo_cat_description_exclude' ),
            false,
            10
        );
        
        // ceneo categories list
        $this->category_settings->register_select_list(
            self::CENEO_CATEGORIES,
            $this->get_translation( 'ceneo_categories_list' ),
            self::get_setting_option( self::SETTING_CENEO_UPDATE_CATEGORIES, array() ),
            array( 0, $this->get_translation( 'use_wc_category' ) ),
            $this->get_translation( 'ceneo_cat_list_desc' ),
            11
        );

        // nokaut exclusion checkbox
        $this->category_settings->register_checkbox(
            self::NOKAUT_EXCLUSION,
            $this->get_translation( 'nokaut_product_exclude' ),
            $this->get_translation( 'nokaut_cat_description_exclude' ),
            false,
            20
        );

        // nokaut categories list
        $this->category_settings->register_select_list(
            self::NOKAUT_CATEGORIES,
            $this->get_translation( 'nokaut_categories_list' ),
            self::get_setting_option( self::SETTING_NOKAUT_UPDATE_CATEGORIES, array() ),
            array( 0, $this->get_translation( 'use_wc_category' ) ),
            $this->get_translation( 'nokaut_cat_list_desc' ),
            21
        );

        // domodi exclusion checkbox
        $this->category_settings->register_checkbox(
            self::DOMODI_EXCLUSION,
            $this->get_translation( 'domodi_product_exclude' ),
            $this->get_translation( 'domodi_cat_description_exclude' ),
            false,
            22
        );

        // domodi categories list
        $this->category_settings->register_select_list(
            self::DOMODI_CATEGORIES,
            $this->get_translation( 'domodi_categories_list' ),
            self::get_setting_option( self::SETTING_DOMODI_UPDATE_CATEGORIES, array() ),
            array( -1, $this->get_translation( 'use_wc_category' ) ),
            $this->get_translation( 'domodi_cat_list_desc' ),
            23
        );
        
        // domodi exclusion checkbox
        $this->category_settings->register_checkbox(
            self::ALLANI_EXCLUSION,
            $this->get_translation( 'allani_product_exclude' ),
            $this->get_translation( 'allani_cat_description_exclude' ),
            false,
            24
        );

        // domodi categories list
        $this->category_settings->register_select_list(
            self::ALLANI_CATEGORIES,
            $this->get_translation( 'allani_categories_list' ),
            self::get_setting_option( self::SETTING_ALLANI_UPDATE_CATEGORIES, array() ),
            array( -1, $this->get_translation( 'use_wc_category' ) ),
            $this->get_translation( 'allani_cat_list_desc' ),
            25
        );
    }

    /**
     * Add a tab for external product
     */
    public function product_tab($product_data_tabs) {

        $product_data_tabs[self::PLUGIN_IDENTIFIER] = array(
    		'label' => $this->get_translation( 'price_comparison' ),
    		'target' => self::PLUGIN_IDENTIFIER,
    	);
        
        return $product_data_tabs;
    }

    /**
     * Save write panel data
     * @global array $post
     */
    public function product_save_data() {

        global $post;
        
        // general
        update_post_meta( $post->ID, self::ALTERNATIVE_DESCRIPTION, filter_input( INPUT_POST, self::ALTERNATIVE_DESCRIPTION ) );

        // checkboxes with exclusion
        update_post_meta( $post->ID, self::CENEO_EXCLUSION, filter_input( INPUT_POST, self::CENEO_EXCLUSION ) );
        update_post_meta( $post->ID, self::CENEO_BASKET, filter_input( INPUT_POST, self::CENEO_BASKET ) );
        update_post_meta( $post->ID, self::NOKAUT_EXCLUSION, filter_input( INPUT_POST, self::NOKAUT_EXCLUSION ) );
        update_post_meta( $post->ID, self::DOMODI_EXCLUSION, filter_input( INPUT_POST, self::DOMODI_EXCLUSION ) );
        update_post_meta( $post->ID, self::ALLANI_EXCLUSION, filter_input( INPUT_POST, self::ALLANI_EXCLUSION ) );

        // nokaut properties
        update_post_meta(
            $post->ID,
            self::NOKAUT_PROPERTIES,
            filter_input( INPUT_POST, self::NOKAUT_PROPERTIES, FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY )
        );

        // nokaut producer
        update_post_meta(
            $post->ID,
            self::NOKAUT_PRODUCER,
            filter_input( INPUT_POST, self::NOKAUT_PRODUCER )
        );

        // ceneo property type
        update_post_meta( $post->ID, self::CENEO_PROPERTY_TYPES, filter_input( INPUT_POST, self::CENEO_PROPERTY_TYPES ) );

        // ceneo properties
        update_post_meta(
            $post->ID,
            self::CENEO_PROPERTIES,
            filter_input( INPUT_POST, self::CENEO_PROPERTIES, FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY )
        );

        // domodi properties
        update_post_meta(
            $post->ID,
            self::DOMODI_PROPERTIES,
            filter_input( INPUT_POST, self::DOMODI_PROPERTIES, FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY )
        );

        // domodi producer
        update_post_meta(
            $post->ID,
            self::DOMODI_PRODUCER,
            filter_input( INPUT_POST, self::DOMODI_PRODUCER )
        );
        
        // Allani gender
        update_post_meta(
            $post->ID,
            self::ALLANI_GENDER,
            filter_input( INPUT_POST, self::ALLANI_GENDER )
        );
    }

    /**
     * Add a write panel for external product
     */
    public function product_write_panel() {

        global $post;

        $nokaut_values = get_post_meta( $post->ID, self::NOKAUT_PROPERTIES, true );
        $nokaut = new Cn_Nokaut( $this->plugin_file );

        $ceneo_values = get_post_meta( $post->ID, self::CENEO_PROPERTIES, true );
        $ceneo = new Cn_Ceneo( $this->plugin_file );

        $domodi_values = get_post_meta( $post->ID, self::DOMODI_PROPERTIES, true );
        $domodi = new Cn_Domodi( $this->plugin_file );
        
        $allani_values = get_post_meta( $post->ID, self::ALLANI_PROPERTIES, true );
        $domodi = new Cn_Allani( $this->plugin_file );

        $this->templater->render( 'admin/product-write-panel', array(
            'tab_id' => self::PLUGIN_IDENTIFIER,
            'groups' => array(
                'integration' => array(
                    array(
                        'id' => self::ALTERNATIVE_DESCRIPTION,
                        'type' => 'textarea',
                        'label' => $this->get_translation('alternative_description'),
                    ),
                ),
                'ceneo' => array(
                    array(
                        'type' => 'header',
                        'label' => $this->get_translation( 'ceneo_settings' )
                    ),
                    array(
                        'id' => self::CENEO_EXCLUSION,
                        'label' => $this->get_translation( 'ceneo_product_exclude' ),
                        'description' => $this->get_translation( 'ceneo_product_description_exclude' ),
                        'type' => 'checkbox'
                    ),
                    array(
                        'id' => self::CENEO_BASKET,
                        'label' => $this->get_translation( 'ceneo_product_basket' ),
                        'description' => $this->get_translation( 'ceneo_product_basket_description' ),
                        'type' => 'checkbox'
                    ),
                    array(
                        'type' => 'info',
                        'content' => __('Ceneo fields below are left to mantain backward compatibility. You should use standard product attributes to declare these properties.', 'woocommerce-ceneo-nokaut-integration')
                    ),
                    array(
                        'id' => self::CENEO_PROPERTY_TYPES,
                        'label' => $this->get_translation( 'property_type' ),
                        'description' => $this->get_translation( 'property_description' ),
                        'options' => $ceneo->get_property_types(),
                        'type' => 'select'
                    ),
                    array(
                        'id' => self::CENEO_PROPERTIES,
                        'type' => 'section_properties',
                        'data' => $ceneo->get_properties(),
                        'meta_values' => is_array( $ceneo_values ) ? $ceneo_values : array(),
                        'property_type' => get_post_meta( $post->ID, self::CENEO_PROPERTY_TYPES, true )
                    )
                ),
                'nokaut' => array(
                    array(
                        'type' => 'header',
                        'label' => $this->get_translation( 'nokaut_settings' )
                    ),
                    array(
                        'id' => self::NOKAUT_EXCLUSION,
                        'label' => $this->get_translation( 'nokaut_product_exclude' ),
                        'description' => $this->get_translation( 'nokaut_product_description_exclude' ),
                        'type' => 'checkbox'
                    ),
                    array(
                        'id' => self::NOKAUT_PRODUCER,
                        'type' => 'text',
                        'label' => $this->get_translation( 'nokaut_producer' ),
                        'description' => $this->get_translation( 'nokaut_producter_desc' )
                    ),
                    array(
                        'id' => self::NOKAUT_PROPERTIES,
                        'type' => 'regular_properties',
                        'data' => $nokaut->get_properties(),
                        'meta_values' => is_array( $nokaut_values ) ? $nokaut_values : array()
                    ),
                ),
                'domodi' => array(
                    array(
                        'type' => 'header',
                        'label' => $this->get_translation( 'domodi_settings' )
                    ),
                    array(
                        'id' => self::DOMODI_EXCLUSION,
                        'label' => $this->get_translation( 'domodi_product_exclude' ),
                        'description' => $this->get_translation( 'domodi_product_description_exclude' ),
                        'type' => 'checkbox'
                    ),
                    array(
                        'id' => self::DOMODI_PRODUCER,
                        'type' => 'text',
                        'label' => $this->get_translation( 'domodi_producer' ),
                        'description' => $this->get_translation( 'domodi_producter_desc' )
                    ),
                    array(
                        'id' => self::DOMODI_PROPERTIES,
                        'type' => 'regular_properties',
                        'data' => $domodi->get_properties(),
                        'meta_values' => is_array( $domodi_values ) ? $domodi_values : array()
                    ),
                ),
                'allani' => array(
                    array(
                        'type' => 'header',
                        'label' => $this->get_translation( 'allani_settings' )
                    ),
                    array(
                        'id' => self::ALLANI_EXCLUSION,
                        'label' => $this->get_translation( 'allani_product_exclude' ),
                        'description' => $this->get_translation( 'allani_product_description_exclude' ),
                        'type' => 'checkbox'
                    ),
                    array(
                        'id' => self::ALLANI_GENDER,
                        'type' => 'select',
                        'label' => $this->get_translation( 'allani_gender' ),
                        'options' => array(
                            '' => '---',
                            'female' => 'Kobieta',
                            'male' => 'Mężczyzna',
                            'female,male' => 'Unisex',
                        )
                    ),
                )
            ),
        ) );
    }
}
