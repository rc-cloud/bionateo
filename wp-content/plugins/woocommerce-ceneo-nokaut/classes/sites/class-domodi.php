<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * class cn_contains a set of methods which can be used for performing actions
 * on ceneo.pl API
 */
class Cn_Domodi extends Cn_Site {
    /**
     * String identifier of the site
     * @var string
     */
    protected $site_name = 'domodi';

    /**
     * @var string
     */
    protected $categories_url = '';

    /**
     * List of remote categories
     * @var array
     */
    protected $categories_list = array();

    /**
     * Filename to final XML document
     * @var string
     */
    protected $xml_output = 'domodi.xml';

    /**
     * A path to xml file which is a correct template for ceneo.pl
     * @var string
     */
    private $xml_template = 'templates/xml/domodi.xml';

    /**
     * Contains the properties listed in domodi documentation
     * @var array
     */
    protected $properties = array(
      'Manufacturer code' => 'domodi_cat_man_code',
      'Size attribute' => 'domodi_size_attribute',
      'Color attribute' => 'domodi_color_attribute'
    );

    /**
     * Contains varitions info
     * @var array
     */
    private $variations;

    /**
     * In this method we generate the xml document
     */
    public function generate_xml() {

        $xml = simplexml_load_file( $this->plugin_dir . 'templates/xml/ceneo.xml' );
        $xml->addChild('offers');

        $all_products =  $this->get_all_products();

        foreach( $all_products as $product ) {

            if ( $product instanceof WC_Product_Variable ) {
                $this->process_variable( $product, $xml->offers );
            }
            else {
                $this->process_product( $product, $xml->offers );
            }
        }

        $this->create_final_xml( $xml );
    }

    /**
     * Process all kind of products
     * @param \WC_Product $product
     * @param \SimpleXMLElement $offers
     * @param string $name_suffix
     * @param array $mapped_values
     */
    protected function process_product( WC_Product $product, SimpleXMLElement $offers, $color = null ) {

        // create 'offer' node
        $offer_node = $offers->addChild( 'offer' );

//        $prod_name = trim( $product->post->post_title . ' ' . $name_suffix );
        $prod_name = trim( $product->post->post_title );
        $offer_id = $this->get_offer_id( $product, $prod_name );
        $id = $product->id;

        // ID
        $id_node = $offer_node->addChild('id'); // Setting offer id
        $this->addCData($id, $id_node);

        // URL
        $offer_node->url = null;
        $this->addCData( get_permalink( $id ), $offer_node->url );

        // Name
        $offer_node->name = null;
		    $this->addCData( $prod_name,$offer_node->name );

        // Description
        $offer_node->desc = null;
		$this->addCData( $this->get_description($product), $offer_node->desc );

        // Category
        $offer_node->cat = null;
	      $this->addCData( $this->get_product_cat( $id ),$offer_node->cat );

        // Brand
        $producer = get_post_meta( $id, Cn_Common::DOMODI_PRODUCER, true );
        $offer_node->brand = null;
		    if ( strlen( $producer ) > 0 ) {
            $this->addCData( $producer,$offer_node->brand );
        }

        // Availibility
        $offer_node->avail = $product->is_in_stock() ? 1 : 99;

        // On sale
        $offer_node->IsPromoted = $product->is_on_sale() ? 1 : 0;

        // Price
        if(get_class($product) == 'WC_Product_Variable'){
            if( $product->is_on_sale() ) {
                $prices = $product->get_variation_prices();
                $oldprice = $offer_node->addChild('oldprice');
                $this->addCData(min($prices['regular_price']),$oldprice);
                $price = $offer_node->addChild('price');
                $this->addCData(min($prices['sale_price']), $price);
            } else {
                $price = $offer_node->addChild('price');
                $this->addCData($product->get_price(), $price);
            }
        }
        else{
            if( $product->is_on_sale() ) {
                $oldprice = $offer_node->addChild('oldprice');
                $this->addCData($product->get_regular_price(),$oldprice);
                $price = $offer_node->addChild('price');
                $this->addCData($product->get_sale_price(), $price);
            } else {
                $price = $offer_node->addChild('price');
                $this->addCData($product->get_price(), $price);
            }
        }

        // Images
        // Main image
        $offer_node->addChild('imgs');
        $image_src = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'large' );
        if( is_array( $image_src ) ) {
            $image = $offer_node->imgs->addChild( 'img' );
            $this->addCData(htmlspecialchars( $image_src[0] ), $image);
            $image->addAttribute('default', 'true');
        }

        // Gallery images
        $attachment_ids = $product->get_gallery_attachment_ids();
        foreach( $attachment_ids as $attachment_id )
        {
          $image_node = $offer_node->imgs->addChild( 'img' );
          $this->addCData(htmlspecialchars( wp_get_attachment_url( $attachment_id )), $image_node);
        }

        // Producer code
        $properties = get_post_meta( $product->id, 'domodi_properties', true );

        if( isset( $properties['Manufacturer code'] ) &&  $properties['Manufacturer code'] ) {
          $offer_node->addChild('attrs');
          $code_attribute = $offer_node->attrs->addChild('attr');
          $this->addCData($properties['Manufacturer code'], $code_attribute);
          $code_attribute->addAttribute( 'name', 'Kod_producenta' );
        }

        // Variations only
        if( $product->get_type() == 'variable' ) {
            // Add sizes
            if( isset( $this->variations[ $id ]['sizes'] ) ) {
                $sizes = implode( '; ', $this->variations[ $product->id ]['sizes'] );
                $sizes_node = $offer_node->addChild('sizes');
                $this->addCData($sizes, $sizes_node);
            }
    
            // Add colors
            if( isset( $this->variations[ $id ]['colors'] ) && ( $this->variations[ $id ]['colors'] !== NULL ) ) {
              $colors = implode( '; ', $this->variations[ $product->id ]['colors'] );
              if( !isset( $offer_node->attrs) ) {
                  $offer_node->addChild('attrs');
              }
              $color_attribute = $offer_node->attrs->addChild('attr');
              $this->addCData($colors, $color_attribute);
              $color_attribute->addAttribute( 'name', 'Kolor' );
            }
        }
    }

    /**
     * Process a variable product by creating an array of variations
     * @param \WC_Product_Variable $product
     * @param \SimpleXMLElement $offers
     */
    private function process_variable( WC_Product_Variable $product, SimpleXMLElement $offers ) {
        $properties = get_post_meta( $product->id, 'domodi_properties', true );
        $color_slug = null;
        $size_slug = null;
        if( isset($properties['Size attribute']) && $properties['Size attribute'] ) {
            $size_slug = sanitize_title($properties['Size attribute']);
        } else {
            $size_slug = sanitize_title( get_option('domodi_size_attribute_name') );
        }
        
        if( isset($properties['Color attribute']) && $properties['Color attribute'] ) {
            $color_slug = sanitize_title($properties['Color attribute']);
        } else {
            $color_slug = sanitize_title( get_option('domodi_color_attribute_name') );
        }
        
        $children = $product->get_children();
        
        foreach ($children as $child) {
          $child = new WC_Product_Variation( $child );
          if( !$child->is_in_stock() ) continue;
          
          $attributes = $child->get_variation_attributes();
          $color = null; $size = null;

          if( isset( $attributes[ 'attribute_' . $size_slug ] ) )  {
            $size = get_term_by('slug', $attributes[ 'attribute_' . $size_slug ], $size_slug)->name;
          }
          elseif( isset( $attributes[ 'attribute_pa_' . $size_slug ] ) )  {
            $size = get_term_by('slug', $attributes[ 'attribute_pa_' . $size_slug ], 'pa_' . $size_slug)->name;
          }

          if( isset( $attributes[ 'attribute_' . $color_slug ] ) )  {
              $color = get_term_by('slug', $attributes[ 'attribute_' . $color_slug ], $color_slug)->name;
          }
          elseif( isset( $attributes[ 'attribute_pa_' . $color_slug ] ) )  {
              $color = get_term_by('slug', $attributes[ 'attribute_pa_' . $color_slug ], 'pa_' . $color_slug)->name;
          }

            if(!isset($this->variations[ $product->id ]['sizes'][ $size ]) && $size !== null ){ $this->variations[ $product->id ]['sizes'] [ $size ] = $size; }
            if(!isset($this->variations[ $product->id ]['colors'][ $color ]) && $color !== null ){ $this->variations[ $product->id ]['colors'] [ $color ] = $color; }

        }
        
        $atts = $product->get_attributes();
        if(!isset($this->variations[ $product->id ]['sizes']) && !empty($atts) ) {
            // Product might have size, but not used as variation
            if( isset($atts['pa_'.$size_slug]) ) {
                $sizes = $atts['pa_'.$size_slug]->get_terms();
                foreach($sizes as $size) $this->variations[ $product->id ]['sizes'][ $size->name ] = $size->name;
            }
        }
        if(!isset($this->variations[ $product->id ]['colors']) && !empty($atts) ) {
            // Product might have color, but not used as variation
            if( isset($atts['pa_'.$color_slug]) ) {
                $colors = $atts['pa_'.$color_slug]->get_terms();
                foreach($colors as $color) $this->variations[ $product->id ]['colors'][ $color->name ] = $color->name;
            }
        }

        $this->process_product( $product, $offers );

    }

    /**
     * Contains a logic of picking correct category name. There are two priorities: most important is to return the
     * remote category name. In no remote category is defined, method will return first met category name.
     * @param int $product_id
     * @return string
     */
    protected function get_product_cat( $product_id ) {

        $category_name = '';
        $categories = $this->get_correct_categories( $product_id );
        $remote_categories = $this->get_categories_list();

        if ( !is_array( $categories ) ) {
            return '';
        }

        foreach( $categories as $cat ) {

            // store the name of first category, so it can be returned in case when no remote category will be found
            if ( strlen( $category_name ) !== 0 ) {
                $category_name .= ' / ';
            }
            $category_name .= $cat->name;
            $remote_identifier = get_woocommerce_term_meta( $cat->term_id, $this->get_category_identifier() );

            // if remote category is set, return it directly
            if ( strlen( $remote_identifier ) > 0 && $remote_identifier != 0 && isset( $remote_categories[$remote_identifier] ) ) {
                return $remote_categories[$remote_identifier];
            }
        }

        return $category_name;
    }

    /**
     * Returns the identifier of nokaut category name
     * @return string
     */
    protected function get_category_identifier() {

        return Cn_Common::DOMODI_CATEGORIES;
    }

    /**
     * Returns an identifier of WP option containing categories list for nokaut
     * @return string
     */
    protected function get_categories_list_identifier() {

        return Cn_Common::SETTING_DOMODI_UPDATE_CATEGORIES;
    }

    /**
     * Returns the identifier of exclusion flag for nokaut site
     * @return string
     */
    protected function get_exclusion_identifier() {

        return Cn_Common::DOMODI_EXCLUSION;
    }

    /**
     * Returns a list on remote categories.
     * @return array
     */
    public function get_remote_categories() {

        //Well, actually in this case it categories are not remote
        $categories_string = file_get_contents( $this->plugin_dir . '/assets/categories/domodi.txt' );

        $categories = explode( '|', $categories_string );

        return $categories;
    }

}
