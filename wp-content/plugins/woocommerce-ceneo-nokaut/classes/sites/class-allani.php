<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Cn_Allani extends Cn_Domodi {
    /**
     * String identifier of the site
     * @var string
     */
    protected $site_name = 'allani';

    /**
     * Filename to final XML document
     * @var string
     */
    protected $xml_output = 'allani.xml';

    /**
     * Contains the properties listed in domodi documentation
     * @var array
     */
    protected $properties = array(
      'Gender' => 'allani_gender_attribute'
    );

    /**
     * Process all kind of products
     * @param \WC_Product $product
     * @param \SimpleXMLElement $offers
     * @param string $name_suffix
     * @param array $mapped_values
     */
    protected function process_product( WC_Product $product, SimpleXMLElement $offers, $color = null ) {
        
        // Properties
        $properties = get_post_meta( $product->id, 'domodi_properties', true );
        $properties['gender'] = get_post_meta( $product->id, 'allani_gender_attribute', true );

        
        // Gender is required by allani
        if( empty($properties['gender']) ) return;

        // create 'offer' node
        $offer_node = $offers->addChild( 'offer' );

        $prod_name = trim( $product->post->post_title );
        $offer_id = $this->get_offer_id( $product, $prod_name );
        $id = $product->id;

        // ID
        $id_node = $offer_node->addChild('id'); // Setting offer id
        $this->addCData($id, $id_node);

        // URL
        $offer_node->url = null;
        $this->addCData( get_permalink( $id ), $offer_node->url );

        // Name
        $offer_node->name = null;
		    $this->addCData( $prod_name,$offer_node->name );

        // Description
        $offer_node->desc = null;
		$this->addCData( $this->get_description($product), $offer_node->desc );

        // Category
        $offer_node->cat = null;
	      $this->addCData( $this->get_product_cat( $id ),$offer_node->cat );

        // Brand
        $producer = get_post_meta( $id, Cn_Common::DOMODI_PRODUCER, true );
        $offer_node->brand = null;
		    if ( strlen( $producer ) > 0 ) {
            $this->addCData( $producer,$offer_node->brand );
        }

        // Availibility
        $offer_node->avail = $product->is_in_stock() ? 1 : 99;

        // On sale
        $offer_node->IsPromoted = $product->is_on_sale() ? 1 : 0;

        // Price
        if(get_class($product) == 'WC_Product_Variable'){
            if( $product->is_on_sale() ) {
                $prices = $product->get_variation_prices();
                $oldprice = $offer_node->addChild('oldprice');
                $this->addCData(min($prices['regular_price']),$oldprice);
                $price = $offer_node->addChild('price');
                $this->addCData(min($prices['sale_price']), $price);
            } else {
                $price = $offer_node->addChild('price');
                $this->addCData($product->get_price(), $price);
            }
        }
        else{
            if( $product->is_on_sale() ) {
                $oldprice = $offer_node->addChild('oldprice');
                $this->addCData($product->get_regular_price(),$oldprice);
                $price = $offer_node->addChild('price');
                $this->addCData($product->get_sale_price(), $price);
            } else {
                $price = $offer_node->addChild('price');
                $this->addCData($product->get_price(), $price);
            }
        }

        // Images
        // Main image
        $offer_node->addChild('imgs');
        $image_src = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'large' );
        if( is_array( $image_src ) ) {
            $image = $offer_node->imgs->addChild( 'img' );
            $this->addCData(htmlspecialchars( $image_src[0] ), $image);
            $image->addAttribute('default', 'true');
        }

        // Gallery images
        $attachment_ids = $product->get_gallery_attachment_ids();
        foreach( $attachment_ids as $attachment_id )
        {
          $image_node = $offer_node->imgs->addChild( 'img' );
          $this->addCData(htmlspecialchars( wp_get_attachment_url( $attachment_id )), $image_node);
        }
        
        // Płeć
        $offer_node->addChild('attrs');
        $gender_attribute = $offer_node->attrs->addChild('gender');
        $this->addCData($properties['gender'], $gender_attribute);

        // Kod producenta
        if( !empty( $properties['Manufacturer code'] ) ) {
          $code_attribute = $offer_node->attrs->addChild('attr');
          $this->addCData($properties['Manufacturer code'], $code_attribute);
          $code_attribute->addAttribute( 'name', 'Kod_producenta' );
        }

        // Variations only
        if( $product->get_type() == 'variable' ) {
            // Add sizes
            if( isset( $this->variations[ $id ]['sizes'] ) ) {
                $sizes = implode( '; ', $this->variations[ $product->id ]['sizes'] );
                $sizes_node = $offer_node->addChild('sizes');
                $this->addCData($sizes, $sizes_node);
            }
    
            // Add colors
            if( isset( $this->variations[ $id ]['colors'] ) && ( $this->variations[ $id ]['colors'] !== NULL ) ) {
              $colors = implode( '; ', $this->variations[ $product->id ]['colors'] );
              if( !isset( $offer_node->attrs) ) {
                  $offer_node->addChild('attrs');
              }
              $color_attribute = $offer_node->attrs->addChild('attr');
              $this->addCData($colors, $color_attribute);
              $color_attribute->addAttribute( 'name', 'Kolor' );
            }
        }
    }
    
        /**
     * Returns the identifier of nokaut category name
     * @return string
     */
    protected function get_category_identifier() {

        return Cn_Common::ALLANI_CATEGORIES;
    }

    /**
     * Returns an identifier of WP option containing categories list for nokaut
     * @return string
     */
    protected function get_categories_list_identifier() {

        return Cn_Common::SETTING_ALLANI_UPDATE_CATEGORIES;
    }

    /**
     * Returns the identifier of exclusion flag for nokaut site
     * @return string
     */
    protected function get_exclusion_identifier() {

        return Cn_Common::ALLANI_EXCLUSION;
    }
}
