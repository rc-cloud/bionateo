<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * class cn_contains a set of methods which can be used for performing actions
 * on ceneo.pl API
 */
class Cn_Nokaut extends Cn_Site {
    /**
     * String identifier of the site
     * @var string
     */
    protected $site_name = 'nokaut';

    /**
     * @var string
     */
    protected $categories_url = 'http://www.nokaut.pl/integracja/kategorie.xml';

    /**
     * List of remote categories
     * @var array
     */
    protected $categories_list = array();

    /**
     * Filename to final XML document
     * @var string
     */
    protected $xml_output = 'nokaut.xml';

    /**
     * A path to xml file which is a correct template for ceneo.pl
     * @var string
     */
    private $xml_template = 'templates/xml/nokaut.xml';

    /**
     * Contains the properties listed in ceneo documentation
     * @var array
     */
    protected $properties = array(
        'isbn' => 'nokaut_cat_isbn',
        'ean' => 'nokaut_cat_ean',
        'mpn' => 'nokaut_cat_mpn',
        'bdk' => 'nokaut_cat_bdk',
        'bloz7' => 'nokaut_cat_bloz7',
        'osdw' => 'nokaut_cat_osdw'
    );

    /**
     * In this method we generate the xml document
     */
    public function generate_xml() {

        $xml = simplexml_load_file( $this->plugin_dir . $this->xml_template );
        $all_products =  $this->get_all_products();

        foreach( $all_products as $product ) {

            if ( $product instanceof WC_Product_Variable ) {

                $this->process_variation( $product, $xml->offers );
            }
            else {

                $this->process_product( $product, $xml->offers );
            }
        }

        $this->create_final_xml( $xml );
    }

    /**
     * Process all kind of products
     * @param \WC_Product $product
     * @param \SimpleXMLElement $offers
     * @param string $name_suffix
     * @param array $mapped_values
     */
    private function process_product( WC_Product $product, SimpleXMLElement $offers, $name_suffix = '', $mapped_values = array() ) {

        // create 'offer' node
        $offer_node = $offers->addChild( 'offer' );

        $prod_name = trim( $product->post->post_title . ' ' . $name_suffix );
        $offer_id = $this->get_offer_id( $product, $prod_name );
        $main_prod_id = $this->get_main_id( $product );

        $offer_node->id = $offer_id;
        $offer_node->name = null;
		$this->addCData( $prod_name,$offer_node->name );
        $offer_node->url = null;
		$this->addCData( get_permalink( $main_prod_id ) . '#' . $offer_id,$offer_node->url );
        if( $product instanceof WC_Product_Variable ){
            $variation = wc_get_product($product->id);
            $offer_node->price = $variation->get_price();
        } else {
            $offer_node->price = $product->get_price();
        }
        
        
        // Add additional attributes
        $attributes = $product->get_attributes();
        if( $attributes && $variation ){
            $additional_properties = array();
            foreach( $attributes as $attribute ) {
                if( $attribute->get_variation() ) continue;
                
                $terms = $attribute->get_terms();
                $values = [];
                if( $terms ) {
                    // Global attribute
                    foreach( $terms as $term ) {
                        $values[] = $term->name;
                    }
                } else {
                    // Local attribute
                    $values = $attribute->get_options();
                }
                
                $name = wc_attribute_label($attribute->get_name(), $product);
                $additional_properties[$name] = implode(', ', $values);
            }
            if( $additional_properties ) {
                // wp_die(print_r($additional_properties, 1));
                $this->add_property_set($additional_properties, $offer_node);
            }
        }
        
        
        $offer_node->description = null;
		$this->addCData( $this->get_description($product), $offer_node->description );
        
        $offer_node->category = null;
		$this->addCData( $this->get_product_cat( $main_prod_id ),$offer_node->category );

        // get producer name and put it into node
        $producer = get_post_meta( $main_prod_id, Cn_Common::NOKAUT_PRODUCER, true );
//        $offer_node->producer = $producer;
        $offer_node->producer = null;
		if ( strlen( $producer ) > 0 ) {
            $this->addCData( $producer,$offer_node->producer );
        }

        $offer_node->availability = ($product->is_in_stock()) ? 0 : 4;
        ($product->managing_stock()) ? $offer_node->instock = $product->get_stock_quantity() : null;

        // get product image URL and put it into node
        $image_src = wp_get_attachment_image_src( get_post_thumbnail_id( $main_prod_id ), 'large' );
//        $offer_node->image = is_array( $image_src ) ? htmlspecialchars( $image_src[0] ) : '';
        $offer_node->image = null;
        is_array( $image_src ) ? $this->addCData( htmlspecialchars( $image_src[0] ),$offer_node->image ) : null;

        // add properties nodes
        $this->add_properties( $offer_node, $main_prod_id, $mapped_values );
    }

    /**
     * Process a variation product by adding nodes for all possible variations
     * @param \WC_Product_Variable $product
     * @param \SimpleXMLElement $offers
     */
    private function process_variation( WC_Product_Variable $product, SimpleXMLElement $offers ) {

        $variations = $product->get_children();
        foreach($variations as $variation){
            $product->set_id($variation);
            $values = array();
            $variation = new WC_Product_Variation( $variation );
            $attributes = $variation->get_attributes();
            $vattributes = $variation->get_variation_attributes();
            foreach($vattributes as $key => $attribute){
                $slug = explode('attribute_',$key);
                if(isset($slug[1]) && array_key_exists($slug[1], $attributes)){
                    if( !empty($attributes[$slug[1]]['is_variation']) ){
                        $values[] = get_term_by('slug', $attribute, $slug[1])->name;
                    }
                    $properties[$slug[1]] = get_term_by('slug', $attribute, $slug[1])->name;
                }
            }
            if(!empty($values)) {
                $name = implode(' ', $values);
                $name = '( ' . $name . ' )';
            }
            $this->process_product( $product, $offers, $name, $properties );
        }

    }

    /**
     * Method adds all of valid properties into xml
     * @param \SimpleXMLElement $offer_node
     * @param int $main_prod_id
     * @param array $mapped_values
     * @return bool
     */
    private function add_properties( SimpleXMLElement $offer_node, $main_prod_id, array $mapped_values ) {

        $properties = get_post_meta( $main_prod_id, 'nokaut_properties', true );
        if ( is_array( $properties ) ) {
            $this->add_property_set( $properties, $offer_node );
        }
        if( ($sku = get_post_meta( $main_prod_id, '_sku', true )) && ($sku_mapping = Cn_Common::get_setting_option("{$this->site_name}_SKU_mapping", '')) ) {
            // Check if property is filled for a product
            if( empty($properties[$sku_mapping]) ) {
    			$property = $offer_node->addChild('property');
    			$this->addCData( $sku, $property );
                $property->addAttribute( 'name', $sku_mapping );
            }
        }

        // remove 'pa_' part from attribute identifiers
        $cleared_map_values = array();
        foreach( $mapped_values as $key => $value ) {

            $new_key_parts = explode( 'pa_', $key );
            if ( isset( $new_key_parts[1] ) ) {
                $cleared_map_values[$new_key_parts[1]] = $value;
            }
        }

        $this->add_property_set( $cleared_map_values, $offer_node );
    }

    /**
     * Adds a 'property' elements for given set of data
     * @param array $data_set
     * @param \SimpleXMLElement $offer_node
     */
    private function add_property_set( array $data_set, SimpleXMLElement $offer_node ) {

        foreach( $data_set as $prop_id => $prop_val ) {

            if ( strlen( $prop_val ) === 0 ) {
                continue;
            }
//			$offer_node->property = null;
			$property = $offer_node->addChild('property');
			$this->addCData( $prop_val, $property );
//            $prop_node = $offer_node->property;
//            $prop_node->addAttribute( 'name', $prop_id );
            $property->addAttribute( 'name', $prop_id );
        }
    }

    /**
     * Returns the identifier of nokaut category name
     * @return string
     */
    protected function get_category_identifier() {

        return Cn_Common::NOKAUT_CATEGORIES;
    }

    /**
     * Returns an identifier of WP option containing categories list for nokaut
     * @return string
     */
    protected function get_categories_list_identifier() {

        return Cn_Common::SETTING_NOKAUT_UPDATE_CATEGORIES;
    }

    /**
     * Returns the identifier of exclusion flag for nokaut site
     * @return string
     */
    protected function get_exclusion_identifier() {

        return Cn_Common::NOKAUT_EXCLUSION;
    }

    /**
     * Returns a list on remote categories.
     * @return array
     */
    public function get_remote_categories($file_name = NULL) {
        $file_dir = ($file_name !== NULL) ? $file_name : $this->get_categories_url();

        $xml = simplexml_load_file( $file_dir );
        $categories = array();

        if ( $xml instanceof \SimpleXMLElement ) {

            foreach( $xml->categories->children() as $category ) {

                if ( !isset( $category['id'] ) ) {
                    continue;
                }
                $categories[(int)$category['id']] = (string)$category;
            }
        }

        return $categories;
    }

}