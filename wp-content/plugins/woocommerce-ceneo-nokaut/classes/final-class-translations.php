<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * class cn_separates the translation used in the extension from code layer
 */
final class Cn_Translations {

    /**
     * Contains key => value pairs of translation index and test
     * @var array
     */
    private $translations = null;

    /**
     * Constructor sets the translations on place
     */
    public function __construct() {

        $this->add( 'price_comparison', __( 'Price comparison', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_product_exclude', __( 'Ceneo.pl exclusion', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_product_description_exclude', __( 'Exclude this product from the XML file prepared for Ceneo.pl', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_product_basket', __('Add to "Kup na Ceneo"'));
        $this->add( 'ceneo_product_basket_description', __('Add this product to "Kup na Ceneo"'));
        $this->add( 'nokaut_product_exclude', __( 'Nokaut.pl exclusion', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'nokaut_product_description_exclude', __( 'Exclude this product from the XML file prepared for Nokaut.pl', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_description_exclude', __( 'Exclude this category from the XML file prepared for Ceneo.pl', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'nokaut_cat_description_exclude', __( 'Exclude this category from the XML file prepared for Nokaut.pl', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'settings_saved', __( 'Price comparison sites settings saved.', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'settings_category_failure', __( 'Failed to get remote categories, however other plugin settings are saved.', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_settings', __( 'Ceneo.pl settings', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'update_category_desc', __( 'Update categories list', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'update_button_text', __( 'Run update', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'nokaut_settings', __( 'Nokaut.pl settings', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'settings_and_categories_saved_ceneo', __( 'Updated categories list for Ceneo.pl. Price comparison sites settings saved.', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'settings_and_categories_saved_nokaut', __( 'Updated categories list for Nokaut.pl. Price comparison sites settings saved.', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_categories_list', __( 'Ceneo.pl category', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'nokaut_categories_list', __( 'Noakut.pl category', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_list_desc', __( 'It\'s highly recommended to map WooCommerce categories into Ceneo.pl categories. In case when above list is empty, go to "WooCommerce -> Price comparison" settings and update the list.', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'nokaut_cat_list_desc', __( 'It\'s highly recommended to map WooCommerce categories into Nokaut.pl categories. In case when above list is empty, go to "WooCommerce -> Price comparison" settings and update the list.', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'use_wc_category', __( 'Use WooCommerce category namespace', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'property_type', __( 'Property type', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'property_description', __( 'Choose the type of properties to display text inputs', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'pick_the_type', __( '- choose type -', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'map_attribute', __( 'Map attribute', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'no_mapping', __( 'No mapping', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'nokaut_producer', __( 'Producer', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'nokaut_producter_desc', __( 'Short name of the producer, i.e "Sony"', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'save_changes', __( 'Save changes', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'file_not_generated', __( 'File not generated yet', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'site_name', __( 'Site name', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'site_url', __( 'XML file URL', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'update_time', __( 'Last update', 'woocommerce-ceneo-nokaut-integration' ) );
    	$this->add( 'update_label', __( 'Update XML File', 'woocommerce-ceneo-nokaut-integration' ) );
    	$this->add( 'update_xml', __( 'Update XML', 'woocommerce-ceneo-nokaut-integration' ) );
    	$this->add( 'xml_update_success_ceneo', __( 'Ceneo.pl XML file successfully updated.', 'woocommerce-ceneo-nokaut-integration' ) );
    	$this->add( 'xml_update_success_nokaut', __( 'Nokaut.pl XML file successfully updated.', 'woocommerce-ceneo-nokaut-integration' ) );
    	$this->add( 'xml_update_fail', __( 'Error occurred while updating XML file.', 'woocommerce-ceneo-nokaut-integration' ) );
    	$this->add( 'xml_update_unknown', __( 'Unknown XML update status.', 'woocommerce-ceneo-nokaut-integration' ) );
    	$this->add('sku_mapping', __('SKU mapping') );

        // translations for ceneo properties
        $this->add( 'ceneo_cat_books', __( 'Books', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_author', __( 'Author full name', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_isbn', __( 'Book ISBN code', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_pages', __( 'Number of pages', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_publishing', __( 'Publishing house', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_pub_date', __( 'Publishing date', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_lum', __( 'Type of luminaire eg, soft, hard', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_book_format', __( 'Book fromat ie. B5, A5, 172x245cm, 15.5x22.5cm', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_book_url', __( 'URL to Table of Contents', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_book_portion', __( 'Link to a portion of the book', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_ebooks', __( 'E-books', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_ebook_format', __( 'E-book format ie. PDF, ePub, AZW, MOBI ', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_audiobooks', __( 'Audiobooks', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_audiobook_format', __( 'Audiobook format ie. MP3, WMV, Płyta CD', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_grocery', __( 'Grocery', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_manufacturer', __( 'The manufacturer of the product', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_ean', __( 'The barcode appearing on the products, packaging', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_quantity', __( 'Quantity in package example. 12szt., 2kg', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_tires', __( 'Tires', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_manufacturer_code', __( 'Manufacturer code', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_tire_type', __( 'Tire type', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_tire_width', __( 'The width of the tire in millimeters', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_aspect_ratio', __( 'Aspect ratio', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_rim_size', __( 'Rim size', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_speed_rating', __( 'Speed rating', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_load_index', __( 'Load index', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_season', __( 'Seasonality ie. Zimowe, Letnie, Całoroczne', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_rims', __( 'Rims', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_man_code', __( 'Code assigned to the product by the manufacturer', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_size_of_rim', __( 'The width and the outer diameter in inches for example, 6.5 x15', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_rim_spec', __( 'Number and diameter of the mounting screws of the circle on which there are openings e.g. 5x110', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_et', __( 'The distance between the mounting plane of the rim, and the center of symmetry (ET)', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_perfumes', __( 'Perfumes', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_line', __( 'Line of smell - a series such as Miss Pucci, Orange Celebration of Happiness', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_perfume_type', __( 'Type of product ie. Woda perfumowana, Woda toaletowa, Woda kolońska, Dezodorant roll on, Dezodorant sztyft', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_capacity', __( 'Capacity given in mililiters ie. 50 ml, 100 ml', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_music', __( 'Music', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_artist', __( 'Artist', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_media', __( 'Media', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_record_label', __( 'Record label', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_genre', __( 'Genre', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_games', __( 'Games', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_platform', __( 'The platform on which the game is meant as PC, PS2, Xbox360', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_movies', __( 'Movies', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_director', __( 'Full name of movie director', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_film_company', __( 'Name the film company', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_actors', __( 'The actors playing in the film', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_original_title', __( 'The original title of the film', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_medicines', __( 'Medicines', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_bloz12', __( 'ID of the drug - it is necessary to provide a minimum of one of the codes for drugs and pharmaceutical products (it is recommended to enter both codes for each product)', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_bloz7', __( 'For articles that do not have the Bloz12 code, enter the Bloz7 code', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_med_qty', __( 'Number of tablets, such as bottle capacity 12szt., 250ml', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_clothes', __( 'Clothes', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_cloth_type', __( 'Type', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_color', __( 'Dominant Color. If the product is present in several color variants, there should be a separate offer for each color', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_cloth_size', __( 'Size. If the product is available in different sizes individual values​should be separated by a semicolon, eg "S;L;XL"', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_cloth_season', __( 'Season eg. "wiosna/lato"', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_fason', __( 'Understood as a fashion single value eg. rurki, dzwony, szerokie', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_set_id', __( 'Set identifier', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_other', __( 'Other', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'ceneo_cat_name_append', __( 'Append to name', 'woocommerce-ceneo-nokaut-integration' ) );
		$this->add( 'shipping_class', __( 'Shipping Class', 'woocommerce-ceneo-nokaut-integration' ) );
		$this->add( 'one_day', __( 'One day', 'woocommerce-ceneo-nokaut-integration' ) );
		$this->add( 'three_days', __( '3 days', 'woocommerce-ceneo-nokaut-integration' ) );
		$this->add( 'seven_days', __( '7 days', 'woocommerce-ceneo-nokaut-integration' ) );
		$this->add( 'more_days', __( 'More than week', 'woocommerce-ceneo-nokaut-integration' ) );

        // translations for nokaut properties
        $this->add( 'nokaut_cat_isbn', __( 'ISBN code', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'nokaut_cat_ean', __( 'The barcode appearing on the products', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'nokaut_cat_mpn', __( 'Manufacturer product code, which is a unique product code specified by manufacturer', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'nokaut_cat_bdk', __( 'BDK code, in case when your shop cooperates with that company', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'nokaut_cat_bloz7', __( 'BLOZ7 code', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'nokaut_cat_osdw', __( 'OSDW code', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'nokaut_cat_model', __( 'Model', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'nokaut_cat_author', __( 'Author', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'nokaut_cat_artist', __( 'Artist', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'nokaut_cat_director', __( 'Director', 'woocommerce-ceneo-nokaut-integration' ) );

        // translations for domodi proporties
        $this->add( 'domodi_settings', __( 'Domodi settings', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'domodi_product_exclude', __( 'Domodi.pl exclusion', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'domodi_cat_description_exclude', __( 'Exclude this category from the XML file prepared for Domodi.pl', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'domodi_categories_list', __( 'Domodi.pl category', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'domodi_product_description_exclude', __( 'Exclude this product from the XML file prepared for Domodi.pl', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'domodi_cat_list_desc', __( 'It\'s highly recommended to map WooCommerce categories into Domodi.pl categories. In case when above list is empty, go to "WooCommerce -> Price comparison" settings and update the list.', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'domodi_producer', __( 'Producer', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'domodi_producter_desc', __( 'Short name of the producer, i.e "Sony"', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'domodi_cat_man_code', __( 'Manufacturer code', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'domodi_size_attribute', __( 'Size attribute slug', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'domodi_color_attribute', __( 'Color atribute slug', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'settings_and_categories_saved_domodi', __( 'Updated categories list for Domodi.pl. Price comparison sites settings saved.', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'xml_update_success_domodi', __( 'Domodi.pl XML file successfully updated.', 'woocommerce-ceneo-nokaut-integration' ) );

        // allani properties
        $this->add('allani_settings', __( 'Allani settings', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'allani_product_exclude', __( 'Allani.pl exclusion', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'allani_cat_description_exclude', __( 'Exclude this category from the XML file prepared for Allani.pl', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'allani_categories_list', __( 'Allani.pl category', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'allani_product_description_exclude', __( 'Exclude this product from the XML file prepared for Allani.pl', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'allani_cat_list_desc', __( 'It\'s highly recommended to map WooCommerce categories into Allani.pl categories. In case when above list is empty, go to "WooCommerce -> Price comparison" settings and update the list.', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'allani_producer', __( 'Producer', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'allani_producter_desc', __( 'Short name of the producer, i.e "Sony"', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'allani_cat_man_code', __( 'Manufacturer code', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'allani_size_attribute', __( 'Size attribute slug', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'allani_color_attribute', __( 'Color atribute slug', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'settings_and_categories_saved_allani', __( 'Updated categories list for Allani.pl. Price comparison sites settings saved.', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'allani_gender_attribute', __('Gender', 'woocommerce-ceneo-nokaut-integration'));
        $this->add( 'allani_gender', __('Gender', 'woocommerce-ceneo-nokaut-integration'));
        $this->add( 'xml_update_success_allani', __( 'Allani.pl XML file successfully updated.', 'woocommerce-ceneo-nokaut-integration' ) );
        $this->add( 'allani_domodi_tracking_code', __('Include Allani/Domodi tracking code', 'woocommerce-ceneo-nokaut-integration') );

        // general options
        $this->add( 'alternative_description', __('Alternative description', 'woocommerce-ceneo-nokau-integration') );
      }

    /**
     * Method adds the translation entry into an array
     * @param string $identifier
     * @param string $translation - this should be a value returned by __() function
     * @throws Exception
     */
    private function add( $identifier, $translation ) {

        if ( isset( $this->translations[$identifier] ) ) {
            throw new Exception( 'Translation identifier "' . $identifier . '" already exists' );
        }

        $this->translations[$identifier] = $translation;
    }

    /**
     * Makes it possible to get the translation from a main set using translation index/key
     * @param string $key
     * @return string
     * @throws Exception
     */
    public function get_translation( $key ) {

        if ( !isset( $this->translations[$key] ) ) {
            throw new Exception( 'There\'s no translation for given key: ' . $key );
        }

        return $this->translations[$key];
    }
}
