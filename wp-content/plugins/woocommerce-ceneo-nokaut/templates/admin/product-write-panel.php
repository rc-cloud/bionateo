<?php
/**
 * Template displays a write panel options which are available under "External source" tab
 * @param string $tab_id
 * @param array groups
 */
?>

<div id="<?php print $tab_id; ?>" class="panel woocommerce_options_panel">
    <?php foreach( $groups as $group ): ?>
        <div class="options_group">
            <?php foreach( $group as $item ): ?>
                <?php switch( $item['type'] ) {

                    // just a header of current group
                    case 'header': ?>
                        <h3 style="margin-left: 10px;"><?php print $item['label']; ?></h3><?php
                        break;

                    // render regular checkbox
                    case 'checkbox':
                        woocommerce_wp_checkbox( $item );
                        break;

                    // render regular select list
                    case 'select':
                        woocommerce_wp_select( $item );
                        break;
                        
                    // render textarea
                    case 'textarea':
                        woocommerce_wp_textarea_input( $item );
                        break;

                    // render text input fields with ceneo properties
                    case 'section_properties':
                        foreach( $item['data'] as $section_id => $attributes ): ?>
                            <section class="pcsi-section <?php $section_id !== $item['property_type'] && print 'hidden'; ?>"
                                     id="<?php print $section_id; ?>">
                                <?php $this->render( 'admin/parts/properties', array(
                                    'attributes' => $attributes,
                                    'identifier' => $item['id'],
                                    'meta_values' => isset( $item['meta_values'][$section_id] ) ? $item['meta_values'][$section_id] : array(),
                                    'section' => $section_id
                                ) ); ?>
                            </section>
                        <?php endforeach;
                        break;

                    // render text input fields with nokaut properties
                    case 'regular_properties':
                        $this->render( 'admin/parts/properties', array(
                            'attributes' => $item['data'],
                            'identifier' => $item['id'],
                            'meta_values' => $item['meta_values']
                        ) );
                        break;

                    case 'text':
                        woocommerce_wp_text_input( $item );
                        break;
                        
                    case 'info':
                        echo '<p>';
                        echo $item['content'];
                        echo '</p>';
                        break;
                } ?>
            <?php endforeach; ?>
        </div>
    <?php endforeach; ?>
</div>