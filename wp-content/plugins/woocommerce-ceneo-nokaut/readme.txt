WooCommerce Ceneo.pl / Nokaut.pl Integration
============================================

Generates XML files which can be used for shop integration on Ceneo.pl and Nokaut.pl

Installation:
-------------
* Activate the plugin
* Go to "WooCommerce => Price comparison" setting page and run category update for all sites
* On same page, map the variation attributes for Ceneo.pl
* Go to "Product => Categories". For each category you can:
    * exclude the category from XML files
    * map the WooCommerce categories into site categories
* Go to "Products". For each product you can edit the settings under "Price comparison" tab:
    * exclude the product from XML files
    * set properties
* XML files are generated each three hours. Current status and URL to XML file can be found under "WooCommerce => Price comparison" setting page
* You must have openssl module installed and enabled in your server to properly update categories. You may need to "have allow_url_fopen" and "allow_url_include" enabled too.
* If for some reason category update is not working, you can update those by choosing manual upload for each site. Download proper file, choose it from hard drive and run update.
