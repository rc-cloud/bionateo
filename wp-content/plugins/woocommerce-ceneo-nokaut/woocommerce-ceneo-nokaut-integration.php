<?php
/*
Plugin Name: WooCommerce Ceneo.pl / Nokaut.pl Integration |  VestaThemes.com
Description: Generates XML files which can be used for shop integration on Ceneo.pl, Nokaut.pl and Domodi.pl
Version: 1.3.1
Author: OptArt | Piotr Szczygiel
Author URI: http://www.optart.biz
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action( 'init', function() {
    if ( class_exists( 'woocommerce' ) ) {
        new Woocommerce_Cn_Integration();
    }
} );

/**
 * Plugin-starter class
 */
class Woocommerce_Cn_Integration {

    /**
     * Module properties
     */
    private $admin,
            $settings_page;

    /**
     * A path to plugin file
     * @var string|null
     */
    private static $plugin_file = null;

    /**
     * Default constructor
     */
    public function __construct() {

        self::load_classes();
        $this->admin = new Cn_Admin( self::get_plugin_file() );
        $this->settings_page = new Cn_Admin_Settings_Page( self::get_plugin_file() );

        $this->run_modules();
        $this->add_tracking_codes();
    }

    /**
     * Retuns the path to plugin file
     * @return string
     */
    public static function get_plugin_file() {

        if ( is_null( self::$plugin_file ) ) {
            self::$plugin_file = __FILE__;
        }

        return self::$plugin_file;
    }

    /**
     * class loader method
     */
    public static function load_classes() {

        // vendor classes
        require_once('classes/vendor/abstract-class-wp-helpers.php');
        require_once('classes/vendor/woocommerce-extended-categories/class-woocommerce-extended-categories.php');
        require_once('classes/vendor/class-wp-templater.php');

        // plugin classes
        require_once('classes/abstract-class-common.php');
        require_once('classes/final-class-translations.php');
        require_once('classes/class-admin.php');
        require_once('classes/class-admin-settings-page.php');

        // generator classes
        require_once('classes/sites/abstract-class-site.php');
        require_once('classes/sites/class-ceneo.php');
        require_once('classes/sites/class-nokaut.php');
        require_once('classes/sites/class-domodi.php');
        require_once('classes/sites/class-allani.php');
    }

    /**
     * Method runs all the modules included in this plugin
     */
    private function run_modules() {

        $this->admin->_run();
        $this->settings_page->_run();
    }
    
    private function add_tracking_codes() {
        if( is_admin() ) return;
        
        $track = Cn_Common::get_setting_option(Cn_Common::ALLANI_DOMODI_TRACKING_CODE, 'False');
        if( $track != 'True') return;
        
        // Add to every page
        add_action('wp_enqueue_scripts', function() {
           // Must be added to every page
           wp_enqueue_script('allani-domodi-tracking-code', '//allani.pl/assets/tracker_async.js', array(), false, true );
           
           // Must be added on order submit
           if( is_wc_endpoint_url( 'order-received' ) ) {
               global $wp;
               $order_id = $wp->query_vars['order-received'];
               $already_submitted = get_post_meta($order_id, 'allani_domodi_tracking_code_submitted', true);
            //   if( !empty($already_submitted) ) return;
               update_post_meta($order_id, 'allani_domodi_tracking_code_submitted', true);
               
               wp_enqueue_script('allani-domodi-tracking-code-thankyou', 
                                 plugin_dir_url( __FILE__ ) . 'assets/js/thankyou.js', 
                                 array('allani-domodi-tracking-code', 'jquery'), false, true );

               $order_id = $wp->query_vars['order-received'];
               $order = new WC_Order($order_id);

               $items = $order->get_items();
               $products = array();
               foreach( $items as $item ) {
                   if( is_a( $item, 'WC_Order_Item_Product' ) ) {
                       $id = $item->get_product_id();
                       $qty = $item->get_quantity();
                       while($qty-->0) {
                           $products[] = $id;
                       }
                   }
               }
               $products = implode($products, ', ');
               
               wp_localize_script('allani-domodi-tracking-code-thankyou', 'tracking_data', array(
                    'order_id' => $order_id,
                    'total' => $order->get_total(),
                    'products' => array($products) // YES, it is a string wrapped in an array. Why? Dunno.
               ));
               
               
           }
        });
        
    }
    
    private static function remove_12x_prop_prefix($val) {
        if( $val == '0' ) return '';
        
        // We need to remove everything before 3rd "_" symbol
        for($i=0; $i<3; $i++) {
            $index = strpos($val, "_");
            if( $index === false) continue;
            $val = substr($val, $index+1);
        }
    	return $val;    
    }
    
    // Migration 1.2.4 -> 1.3.0
    public static function migration124to130() {
        // Attributes
        $attribute_taxonomies = wc_get_attribute_taxonomies();
        $settings = get_option('woocommerce_price_comparison_sites_integration');
        $options = array();
    	foreach ( $attribute_taxonomies as $attribute ) {
    
            $name = 'attribute_ceneo_' . $attribute->attribute_name;
            
            if( !isset($settings[$name]) ) continue;
            
            $val = $settings[$name];

    	    $options[$attribute->attribute_id] = array(
    	        'mapping' => self::remove_12x_prop_prefix($val),
    	        'exclusion' => false
    	    );
    	    
    	    unset($settings[$name]);
    	}
    	
    	if( !empty($options) ) {
        	update_option('ceneo_attributes_settings', $options);
    	}
    	
    	// SKU
    	foreach( array('ceneo', 'nokaut') as $site ) {
        	if( isset($settings[$site.'_SKU_mapping']) ) {
        	    update_option($site.'_SKU_mapping', self::remove_12x_prop_prefix($settings[$site.'_SKU_mapping']));
        	    unset($settings[$site.'_SKU_mapping']);
        	}
    	}
    	
    	update_option('woocommerce_price_comparison_sites_integration', $settings);
    }
}

add_filter( 'cron_schedules', function( $schedules ) {
    $schedules['price_comparison'] = array(
        'interval' => 60*60*3, // each three hours
        'display' => __( 'Price comparison sites interval' )
    );
    return $schedules;
} );

/**
 * While plugin is activated we need to set up a hook which will be used as
 * a cron-job.
 */
register_activation_hook( __FILE__, function() {
    Woocommerce_Cn_Integration::load_classes();
    wp_schedule_event( time(), 'price_comparison', 'generate_price_comparison_sites_data' );
    Woocommerce_Cn_Integration::migration124to130();
});
require_once('classes/class-generator.php');

/**
 * Deactivate cron-job hook when deactivating the plugin
 */
register_deactivation_hook( __FILE__, function() {
	wp_clear_scheduled_hook( 'generate_price_comparison_sites_data' );
});
