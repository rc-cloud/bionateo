﻿=== B2BKing — Ultimate WooCommerce Wholesale and B2B Solution ===
Plugin URI: https://codecanyon.net/item/b2bking-the-ultimate-woocommerce-b2b-plugin/26689576
Contributors: WebWizardsDev
Donate link: https://webwizards.dev
Tags: 	wholesale prices, b2bking, woocommerce b2b, woocommerce wholesale, b2b plugin for woocommerce, wholesale plugin
Author URI: webwizards.dev
Author: WebWizards
Requires at least: 4.8
Tested up to: 5.5
Requires PHP: 5.6.20
Stable tag: 2.2.5
Version: 2.2.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

**B2BKing is the complete solution for running a Wholesale, B2B or B2B + B2C hybrid store with WooCommerce.**

🏆 **Featured on Envato Market.** B2BKing has been chosen and featured on the CodeCanyon Front Page.

🚀 B2BKing is a **weekly bestseller** on Envato and one of the fastest-growing plugins of 2020.

👉 [Check out B2BKing Premium and Full Live Demo](https://codecanyon.net/item/b2bking-the-ultimate-woocommerce-b2b-plugin/26689576 "Click here for B2BKing Premium and Full Live Demo")

== B2BKing – #1 WooCommerce B2B and Wholesale Plugin ==

B2BKing takes care of everything, from basic B2B aspects such as hiding prices for guest users, to complex features such as tiered pricing structures, tax exemptions, VAT handling, and multiple buyers per account. From an extended business registration and separate B2B/B2C registration forms, to custom billing fields, an invoice payment gateway and negotiated price offers, B2BKing does it all.

[youtube https://www.youtube.com/watch?v=Xqs3NgXPQ_A]

== Features List (Free Version) ==
* Set **wholesale prices (different prices for the same product)** for each separate group (maximum 2 in free version) in the product page
* Control **available payment methods** for each b2b group, for b2c users, and for logged out users
* Control **available shipping methods** for each b2b group, for b2c users, and for logged out users
* Automatic or Manual **Registration Approval** with **registration review process**, based on Role
* Unlimited **Registration Roles** (e.g. Reseller, Factory, Distributor, etc.)
* **Discount Amount** Dynamic Rule
* **Discount Percentage** Dynamic Rule
* **Works with any theme**
* Email notifications for: New Customer Requires Approval, Your Account is Waiting for Approval, Your Account has been approved, You have a new customer registration

== B2BKing Full Premium Features ==
* Unlimited Number of Groups
* **Private Store Functionality** (Hide/Lock Prices and Store)
* **Wholesale Bulk Order Form**
* **Conversations and Messaging** between Shop and Buyers
* **Purchase Lists** that can be user to re-order/replenish stock by B2B buyers
* **Offers (Product Bundles)** created in Backend, visible for Users/Groups
* **Request a Quote** Functionality
* Powerful **Extended Registration with Custom Registration Fields** (e.g. Company Name, Custom Fields, VAT Number)
* 8 Types of Custom fields (text, number, phone, select, file upload, etc.). Fields are added to billing and checkout.
* **VAT VIES Validation (for EU)**
* **Tax Exemption** Dynamic Rules
* **Invoice Payment Gateway**
* **Tiered Pricing Table** in Product Page. Auto-generated and design adapts to any theme.
* **Custom Information Table** in Product Page
* **Content Visibility Restriction** Shortcode
* **Hide prices for guests** or Hide the entire website
* **Minimum and Maximum Order** Rules
* Free Shipping Rules
* **Replace Cart with Quote** System
* CSV Import Export Tool
* **Prices Excluding VAT/Tax for B2B** users (Can pay VAT/Tax in Cart)
* **Product visibility** (hide products) by group or user
* **Category visibility** (hide categories) by group or user
* **Multiple buyers on account** (Subaccounts)
* Complex permissions setup for each subaccount
* Change Currency Dynamic Rule
* Zero Tax Product Rules
* Fixed Price Rules
* VAT Exemption Rules by VAT ID / Country with Complex setups (e.g. in the EU it supports reverse VAT charge)
* **Wholesale prices** by user
* **Add Custom Taxes & Fees**
* Quote Requests for Guests
* Hide prices for specific products only
* **Payment Method Minimum Order** (e.g. Bank Transfer only for orders $1000 and up)
* Bulk Management Tools
* Condition-Based Discounts
* **Search by SKU**
* Extended Re-Order
* Required Multiple Rules (Products can be purchased in multiples of X, e.g. 6, 12, 18)
* WP Roles per B2BKing Groups
* **Conversations, Offers, Purchase Lists, Bulk Order, Subaccounts added to My Account for each B2B user, with Pre-Built Theme Design**
* Multiple users in a company can place orders or send messages. You can view who placed which order or sent which message.
* Withholding Tax (Ritenuta D’acconto)
* Force Login for Guests
* B2B Registration Shortcode
* Wholesale Order Form Shortcode
* **Many more features** and integrations across features
* **New features constantly added. Get in touch with us for pre-sales questions.**

👉 [Get B2BKing - The Ultimate WooCommerce Wholesale & B2B Plugin](https://woocommerce-b2b-plugin.com "Get B2BKing - The Ultimate WooCommerce Wholesale & B2B Plugin")

== Reviews ==
⭐⭐⭐⭐⭐

🎉 *"Easily the best solution for B2B functionality for WordPress. This one ticks all the boxes for a rich feature-set, ease of use, nice user interface, no issues or bugs whatsoever, and the professional and timely customer service."*

🎉 *"I was looking a long time for something like b2bking. It is an absolute game changer at woocommerce and b2b solutions. The support cannot be better:)"*

🎉 *"B2BKing is easy to configure, looks professional, is seemlessly integrated into an existing Wordpress & WooCommerce environment, and best of all, it has superb customer support. Excellent product and experience."*

🎉 *"Great Plug-in with all the functions a B2B shop may need. It was ideal for our Company since we use a multisite with retail and separate B2B shop that we need it to be hidden. Thank you also for the great and fast Support."*

🎉 *"This plugin is by far the most promising! In order to do what this plugin does, you have to install many plugins, which costs money and are heavy. Lite with nice and easy UI"*

🎉 *"This is the best B2B plugin in the market - but not only that the plugin is amazing - And the customer support is out of the world - I wish that every plugin authors would have 10% of the care and support - I highly recommend buying this plugin if you need to sell to vendors and B2C."*


== Installation ==
1.  Go to: Plugins > Add New > Upload > select the plugin zip and click "Install Now"
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Go to "B2BKing" in the left side menu
4. Go to "Settings -> Main Settings" and enable the plugin
5. To set wholesale prices, you can set these in each product page, for each b2b group (B2BKing->Groups->Business Groups).
6. To control payment and shipping methods, you can do so for each b2b group in B2BKing->Groups.

== Upgrade Notice ==

 = 2.2.5 =
* Initial Release

== Screenshots ==
1. Wholesale Prices
2. Admin Dashboard
3. Settings Menu
4. User Approval / Rejection
5. Product Visibility
6. Conversations
7. Custom Quote Request
8. Website Restriction for Guests
9. Groups

== Changelog ==

= 2.2.5 =
* Initial Release

== Frequently Asked Questions ==

= Is WooCommerce necessary for this plugin to work? =

Yes. This plugin requires WooCommerce

