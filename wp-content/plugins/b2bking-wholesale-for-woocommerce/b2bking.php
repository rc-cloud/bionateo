<?php
/*
/**
 * Plugin Name:       B2BKing — Ultimate WooCommerce Wholesale and B2B Solution
 * Plugin URI:        https://codecanyon.net/item/b2bking-the-ultimate-woocommerce-b2b-plugin/26689576
 * Description:       B2BKing is the complete solution for turning WooCommerce into an enterprise-level B2B e-commerce platform. Lite Version.
 * Version:           2.4.0
 * Author:            WebWizards
 * Author URI:        webwizards.dev
 * Text Domain:       b2bking
 * Domain Path:       /languages
 * WC requires at least: 3.0.0
 * WC tested up to: 4.4
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'B2BKINGLITE_DIR', plugin_dir_path( __FILE__ ) );

function b2bkinglite_activate() {
	require_once B2BKINGLITE_DIR . 'includes/class-b2bking-activator.php';
	B2bkinglite_Activator::activate();
}
register_activation_hook( __FILE__, 'b2bkinglite_activate' );

require B2BKINGLITE_DIR . 'includes/class-b2bking.php';

// Load plugin language
add_action( 'init', 'b2bkinglite_load_language');
function b2bkinglite_load_language() {
   load_plugin_textdomain( 'b2bking', FALSE, basename( dirname( __FILE__ ) ) . '/languages');
}

// Begins execution of the plugin.
function b2bkinglite_run() {
	$plugin = new B2bkinglite();
}

b2bkinglite_run();
