<?php
/**
*
*   key = u_fav
*   b = product id
*   c = check if is in array 0/1
*   d = user id
**/
function addToFav() {
    $b = $_POST['b'];
    $c = $_POST['c'];
    $d = $_POST['d'];
    $meta = array();
    $user_fav = get_user_meta( $d, 'u_fav');
    $add_text = '';
    $add_fa = '';
    if($user_fav){
        if($c == 0){
            foreach($user_fav[0] as $u){
                $meta[] = $u;
            }
            $meta[] = $b; 
            $c = 1;
            $add_fa = 'fa-heart';
            $add_text = 'Usuń z ulubionych';
            $return = update_user_meta( $d, 'u_fav', $meta);
        }else{
            foreach($user_fav[0] as $u){
                if($u != $b){
                    $meta[] = $u;
                }
            }
            $return = update_user_meta( $d, 'u_fav', $meta);
            $c = 0;
            $add_fa = 'fa-heart-o';
            $add_text = 'Dodaj do ulubionych';
        }
    }else{
        $meta[] = $b;
        $c = 1;
        $add_fa = 'fa-heart';
        $add_text = 'Usuń z ulubionych';
        $return = add_user_meta( $d, 'u_fav', $meta);
    } 
    echo json_encode(array('success' => $return, 'b' => $b,'c' => $c ,'d'=>$d,'add_fa'=>$add_fa,'add_text'=>$add_text));
    die();
}
add_action('wp_ajax_addToFav', 'addToFav');
add_action('wp_ajax_nopriv_addToFav', 'addToFav');