<?php
/**
*
*   key = u_compare
*   b = product id
*   c = user id
*   compare page id = 828
**/
function addToCompare() {
    $b = $_POST['b'];
    $c = $_POST['c'];
    $meta = array();
    $user_compare = get_user_meta( $c, 'u_compare');
    if($user_compare){
            foreach($user_compare[0] as $u){
                if($u != $b){
                    $meta[] = $u;
                }
            }
            $lenght =  count($meta);
            if($lenght > 2){
                array_splice($meta, 0, 1);
                $meta[] = $b;
            }else{
                $meta[] = $b;
            }
            $return = update_user_meta( $c, 'u_compare', $meta);
    }else{
        $meta[] = $b;
        $return = add_user_meta( $c, 'u_compare', $meta);
    }
    $id = get_user_meta( $c, 'u_compare');
    $url = get_the_permalink( 828 );
    echo json_encode(array('success' => $return, 'b' => $b,'c' => $c,'id'=> $id[0],'url' => $url ));
    die();
}
add_action('wp_ajax_addToCompare', 'addToCompare');
add_action('wp_ajax_nopriv_addToCompare', 'addToCompare');