<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.1
 */
defined('ABSPATH') || exit;

global $post, $product;
$login = is_user_logged_in();
$post_id = get_the_ID();
$user_id = get_current_user_id();
$thumb = get_the_post_thumbnail_url($post_id, 'full');
$_product = wc_get_product($post_id);
$regular_price = $_product->get_regular_price();
$sale_price = $_product->get_sale_price();
$price = wc_price($_product->get_price());
$currency = get_woocommerce_currency_symbol();
$promo = $sale_price - $regular_price;
$u_fav = get_user_meta($user_id, 'u_fav');
$data_in_fav = 0;
$promo_checkbox = get_field('promo_checkbox');
$new_checkbox = get_field('new_checkbox');
$fa_heart = 'fa-heart-o';
$fav_text = 'Dodaj do ulubionych';
if ($u_fav) {
    foreach ($u_fav[0] as $u) {
        if ($u == $post_id) {
            $data_in_fav = 1;
            $fa_heart = 'fa-heart';
            $fav_text = 'Usuń z ulubionych';
            break;
        }
    }
}
?>
<div class="single-product__top__gallery">
    <div class="single-product__top__gallery__thumbnails">
        <?php
        $i = 0;
        foreach ($product->get_gallery_image_ids() as $p) {
            if ($i == 0) {
                $img_2 = wp_get_attachment_image_src($p, 'single');
            }
            $img = wp_get_attachment_image_src($p, 'single-gallery');
            ?>
            <div class="single-product__top__gallery__thumbnails__single <?php if ($i == 0): echo 'active';
        endif; ?>" data-url="<?php echo wp_get_attachment_image_src($p, 'single')[0]; ?>">
                <img src="<?php echo $img[0]; ?>"/>
            </div>
            <?php
            $i++;
        }
        ?>
    </div>
    <div class="single-product__top__gallery__img">
        <div class="single--product__img__bar">
<?php if ($promo_checkbox): ?>
                <div class="single--product__img__bar__promo">
                    promocja
                </div>
            <?php endif; ?>
<?php if ($new_checkbox): ?>
                <div class="single--product__img__bar__new">
                    nowość
                </div>
<?php endif; ?>
        </div>
        <img src="<?php echo $img_2[0]; ?>">
    </div>
</div>
<div class="single-product__top__fav_com">
<?php if ($login): ?>
        <div class="single--product__img__compare single-product__top__fav_com__compare" data-id="<?php echo $post_id; ?>" data-user="<?php echo $user_id; ?>">
            <span class="single--product__img__compare__icon single-product__top__fav_com__compare__icon">
                <span class="single--product__img__compare__icon__s1"></span>
                <span class="single--product__img__compare__icon__s2"></span>
                <span class="single--product__img__compare__icon__s3"></span>
            </span>
            <span class="single--product__img__compare__text single-product__top__fav_com__compare__text">Porównaj produkty</span>

        </div>
        <div class="single--product__img__add_to_favorities single-product__top__fav_com__favorities" data-in="<?php echo $data_in_fav; ?>" data-user="<?php echo $user_id; ?>" data-id="<?php echo $post_id; ?>">
            <span class="single--product__img__add_to_favorities__icon fav_icon single-product__top__fav_com__favorities__icon">
                <i class="fa <?php echo $fa_heart; ?>" aria-hidden="true"></i>
            </span>
            <span class="single--product__img__add_to_favorities__text single-product__top__fav_com__favorities__text fav_text"><?php echo $fav_text; ?></span>

        </div>


<?php endif; ?>
</div>