<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
	global $product;
	$related_products = $product->get_upsell_ids();
if ( $related_products ) : ?>

	<section class="related products">
		<div class="container">
			<div class="row expanded">
							<div class="large-3 columns page-template-b2b__left">
                                <h3 class="page-template-b2b__left__subtitle">Produkty</h3>
                                <h2 class="page-template-b2b__left__title">Powiązane</h2>
                            </div>
                            <div class="large-9 columns  page-template-b2b__promo">
                                <div class="slick slick--custom">
									<?php foreach ( $related_products as $related_product ) : $extra_class = ' single--product'; ?>
										<article id="post-<?php the_ID(); ?>" <?php post_class($extra_class); ?> role="article">
										<?php
											$post_object = $related_product;

											setup_postdata( $GLOBALS['post'] =& $post_object );

											wc_get_template_part( 'content', 'product' ); ?>
										</article>
									<?php endforeach; ?>
                            </div>				
			</div>
		</div>
	</section>

<?php endif;

wp_reset_postdata();
