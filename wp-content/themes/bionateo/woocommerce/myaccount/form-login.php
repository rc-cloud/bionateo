
<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
?>

<?php wc_print_notices(); ?>

<?php do_action('woocommerce_before_customer_login_form'); ?>
<div class="container">

    <?php if (get_option('woocommerce_enable_myaccount_registration') === 'yes') : ?>



    <?php endif; ?>

    <div class="row expanded">
        <div class="columns large-4 large-offset-2">
            <h2 class="woo-title"><?php _e('Login', 'woocommerce'); ?></h2>

            <form class="woocomerce-form woocommerce-form-login login" method="post">

                <?php do_action('woocommerce_login_form_start'); ?>

                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <label for="username"><?php _e('Username or email address', 'woocommerce'); ?> <span class="required">*</span></label>
                    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" value="<?php echo (!empty($_POST['username']) ) ? esc_attr($_POST['username']) : ''; ?>" />
                </p>
                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <label for="password"><?php _e('Password', 'woocommerce'); ?> <span class="required">*</span></label>
                    <input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" />
                </p>

                <?php do_action('woocommerce_login_form'); ?>

                <p class="form-row hideborder">
                    <?php wp_nonce_field('woocommerce-login', 'woocommerce-login-nonce'); ?>
                    <input type="submit" class="woocommerce-Button button" name="login" value="<?php esc_attr_e('Login', 'woocommerce'); ?>" />
                    <label class="woocommerce-form__label woocommerce-form__label-for-checkbox inline">
                        <input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span><?php _e('Remember me', 'woocommerce'); ?></span>
                    </label>
                </p>
                <p class="woocommerce-LostPassword lost_password">
                    <a href="<?php echo esc_url(wp_lostpassword_url()); ?>"><?php _e('Lost your password?', 'woocommerce'); ?></a>
                </p>

                <?php do_action('woocommerce_login_form_end'); ?>

            </form>
        </div>

        <div class="columns large-4 end">

            <h2 class="woo-title"><?php _e('Register', 'woocommerce'); ?></h2>

            <form method="post" class="woocomerce-form woocommerce-form-login register">

                <?php do_action('woocommerce_register_form_start'); ?>

                <?php if ('no' === get_option('woocommerce_registration_generate_username')) : ?>

                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="reg_username"><?php _e('Username', 'woocommerce'); ?> <span class="required">*</span></label>
                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="reg_username" value="<?php echo (!empty($_POST['username']) ) ? esc_attr($_POST['username']) : ''; ?>" />
                    </p>

                <?php endif; ?>

                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <label for="reg_email"><?php _e('Email address', 'woocommerce'); ?> <span class="required">*</span></label>
                    <input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" value="<?php echo (!empty($_POST['email']) ) ? esc_attr($_POST['email']) : ''; ?>" />
                </p>

                <?php if ('no' === get_option('woocommerce_registration_generate_password')) : ?>

                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="reg_password"><?php _e('Password', 'woocommerce'); ?> <span class="required">*</span></label>
                        <input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password" />
                    </p>

                <?php endif; ?>

                <!-- Spam Trap -->
                <div style="<?php echo ( ( is_rtl() ) ? 'right' : 'left' ); ?>: -999em; position: absolute;"><label for="trap"><?php _e('Anti-spam', 'woocommerce'); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" autocomplete="off" /></div>

                <p class="checkbox-info"></p>

                <p class="woocommerce-form__label-for-checkbox agree-p">
                    <input class="agree" type="checkbox" name="checkbox" value="check" id="agree" />Wyrażam zgodę na przetwarzanie moich danych osobowych, tj. imię, nazwisko, adres zamieszkania, numer telefonu oraz adres e-mail przez Bionateo Sp. z o.o., ul. Chocimska 6, 62-800 Kalisz w celu realizacji zamówienia i wysyłki towaru.
                    <span class="span-agree"></span>
                <div id="agree-empty" style="display: none; color: #e74c3c; border: 2px solid #e74c3c; padding: 0.9375rem;">Musisz wyrazić zgodę, aby móc zarejestrować konto klienta</div>
                </p>

                <p class="woocomerce-FormRow form-row hideborder">
                    <?php wp_nonce_field('woocommerce-register', 'woocommerce-register-nonce'); ?>
                    <input type="submit" class="woocommerce-Button button" name="register" value="<?php esc_attr_e('Register', 'woocommerce'); ?>" onclick="if (document.getElementById('agree').checked) {
                                            return true;
                                        } else {
                                            document.getElementById('agree-empty').style.display = 'block';
                                            return false;
                                        }"/>
                </p>

                <?php do_action('woocommerce_register_form'); ?>

                <p>Ochrona Państwa danych osobowych jest dla nas bardzo ważna, dlatego obiecujemy obchodzić się z podanymi informacjami zgodnie z przepisami prawa, zachowując ich poufność. </p>

                <p>Informujemy, że wyrażając poniższą zgodę ma Pani/Pan prawo w dowolnym momencie ją wycofać, kierując informację na adres e-mail: biuro@bionateo.com. Jednocześnie informujemy, że do momentu wycofania zgody przetwarzanie danych będzie zgodne z prawem.</p>


                <?php do_action('woocommerce_register_form_end'); ?>

            </form>

        </div>

    </div>
</div>
<?php do_action('woocommerce_after_customer_login_form'); ?>

<style>
    .woocommerce .woocomerce-form .form-row {
        margin-bottom: 1.1875rem;
    }
    .woocommerce-form__label-for-checkbox.agree-p {
        position: relative;
        padding-left: 30px;
    }
    input.agree {
        width: 24px;
        height: 24px !important;
        border: 2px solid #8eb81f !important;
        display: inline-block !important;
        padding: 0 !important;
        float: left;
        cursor: pointer;
        margin-right: 5px;
        position: absolute !important;
        left: 0;
        top: 0;
    }
    .woocommerce .woocommerce-form__label-for-checkbox.agree-p .span-agree:after {
        left: -37px;
    }
    .woocommerce .woocommerce-form__label-for-checkbox.agree-p span.span-agree {
        position: absolute;
        top: 5px;
        left: 26px;
        z-index: -1;
    }
    .woocommerce .woocommerce-form__label-for-checkbox.agree-p input:checked + span.span-agree {
        left: 32px;
    }
    .woocommerce .woocommerce-form__label-for-checkbox.agree-p input:checked {
        background: transparent;
    }
    .woocommerce .woocommerce-form__label-for-checkbox.agree-p span:before {
        display: none;
    }
    .woocommerce .woocommerce-form__label-for-checkbox.agree-p input:checked + span:before {
        display: block;
    }
    .checkbox-info {
        background: #fff;
        position: relative;
        height: 3px;
    }
    .checkbox-info:before {
        position: absolute;
        content: '';
        width: 30px;
        height: 30px;
        background: #fff;
        bottom: -20px;
        left: -8px;
        z-index: -1;
    }
    .woocommerce-form__label-for-checkbox agree-p {
        position: relative;
    }
    .woocommerce-form__label-for-checkbox agree-p:before {
        position: absolute;
        content: '';
        width: 30px;
        height: 30px;
        background: #fff;
        bottom: -20px;
        left: -8px;
        z-index: -1;
    }
</style>

