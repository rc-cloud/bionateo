<?php
/**
 * Orders
 *
 * Shows orders on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/orders.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */
defined('ABSPATH') || exit;

do_action('woocommerce_before_account_orders', $has_orders);
?>

<?php if ($has_orders) : ?>
    <?php $total = $customer_orders->total; ?>
    <div class="small-12 columns">
        <div class="woocommerce-MyAccount-content__left">
            <h2 class="woocommerce-MyAccount-content__left__title">Twoje</h2>
            <h4 class="woocommerce-MyAccount-content__left__subtitle">Zamówienia</h3>
                <p class="woocommerce-MyAccount-content__left__info">Ilość zamówień: <?php echo $total; ?></p>
        </div>
    </div>
    <table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
        <thead>
            <tr>
                <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-number-of"><span class="nobr">#</span></th>
                <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-number"><span class="nobr">Zamówienie nr</span></th>
                <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-date"><span class="nobr">Data</span></th>
                <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-total"><span class="nobr">Wartość</span></th>
                <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions"><span class="nobr"></span></th>
            </tr>
        </thead>

        <tbody>

            <?php
            foreach ($customer_orders->orders as $customer_order) :
                $order = wc_get_order($customer_order);
                $item_count = $order->get_item_count();
                ?>
                <tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-<?php echo esc_attr($order->get_status()); ?> order">
                    <td class="woocommerce-orders-table__cell number"><?php echo $total; ?></td>
                    <?php foreach (wc_get_account_orders_columns() as $column_id => $column_name) : ?>
                        <?php if ('order-status' === $column_id): ?>
                        <?php else: ?>
                            <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-<?php echo esc_attr($column_id); ?>" data-title="<?php echo esc_attr($column_name); ?>">
                                <?php if (has_action('woocommerce_my_account_my_orders_column_' . $column_id)) : ?>
                                    <?php do_action('woocommerce_my_account_my_orders_column_' . $column_id, $order); ?>

                                <?php elseif ('order-number' === $column_id) : ?>
                                    <a href="<?php echo esc_url($order->get_view_order_url()); ?>">
                                        <?php echo _x('#', 'hash before order number', 'woocommerce') . $order->get_order_number(); ?>
                                    </a>

                                <?php elseif ('order-date' === $column_id) : ?>
                                    <time datetime="<?php echo esc_attr($order->get_date_created()->date('d-m-Y')); ?>"><?php echo esc_html($order->get_date_created()->date('d-m-Y')); ?></time>

                                    <?php //elseif ( 'order-status' === $column_id ) :  ?>
                                    <?php //echo esc_html( wc_get_order_status_name( $order->get_status() ) ); ?>

                                <?php elseif ('order-total' === $column_id) : ?>
                                    <?php
                                    /* translators: 1: formatted order total 2: total order items */
                                    echo $order->get_formatted_order_total();
                                    ?>

                                <?php elseif ('order-actions' === $column_id) : ?>
                                    <?php
                                    $actions = array(
                                        'pay' => array(
                                            'url' => $order->get_checkout_payment_url(),
                                            'name' => __('Pay', 'woocommerce'),
                                        ),
                                        'view' => array(
                                            'url' => $order->get_view_order_url(),
                                            'name' => 'Więcej',
                                        ),
                                        'cancel' => array(
                                            'url' => $order->get_cancel_order_url(wc_get_page_permalink('myaccount')),
                                            'name' => __('Cancel', 'woocommerce'),
                                        ),
                                    );

                                    if (!$order->needs_payment()) {
                                        unset($actions['pay']);
                                    }

                                    if (!in_array($order->get_status(), apply_filters('woocommerce_valid_order_statuses_for_cancel', array('pending', 'failed'), $order))) {
                                        unset($actions['cancel']);
                                    }

                                    if ($actions = apply_filters('woocommerce_my_account_my_orders_actions', $actions, $order)) {

                                        foreach ($actions as $key => $action) {
                                            echo '<a href="' . esc_url($action['url']) . '" class=" ' . sanitize_html_class($key) . '">' . esc_html($action['name']) . '</a>';
                                        }
                                    }
                                    ?>
                                <?php endif; ?>
                            </td>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </tr>
                <?php
                $total--;
            endforeach;
            ?>
        </tbody>
    </table>

    <?php do_action('woocommerce_before_account_orders_pagination'); ?>

        <?php if (1 < $customer_orders->max_num_pages) : ?>
        <div class="woocommerce-pagination woocommerce-pagination--without-numbers woocommerce-Pagination">
            <?php if (1 !== $current_page) : ?>
                <a class="woocommerce-button woocommerce-button--previous woocommerce-Button woocommerce-Button--previous button" href="<?php echo esc_url(wc_get_endpoint_url('orders', $current_page - 1)); ?>"><?php _e('Previous', 'woocommerce'); ?></a>
            <?php endif; ?>

            <?php if (intval($customer_orders->max_num_pages) !== $current_page) : ?>
                <a class="woocommerce-button woocommerce-button--next woocommerce-Button woocommerce-Button--next button" href="<?php echo esc_url(wc_get_endpoint_url('orders', $current_page + 1)); ?>"><?php _e('Next', 'woocommerce'); ?></a>
        <?php endif; ?>
        </div>
    <?php endif; ?>

<?php else : ?>
    <div class="woocommerce-message woocommerce-message--info woocommerce-Message woocommerce-Message--info woocommerce-info">
        <a class="woocommerce-Button button" href="<?php echo esc_url(apply_filters('woocommerce_return_to_shop_redirect', wc_get_page_permalink('shop'))); ?>">
        <?php esc_html_e('Go to the shop', 'woocommerce') ?>
        </a>
    <?php esc_html_e('No order has been made yet.', 'woocommerce'); ?>
    </div>
<?php endif; ?>

<?php do_action('woocommerce_after_account_orders', $has_orders); ?>
