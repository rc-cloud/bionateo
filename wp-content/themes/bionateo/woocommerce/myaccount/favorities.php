<?php
$user_id = get_current_user_id();
$u_fav = get_user_meta($user_id, 'u_fav');
$array = array();
foreach($u_fav[0] as $u){
    $array[] = $u;
}
$lenght =  count($array);
$args = array(
    'post_type' => 'product',
    'posts_per_page' => -1,
    'post__in' => $array,
);
$the_query = new WP_Query( $args );
?>
<div class="row expanded">
    <div class="small-12 columns">
        <div class="woocommerce-MyAccount-content__left">
            <h2 class="woocommerce-MyAccount-content__left__title">Twoje</h2>
            <h4 class="woocommerce-MyAccount-content__left__subtitle">Ulubione produkty</h3>
            <p class="woocommerce-MyAccount-content__left__info">Ilość produktów: <?php echo $lenght; ?></p>
        </div>
    </div>
    <div class="wrap-fav">
    <?php if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post(); ?>
            <article data-equalizer-watch="" id="post-<?php the_ID(); ?>" <?php post_class('large-4 medium-6 columns end single--product customHeight'); ?> role="article">					
                <?php get_template_part( 'parts/loop', 'archive' ); ?>
            </article>
        <?php endwhile; ?>	
    <?php endif; ?>
    </div>
    
</div>

