<?php 
global $tabs;

add_filter('woocommerce_variable_price_html', 'custom_variation_price', 10, 2);

 

function custom_variation_price( $price, $product ) {
     $price = '';
     $price .= wc_price($product->get_price());
     return $price;
}

function woo_remove_product_tabs( $tabs ) {
    unset( $tabs['additional_information'] );
    return $tabs;
}
function iconic_account_menu_items( $items ) {
	
	$items['favorities'] = 'Ulubione produkty';
	return $items;
	
}

function iconic_favorities_endpoint_content() {
    get_template_part( '/woo/myaccount/favorities' );
}
 
add_action( 'woocommerce_account_favorities_endpoint', 'iconic_favorities_endpoint_content' );



function wpb_woo_my_account_order() {
	$myorder = array(
		'dashboard' => __( 'Dashboard', 'woocommerce' ),
		'orders' => __( 'Orders', 'woocommerce' ),
		'downloads' => __( 'Downloads', 'woocommerce' ),
		'edit-address' => __( 'Addresses', 'woocommerce' ),
		'payment-methods' => 'Metody płatności',
		'edit-account' => 'Ustawienia',
		'favorities' => 'Ulubione produkty',
		'customer-logout' => __( 'Logout', 'woocommerce' ),
	);
	return $myorder;
   }
   add_filter ( 'woocommerce_account_menu_items', 'wpb_woo_my_account_order' );
	
add_filter( 'woocommerce_account_menu_items', 'iconic_account_menu_items', 10, 1 );

function iconic_add_my_account_endpoint() {
	
	add_rewrite_endpoint( 'favorities', EP_PAGES );
	
}
	
add_action( 'init', 'iconic_add_my_account_endpoint' );

add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
function woo_rename_tabs( $tabs ) {

	$tabs['description']['title'] = __( 'Opis produktu' );		// Rename the description tab
	$tabs['reviews']['title'] = __( 'Opinie' );				// Rename the reviews tab

	return $tabs;

}

//woocommerce supprot
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

//Remove each style one by one - u can select
add_filter( 'woocommerce_enqueue_styles', 'jk_dequeue_styles' );
function jk_dequeue_styles( $enqueue_styles ) {
	unset( $enqueue_styles['woocommerce-general'] );	// Remove the gloss
	//unset( $enqueue_styles['woocommerce-layout'] );		// Remove the layout
	//unset( $enqueue_styles['woocommerce-smallscreen'] );	// Remove the smallscreen optimisation
	return $enqueue_styles;
}

//custom hook
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

add_action('woocommerce_before_main_content', 'joints_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'joints_theme_wrapper_end', 10);

function joints_theme_wrapper_start() {
  echo '<div class="container"><div id="inner-content" class="row expanded">';
}

function joints_theme_wrapper_end() {
  echo '</div></div>';
}