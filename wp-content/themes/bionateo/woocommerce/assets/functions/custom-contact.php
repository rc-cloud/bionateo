<?php

function removePluginScripts(){
	// if (class_exists('WPCF7')) {
	// 		 wp_dequeue_script( 'jquery-form' );
	// 		 wp_dequeue_script('contact-form-7');
	// 		 wp_dequeue_script('jquery-ui-datepicker');
	// 		 wp_dequeue_script('jquery-ui-spinner');
	// 		 wp_dequeue_script('jquery-ui-smoothness');
	// 		wp_dequeue_style( 'contact-form-7' );
	// }
	// if (is_page(666)) include_contact_form_7_scripts();
}

function include_contact_form_7_scripts(){
			wp_enqueue_script( 'jquery-form' );
			wp_enqueue_script('contact-form-7');
			wp_enqueue_script('jquery-ui-datepicker');
			wp_enqueue_script('jquery-ui-spinner');
			wp_enqueue_script('jquery-ui-smoothness');
			wp_enqueue_style( 'contact-form-7' );
}
add_action( 'wp_enqueue_scripts', 'removePluginScripts', 100 );

//Tylko, jeżeli strona nie istnieje
if (!get_page(666)) {
	$defaultContactContent = '';
	//Wycigamy wszystkie formularze dostępne na stronie (zazwyczaj w momencie startu jest to tylko jedna strona.
	$args = array('post_type' => 'wpcf7_contact_form', 'posts_per_page' => -1);
	if( $cf7Forms = get_posts( $args ) )
		foreach($cf7Forms as $cf7Form) $defaultContactContent = '[contact-form-7 id="'.$cf7Form->ID.'" title="'.($cf7Form->post_title).'"]';

	//Przygotowujemy treść i ustawienia podstrony.
	$my_post = array(
	  'post_title'    => wp_strip_all_tags('Kontakt'),
	  'post_content'  => $defaultContactContent,
	  'post_status'   => 'publish',
	  'post_type' => 'page',
	  'post_author'   => 1,
	  'import_id' => 666
	);
	// Umieszczamu informację w bazie danych.
	wp_insert_post( $my_post );
}
