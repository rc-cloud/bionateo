<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */
defined('ABSPATH') || exit;

global $product;

$login = is_user_logged_in();
$post_id = get_the_ID();
$user_id = get_current_user_id();
$thumb = get_the_post_thumbnail_url($post_id, 'full');
$_product = wc_get_product($post_id);
$regular_price = $_product->get_regular_price();
$sale_price = $_product->get_sale_price();
$price = wc_price($_product->get_price());
$currency = get_woocommerce_currency_symbol();
$promo = $sale_price - $regular_price;
$u_fav = get_user_meta($user_id, 'u_fav');
$data_in_fav = 0;
$promo_checkbox = get_field('promo_checkbox');
$new_checkbox = get_field('new_checkbox');
$fa_heart = 'fa-heart-o';
$fav_text = 'Dodaj do ulubionych';
if ($u_fav) {
    foreach ($u_fav[0] as $u) {
        if ($u == $post_id) {
            $data_in_fav = 1;
            $fa_heart = 'fa-heart';
            $fav_text = 'Usuń z ulubionych';
            break;
        }
    }
}
?>
<div class="columns medium-2 home-products-col" data-equalizer-watch>
    <div class="single--product__img">
        <a class="img-link" rel="nofollow" href="<?php the_permalink(); ?>"></a>
        <div class="single--product__img__bar">
            <?php if ($promo_checkbox): ?>
                <div class="single--product__img__bar__promo">
                    promocja
                </div>
            <?php endif; ?>
            <?php if ($new_checkbox): ?>
                <div class="single--product__img__bar__new">
                    nowość
                </div>
            <?php endif; ?>
        </div>
        <img class="single--product__img__image" alt="<?php the_title(); ?>" src="<?php echo $thumb; ?>">
        <?php if ($login): ?>
            <div class="single--product__img__compare" data-id="<?php echo $post_id; ?>" data-user="<?php echo $user_id; ?>">
                <span class="single--product__img__compare__text">Porównaj produkty</span>
                <span class="single--product__img__compare__icon">
                    <span class="single--product__img__compare__icon__s1"></span>
                    <span class="single--product__img__compare__icon__s2"></span>
                    <span class="single--product__img__compare__icon__s3"></span>
                </span>
            </div>
            <div class="single--product__img__add_to_favorities" data-in="<?php echo $data_in_fav; ?>" data-user="<?php echo $user_id; ?>" data-id="<?php echo $post_id; ?>">
                <span class="single--product__img__add_to_favorities__text fav_text"><?php echo $fav_text; ?></span>
                <span class="single--product__img__add_to_favorities__icon fav_icon">
                    <i class="fa <?php echo $fa_heart; ?>" aria-hidden="true"></i>
                </span>
            </div>
        <?php endif; ?>
        <a rel="nofollow" href="<?php the_permalink() ?>" class="single--product__img__add_to_cart" data-id="<?php echo $post_id; ?>">
            <span class="single--product__img__add_to_cart__text" >Zobacz produkt</span>
            <span class="single--product__img__add_to_cart__spinner"><i class="fa fa-spinner" aria-hidden="true"></i><span>
                    </a>
                    </div>
                    <a href="<?php the_permalink(); ?>"><h2 class="single--product__title"><?php the_title(); ?></h2></a>
                    <div class="single--product__prices">
                        <span class="single--product__prices__regular"><?php if ($product->get_price_html() && $product->is_type('variable')) {
            echo 'od';
        }
        ?>
                            <?php // echo $price; ?>
                        <?php echo $product->get_price_html(); ?>
                        </span>
                        <?php if ($promo < 0): ?>
                                    <!--<span class="single--product__prices__promo"><?php echo $sale_price; ?> <?php echo $currency; ?></span>-->
<?php endif; ?>
                    </div>
                    </div>