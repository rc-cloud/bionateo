<?php
/**
 * Login form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     3.6.0
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (is_user_logged_in()) {
    return;
}
?>
<form class="woocomerce-form woocommerce-form-login login" method="post" <?php echo ( $hidden ) ? 'style="display:none;"' : ''; ?>>

<?php do_action('woocommerce_login_form_start'); ?>

    <?php echo ( $message ) ? wpautop(wptexturize($message)) : ''; ?>

    <p class="form-row__wrap">
        <label for="username">E-mail</label>
    <div class="form-row">
        <input type="text" class="input-text" name="username" id="username" placeholder="example@email.com" />
    </div>
</p>
<p class="form-row__wrap">
    <label for="password">Hasło</label>
    <span class="lost_password float-right">
        <a href="<?php echo esc_url(wp_lostpassword_url()); ?>">Zapomniałeś?</a>
    </span>
<div class="form-row">
    <input class="input-text" type="password" name="password" id="password" placeholder="pass" />
</div>

</p>
<div class="clear"></div>

<?php do_action('woocommerce_login_form'); ?>

<p class="form-row__wrap">
<?php wp_nonce_field('woocommerce-login'); ?>
    <button type="submit" class="button button-b button-white" name="login" value="Zaloguj się">Zaloguj się</button>
    <?php $profile_url = get_permalink(get_option('woocommerce_myaccount_page_id')); ?>
<!--<small>Nie masz jeszcze konta? <a href="<?php echo $profile_url; ?>">załóż je</a></small>-->
    <input type="hidden" name="redirect" value="<?php echo esc_url($redirect) ?>" />

</p>

<div class="clear"></div>

<?php do_action('woocommerce_login_form_end'); ?>

</form>