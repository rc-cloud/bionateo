
<?php
/**
 * Checkout billing information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-billing.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 * @global WC_Checkout $checkout
 */
defined('ABSPATH') || exit;
?>
<div class="woocommerce-billing-fields">
    <?php if (wc_ship_to_billing_address_only() && WC()->cart->needs_shipping()) : ?>

        <h3><?php _e('Billing &amp; Shipping', 'woocommerce'); ?></h3>

    <?php else : ?>

        <h3><?php _e('Billing details', 'woocommerce'); ?></h3>

    <?php endif; ?>

    <?php do_action('woocommerce_before_checkout_billing_form', $checkout); ?>

    <div class="woocommerce-billing-fields__field-wrapper">
        <?php
        $fields = $checkout->get_checkout_fields('billing');

        foreach ($fields as $key => $field) {
            if (isset($field['country_field'], $fields[$field['country_field']])) {
                $field['country'] = $checkout->get_value($field['country_field']);
            }
            woocommerce_form_field($key, $field, $checkout->get_value($key));
        }
        ?>
    </div>

    <?php do_action('woocommerce_after_checkout_billing_form', $checkout); ?>
</div>

<?php if (!is_user_logged_in() && $checkout->is_registration_enabled()) : ?>
    <div class="woocommerce-account-fields">
        <?php if (!$checkout->is_registration_required()) : ?>

            <p class="form-row form-row-wide create-account">
                <label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
                    <input class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" id="createaccount" <?php checked(( true === $checkout->get_value('createaccount') || ( true === apply_filters('woocommerce_create_account_default_checked', false) )), true) ?> type="checkbox" name="createaccount" value="1" /> <span><?php _e('Create an account?', 'woocommerce'); ?></span>
                </label>
            </p>

        <?php endif; ?>

        <?php do_action('woocommerce_before_checkout_registration_form', $checkout); ?>

        <?php if ($checkout->get_checkout_fields('account')) : ?>
            <div class="create-account" style="display: block;">

                <div style="position: relative; top: -50px;" id="if-input-checkbox"></div>
                <div class="if-input-checkbox">
                    <p>Ochrona Państwa danych osobowych jest dla nas bardzo ważna, dlatego obiecujemy obchodzić się z podanymi informacjami zgodnie z przepisami prawa, zachowując ich poufność. </p>

                    <p>Informujemy, że wyrażając poniższą zgodę ma Pani/Pan prawo w dowolnym momencie ją wycofać, kierując informację na adres e-mail: biuro@bionateo.com. Jednocześnie informujemy, że do momentu wycofania zgody przetwarzanie danych będzie zgodne z prawem.</p>

                    <p class="woocommerce-form__label-for-checkbox agree-p">
                        <input class="agree" type="checkbox" name="checkbox" value="check" id="agree" />Wyrażam zgodę na przetwarzanie moich danych osobowych, tj. imię, nazwisko, adres zamieszkania, numer telefonu oraz adres e-mail przez Bionateo Sp. z o.o., ul. Chocimska 6, 62-800 Kalisz w celu realizacji zamówienia i wysyłki towaru.
                        <span class="span-agree"></span>
                        <span id="agree-empty" style="display: none; color: #e74c3c; border: 2px solid #e74c3c; padding: 0.7375rem; margin-top: 20px;">Musisz wyrazić zgodę, aby móc zarejestrować konto klienta</span>
                    </p>

                </div>

                <p id="account_password_field" data-priority="" class="validate-required woocommerce-invalid woocommerce-invalid-required-field">
                    <label for="account_password" class="">Hasło <abbr class="required" title="pole wymagane">*</abbr></label>
                <p class="form-row">
                    <input class="input-text " name="account_password" id="account_password" placeholder="Hasło" value="" type="password">
                </p>
                </p>
                <div class="clear"></div>
            </div>

        <?php endif; ?>

        <?php do_action('woocommerce_after_checkout_registration_form', $checkout); ?>
    </div>
<?php endif; ?>

<script>

    jQuery(document).ready(function () {
        jQuery("#createaccount").on('click', function () {
            console.log('test');
            if (document.getElementById("createaccount").checked) {
                console.log('test2');
                document.getElementById('input-checkbox2').style.display = 'none';
            } else {
                jQuery('#input-checkbox2').show();
            }
        });
    });

</script>

<style>
    .woocommerce .create-account.form-row-wide .woocommerce-form__label-for-checkbox span:after {
        left: -35px;
    }
    .woocommerce .create-account.form-row-wide .woocommerce-form__label-for-checkbox span:before {
        left: -29px;
    }
    .woocommerce .create-account.form-row-wide .woocommerce-form__label-for-checkbox input:checked + span:after {
        left: -29px;
    }
    #agree-empty:after, #agree-empty2:after {
        display: none !important;
    }
    .woocommerce .woocomerce-form .form-row {
        margin-bottom: 1.1875rem;
    }
    .woocommerce-form__label-for-checkbox.agree-p {
        position: relative;
        clear: both;
        display: block;
        padding-left: 30px;
    }
    input.agree {
        width: 24px;
        height: 24px !important;
        border: 2px solid #8eb81f !important;
        display: inline-block !important;
        padding: 0 !important;
        float: left;
        cursor: pointer;
        margin-right: 5px;
        position: absolute !important;
        left: 0;
        top: 0;
    }
    .woocommerce .woocommerce-form__label-for-checkbox.agree-p .span-agree:after {
        left: -37px;
    }
    .woocommerce .woocommerce-form__label-for-checkbox.agree-p span.span-agree {
        position: absolute;
        top: 5px;
        left: 26px;
        z-index: -1;
    }
    .woocommerce .woocommerce-form__label-for-checkbox.agree-p input:checked + span.span-agree {
        left: 32px;
    }
    .woocommerce .woocommerce-form__label-for-checkbox.agree-p input:checked {
        background: transparent;
    }
    .woocommerce .woocommerce-form__label-for-checkbox.agree-p span:before {
        display: none;
    }
    .woocommerce .woocommerce-form__label-for-checkbox.agree-p input:checked + span:before {
        display: block;
    }
    .checkbox-info {
        background: #fff;
        position: relative;
        height: 3px;
    }
    .checkbox-info:before {
        position: absolute;
        content: '';
        width: 30px;
        height: 30px;
        background: #fff;
        bottom: -20px;
        left: -8px;
        z-index: -1;
    }
    .woocommerce-form__label-for-checkbox agree-p {
        position: relative;
    }
    .woocommerce-form__label-for-checkbox agree-p:before {
        position: absolute;
        content: '';
        width: 30px;
        height: 30px;
        background: #fff;
        bottom: -20px;
        left: -8px;
        z-index: -1;
    }
    .if-input-checkbox2 {
        clear: both;
    }
</style>
