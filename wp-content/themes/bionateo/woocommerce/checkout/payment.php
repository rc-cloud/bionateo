<?php
/**
 * Checkout Payment Section
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/payment.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.3
 */
defined('ABSPATH') || exit;

if (!is_ajax()) {
    do_action('woocommerce_review_order_before_payment');
}
?>
<div id="payment" class="woocommerce-checkout-payment">
    <?php if (WC()->cart->needs_payment()) : ?>
        <ul class="wc_payment_methods payment_methods methods">
            <?php
            if (!empty($available_gateways)) {
                foreach ($available_gateways as $gateway) {
                    wc_get_template('checkout/payment-method.php', array('gateway' => $gateway));
                }
            } else {
                echo '<li class="woocommerce-notice woocommerce-notice--info woocommerce-info">' . apply_filters('woocommerce_no_available_payment_methods_message', WC()->customer->get_billing_country() ? __('Sorry, it seems that there are no available payment methods for your state. Please contact us if you require assistance or wish to make alternate arrangements.', 'woocommerce') : __('Please fill in your details above to see available payment methods.', 'woocommerce') ) . '</li>';
            }
            ?>
        </ul>
    <?php endif; ?>
    <div class="form-row place-order">
        <noscript>
        <?php _e('Since your browser does not support JavaScript, or it is disabled, please ensure you click the <em>Update Totals</em> button before placing your order. You may be charged more than the amount stated above if you fail to do so.', 'woocommerce'); ?>
        <br/><input type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="<?php esc_attr_e('Update totals', 'woocommerce'); ?>" />
        </noscript>

        <?php wc_get_template('checkout/terms.php'); ?>

        <div style="position: relative; top: -50px;" id="if-input-checkbox2"></div>
        <div class="if-input-checkbox" id="input-checkbox2">
            <p class="woocommerce-form__label-for-checkbox agree-p">
                <input class="agree" type="checkbox" name="checkbox" value="check" id="agree2" />Wyrażam zgodę na przetwarzanie moich danych osobowych, tj. imię, nazwisko, adres zamieszkania, numer telefonu oraz adres e-mail przez Bionateo Sp. z o.o., ul. Chocimska 6, 62-800 Kalisz w celu realizacji zamówienia i wysyłki towaru.
                <span class="span-agree"></span>
                <span id="agree-empty2" style="display: none; color: #e74c3c; border: 2px solid #e74c3c; padding: 0.7375rem; margin-top: 20px;">Musisz wyrazić zgodę, aby złożyć zamówienie</span>
            </p>

        </div>

        <?php do_action('woocommerce_review_order_before_submit'); ?>

        <?php echo apply_filters('woocommerce_order_button_html', '<input type="submit" class="button button-b button-green alt" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr($order_button_text) . '" data-value="' . esc_attr($order_button_text) . '" onclick="  if(document.getElementById(\'createaccount\').checked) {
        if(document.getElementById(\'agree\').checked) {
            document.getElementById(\'agree-empty\').style.display = \'none\';
            return true;
        } else {
            document.getElementById(\'agree-empty\').style.display = \'block\';
            document.getElementById(\'if-input-checkbox\').scrollIntoView();
            return false;
        }
    } else {
        if(document.getElementById(\'agree2\').checked) {
            document.getElementById(\'agree-empty2\').style.display = \'none\';
            return true;
        } else {
            document.getElementById(\'agree-empty2\').style.display = \'block\';
            document.getElementById(\'if-input-checkbox2\').scrollIntoView();
            return false;
        }
    }" />'); ?>

        <?php do_action('woocommerce_review_order_after_submit'); ?>

        <?php wp_nonce_field('woocommerce-process_checkout'); ?>
    </div>
</div>
<?php
if (!is_ajax()) {
    do_action('woocommerce_review_order_after_payment');
}
