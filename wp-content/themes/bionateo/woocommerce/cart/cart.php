<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */
defined('ABSPATH') || exit;

wc_print_notices();

do_action('woocommerce_before_cart');
?>
<div class="small-12 columns">
    <a class="single-product__top__back" href="<?php echo get_the_permalink(773); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/arrow-back.png"/> Wróć do zakupów</a>
</div>
<form class="woocommerce-cart-form" action="<?php echo esc_url(wc_get_cart_url()); ?>" method="post">
<?php do_action('woocommerce_before_cart_table'); ?>

    <table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
        <tbody>
            <?php do_action('woocommerce_before_cart_contents'); ?>

            <?php
            foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
                $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
                $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

                if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
                    $product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key);
                    ?>
                    <tr class="woocommerce-cart-form__cart-item <?php echo esc_attr(apply_filters('woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key)); ?>">

                        <td class="product-thumbnail">
                            <?php
                            $img_2 = wp_get_attachment_image_src($_product->get_image_id(), 'single');
                            ?>
                            <div class="product-thumbnail__img">
                                <img src="<?php echo $img_2[0]; ?>"/>
                            </div>
                        </td>

                        <td class="product-name" data-title="<?php esc_attr_e('Product', 'woocommerce'); ?>">
                            <?php
                            $v_array = array();
                            foreach ($cart_item['variation'] as $v) {
                                $v_array[] = $v;
                            }
                            ?>

                            <h3 class="product-name__category"><?php echo get_the_terms($product_id, 'product_cat')[0]->name; ?></h3>
                            <h2 class="product-name__title"><a href="<?php echo esc_url($product_permalink); ?>"><?php echo $_product->get_title() ?></a></h2>
        <?php if ($v_array): ?>
                                <div class="product-name__variations">
                                    <div class="product-name__variations__single">
                                        Rozmiar: <?php echo $v_array[0] ?>
                                    </div>
                                    <div class="product-name__variations__single">
                                        Stężenie: <?php echo $v_array[1] ?>
                                    </div>
                                </div>
        <?php endif; ?>
                        </td>

                        <td class="product-price" data-title="<?php esc_attr_e('Price', 'woocommerce'); ?>">
                            <h6 class="shop_table__title">Cena za sztukę</h6>
                            <?php
                            echo apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key);
                            ?>
                        </td>

                        <td class="product-quantity" data-title="<?php esc_attr_e('Quantity', 'woocommerce'); ?>">
                            <h6 class="shop_table__title">Ilość</h6>
                            <?php
                            if ($_product->is_sold_individually()) {
                                $product_quantity = sprintf('1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key);
                            } else {
                                $product_quantity = woocommerce_quantity_input(array(
                                    'input_name' => "cart[{$cart_item_key}][qty]",
                                    'input_value' => $cart_item['quantity'],
                                    'max_value' => $_product->get_max_purchase_quantity(),
                                    'min_value' => '0',
                                        ), $_product, false);
                            }

                            echo apply_filters('woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item);
                            ?>
                        </td>

                        <td class="product-subtotal" data-title="<?php esc_attr_e('Total', 'woocommerce'); ?>">
                            <h6 class="shop_table__title">Cena razem</h6>
                            <?php
                            echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key);
                            ?>
                        </td>
                        <td class="product-remove">
                            <?php
                            echo apply_filters('woocommerce_cart_item_remove_link', sprintf(
                                            '<a href="%s" class="remove product-remove__url" aria-label="%s" data-product_id="%s" data-product_sku="%s"></a>', esc_url(WC()->cart->get_remove_url($cart_item_key)), __('Remove this item', 'woocommerce'), esc_attr($product_id), esc_attr($_product->get_sku())
                                    ), $cart_item_key);
                            ?>
                        </td>
                    </tr>
                            <?php
                        }
                    }
                    ?>

            <?php do_action('woocommerce_cart_contents'); ?>

            <tr>
                <td colspan="6" class="actions">

<?php if (wc_coupons_enabled()) { ?>
                        <div class="coupon">
                            <label for="coupon_code"><?php _e('Coupon:', 'woocommerce'); ?></label> <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e('Coupon code', 'woocommerce'); ?>" /> <input type="submit" class="button button-b button-green" name="apply_coupon" value="<?php esc_attr_e('Apply coupon', 'woocommerce'); ?>" />
                        <?php do_action('woocommerce_cart_coupon'); ?>
                        </div>
                        <?php } ?>

                    <input type="submit" class="button button-b button-green button-update-cart" name="update_cart" value="<?php esc_attr_e('Update cart', 'woocommerce'); ?>" />

<?php do_action('woocommerce_cart_actions'); ?>

                    <?php wp_nonce_field('woocommerce-cart'); ?>
                </td>
            </tr>

<?php do_action('woocommerce_after_cart_contents'); ?>
        </tbody>
    </table>
            <?php do_action('woocommerce_after_cart_table'); ?>
</form>

<div class="cart-collaterals">
<?php
/**
 * woocommerce_cart_collaterals hook.
 *
 * @hooked woocommerce_cross_sell_display
 * @hooked woocommerce_cart_totals - 10
 */
do_action('woocommerce_cart_collaterals');
?>
</div>

    <?php do_action('woocommerce_after_cart'); ?>

<style>
    .cart-collaterals .cart_totals.calculated_shipping #shipping_method li:after {
        display: none;
    }
    .woocommerce #shipping_method label[for="shipping_method_0_local_pickup1"]:after {
        top: 2px;
    }
    .woocommerce #shipping_method label[for="shipping_method_0_local_pickup1"]:before {
        top: -2px;
    }
    .woocommerce #shipping_method input:checked + label[for="shipping_method_0_local_pickup1"]:after {
        top: -3px;
        left: -34px;
    }
    .shop_table:not(.my_account_orders) .product-remove__url {
        margin: 1px;
    }
</style>
