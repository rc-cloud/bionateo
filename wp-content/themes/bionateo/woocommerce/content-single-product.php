<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */
defined('ABSPATH') || exit;

function convertAccentsAndSpecialToNormal($string) {
    $table = array(
        'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Ă' => 'A', 'Ā' => 'A', 'Ą' => 'A', 'Æ' => 'A', 'Ǽ' => 'A',
        'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'ă' => 'a', 'ā' => 'a', 'ą' => 'a', 'æ' => 'a', 'ǽ' => 'a',
        'Þ' => 'B', 'þ' => 'b', 'ß' => 'Ss',
        'Ç' => 'C', 'Č' => 'C', 'Ć' => 'C', 'Ĉ' => 'C', 'Ċ' => 'C',
        'ç' => 'c', 'č' => 'c', 'ć' => 'c', 'ĉ' => 'c', 'ċ' => 'c',
        'Đ' => 'Dj', 'Ď' => 'D', 'Đ' => 'D',
        'đ' => 'dj', 'ď' => 'd',
        'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ĕ' => 'E', 'Ē' => 'E', 'Ę' => 'E', 'Ė' => 'E',
        'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ĕ' => 'e', 'ē' => 'e', 'ę' => 'e', 'ė' => 'e',
        'Ĝ' => 'G', 'Ğ' => 'G', 'Ġ' => 'G', 'Ģ' => 'G',
        'ĝ' => 'g', 'ğ' => 'g', 'ġ' => 'g', 'ģ' => 'g',
        'Ĥ' => 'H', 'Ħ' => 'H',
        'ĥ' => 'h', 'ħ' => 'h',
        'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'İ' => 'I', 'Ĩ' => 'I', 'Ī' => 'I', 'Ĭ' => 'I', 'Į' => 'I',
        'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'į' => 'i', 'ĩ' => 'i', 'ī' => 'i', 'ĭ' => 'i', 'ı' => 'i',
        'Ĵ' => 'J',
        'ĵ' => 'j',
        'Ķ' => 'K',
        'ķ' => 'k', 'ĸ' => 'k',
        'Ĺ' => 'L', 'Ļ' => 'L', 'Ľ' => 'L', 'Ŀ' => 'L', 'Ł' => 'L',
        'ĺ' => 'l', 'ļ' => 'l', 'ľ' => 'l', 'ŀ' => 'l', 'ł' => 'l',
        'Ñ' => 'N', 'Ń' => 'N', 'Ň' => 'N', 'Ņ' => 'N', 'Ŋ' => 'N',
        'ñ' => 'n', 'ń' => 'n', 'ň' => 'n', 'ņ' => 'n', 'ŋ' => 'n', 'ŉ' => 'n',
        'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ō' => 'O', 'Ŏ' => 'O', 'Ő' => 'O', 'Œ' => 'O',
        'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ō' => 'o', 'ŏ' => 'o', 'ő' => 'o', 'œ' => 'o', 'ð' => 'o',
        'Ŕ' => 'R', 'Ř' => 'R',
        'ŕ' => 'r', 'ř' => 'r', 'ŗ' => 'r',
        'Š' => 'S', 'Ŝ' => 'S', 'Ś' => 'S', 'Ş' => 'S',
        'š' => 's', 'ŝ' => 's', 'ś' => 's', 'ş' => 's',
        'Ŧ' => 'T', 'Ţ' => 'T', 'Ť' => 'T',
        'ŧ' => 't', 'ţ' => 't', 'ť' => 't',
        'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ũ' => 'U', 'Ū' => 'U', 'Ŭ' => 'U', 'Ů' => 'U', 'Ű' => 'U', 'Ų' => 'U',
        'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ũ' => 'u', 'ū' => 'u', 'ŭ' => 'u', 'ů' => 'u', 'ű' => 'u', 'ų' => 'u',
        'Ŵ' => 'W', 'Ẁ' => 'W', 'Ẃ' => 'W', 'Ẅ' => 'W',
        'ŵ' => 'w', 'ẁ' => 'w', 'ẃ' => 'w', 'ẅ' => 'w',
        'Ý' => 'Y', 'Ÿ' => 'Y', 'Ŷ' => 'Y',
        'ý' => 'y', 'ÿ' => 'y', 'ŷ' => 'y',
        'Ž' => 'Z', 'Ź' => 'Z', 'Ż' => 'Z', 'Ž' => 'Z',
        'ž' => 'z', 'ź' => 'z', 'ż' => 'z', 'ž' => 'z',
        '“' => '"', '”' => '"', '‘' => "'", '’' => "'", '•' => '-', '…' => '...', '—' => '-', '–' => '-', '¿' => '?', '¡' => '!', '°' => ' degrees ',
        '¼' => ' 1/4 ', '½' => ' 1/2 ', '¾' => ' 3/4 ', '⅓' => ' 1/3 ', '⅔' => ' 2/3 ', '⅛' => ' 1/8 ', '⅜' => ' 3/8 ', '⅝' => ' 5/8 ', '⅞' => ' 7/8 ',
        '÷' => ' divided by ', '×' => ' times ', '±' => ' plus-minus ', '√' => ' square root ', '∞' => ' infinity ',
        '≈' => ' almost equal to ', '≠' => ' not equal to ', '≡' => ' identical to ', '≤' => ' less than or equal to ', '≥' => ' greater than or equal to ',
        '←' => ' left ', '→' => ' right ', '↑' => ' up ', '↓' => ' down ', '↔' => ' left and right ', '↕' => ' up and down ',
        '℅' => ' care of ', '℮' => ' estimated ',
        'Ω' => ' ohm ',
        '♀' => ' female ', '♂' => ' male ',
        '©' => ' Copyright ', '®' => ' Registered ', '™' => ' Trademark ',
    );

    $string = strtr($string, $table);
    // Currency symbols: £¤¥€  - we dont bother with them for now
    $string = preg_replace("/[^\x9\xA\xD\x20-\x7F]/u", "", $string);

    return $string;
}

global $product;
?>

<?php
/**
 * woocommerce_before_single_product hook.
 *
 * @hooked wc_print_notices - 10
 */
do_action('woocommerce_before_single_product');

if (post_password_required()) {
    echo get_the_password_form();
    return;
}
?>

<div id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="single-product__top">
        <div class="container">
            <div class="row expanded">
                <div class="small-12 columns">
                    <a class="single-product__top__back" href="<?php echo get_the_permalink(773); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/arrow-back.png"/> Wróć do listy produktów</a>
                </div>
                <div class="large-5 columns">
<?php
/**
 * woocommerce_before_single_product_summary hook.
 *
 * @hooked woocommerce_show_product_sale_flash - 10
 * @hooked woocommerce_show_product_images - 20
 */
do_action('woocommerce_before_single_product_summary');
?>

                </div>
                <div class="large-7 columns single-product__top__right">
                    <div class="single-product__top__right__wrap">
<?php wc_get_template('single-product/meta.php'); ?>
<?php wc_get_template('single-product/title.php'); ?>
<?php if ($product->is_type('variable')) { ?>
                            <div class="price_woo_edit"></div>
<?php } else { ?>
                            <div class="price-product">
                        <?php echo $product->get_price_html(); ?>
                            </div>
                    <?php } ?>

                    <?php wc_get_template('single-product/rating.php'); ?>
                    <?php wc_get_template('single-product/short-description.php'); ?>
                        <div class="row expanded new_variations">

                    <?php
                    if ($product->is_type('variable')) {
                        $meta = $product->get_variation_attributes();
                        $i = 0;
                        foreach ($meta as $key => $m) {
                            $k = $key;
                            ?>
                                    <div class="new_variations__wrap col-<?php echo $i; ?> large-6 columns">
                                        <h6 class="new_variations__wrap__title"><?php echo $key; ?></h6>
                                <?php
                                $u = 0;
                                foreach ($m as $p) {
                                    ?>
                                            <div meta-data="select-<?php echo $i; ?>"
                                                 meta-col="get-col-<?php echo $i; ?>"
                                                 meta-name="<?php echo $p; ?>"
                                                 meta-select="<?php echo convertAccentsAndSpecialToNormal(strtolower($k)); ?>"
                                                 class="new_variations__wrap__single <?php if ($u == 0): echo 'active';
                        endif; ?> get-col get-col-<?php echo $i; ?> get-col-<?php echo $p; ?>">
                                                <span><?php echo $p; ?></span>
                                            </div>
                                        <?php
                                        $u++;
                                    }
                                    ?>
                                    </div>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                        </div>
                                <?php do_action('woocommerce_' . $product->get_type() . '_add_to_cart'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="single-product__medium">
        <div class="container">
            <div class="row expanded display-flex">
                <div class="large-7 columns product_left">
                                <?php wc_get_template('single-product/tabs/tabs.php'); ?>
                </div>
                <div class="large-4 large-offset-1 columns product_right">
                            <?php
                            if (have_rows('product_right', 'options')):
                                while (have_rows('product_right', 'options')) : the_row();
                                    $product_right_img = get_sub_field('product_right_img');
                                    $product_right_title = get_sub_field('product_right_title');
                                    $product_right_content = get_sub_field('product_right_content');
                                    ?>
                            <div class="product_right__item">
                                <div class="product_right__item__img">
                                    <img class="product_right__item__img__position" src="<?php echo $product_right_img['url']; ?>">
                                </div>
                                <div class="product_right__item__wrap">
                                    <h6 class="product_right__item__wrap__title"><?php echo $product_right_title; ?></h2>
                                        <div class="product_right__item__wrap__content"><?php echo $product_right_content; ?></div>
                                </div>
                            </div>
                            <?php
                        endwhile;
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div>
                    <?php wc_get_template('single-product/related.php'); ?>
    </div>
</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action('woocommerce_after_single_product'); ?>
