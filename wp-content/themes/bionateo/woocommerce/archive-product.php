<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */
$archive_text = get_field('archive_text', 'option');

defined('ABSPATH') || exit;

get_header('shop');
?>
<?php
//wp_reset_query();
$filtr_order_array = array(
    0 => array('name' => 'Domyślne sortowanie', 'slug' => 'random'),
    1 => array('name' => 'Nazwy rosnąco', 'slug' => 'asc'),
    2 => array('name' => 'Nazwy malejąco', 'slug' => 'desc'),
    3 => array('name' => 'Od Najnowszych', 'slug' => 'newest'),
    4 => array('name' => 'Od Najstarszych', 'slug' => 'oldest'),
    5 => array('name' => 'Ceny rosnąco', 'slug' => 'lower'),
    6 => array('name' => 'Ceny malejąco', 'slug' => 'upper'),
);
if (isset($_GET['filtr_brand_cat'])) {
    $filtr_brand_cat = explode(",", $_GET['filtr_brand_cat']);
} else {
    $filtr_brand_cat = false;
}




if (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/artykuly-spozywcze/herbata/") !== false) {
    $filtr_product_cat = "herbata";

    $desc1 = '<div class="icol-sm-8"><h2>Susz z konopi to jedna z najlepszych opcji na wartościowe herbaty ziołowe.</h2>
        <p>Przyjmowanie konopi w postaci naparu to jeden z najwygodniejszych i najmniej uciążliwych sposobów na ich dawkowanie. Herbata konopna produkowana jest z wysuszonych liści wyselekcjonowanych odmian konopi siewnych, które są w pełni legalne, uprawiane w Europie w sposób organiczny i naturalny, bez wykorzystywania nawozów sztucznych. Psychoaktywność w tego rodzaju konopiach jest bardzo niska - ze względu na niewielkie stężenie THC, przygotowany napar jest bezpieczny dla zdrowia i nie powoduje uzależnienia.
</p>
        </div>
        <div class="icol-sm-4">
        <img src="https://bionateo.com/img/herbata1.png" />
        </div>';
    $desc2 = '<div class="irow">
        <div class="icol-sm-4">
        <img src="https://bionateo.com/img/herbata2.png" />
        </div>
        <div class="icol-sm-8"><h2>Herbata z konopi - jak parzyć?</h2>
        <p>Jej regularne spożywanie pozwala na efektywne zwalczanie wielu dolegliwości i problemów zdrowotnych. Napar z konopi siewnych pomaga usunąć problem dokuczliwych migren, obniża cholesterol oraz wspomaga układ odpornościowy, dzięki czemu jest szczególnie zalecany osobom w stanie rekonwalescencji, po długich, ciężkich chorobach. Herbata z konopi pomaga też wyregulować poziom cukru we krwi oraz obniża ciśnienie krwi. Zawarte w konopiach kwasy omega-3 i omega-6 wspomagają także walkę z takimi chorobami, jak Alzheimer.
</p>
        <p>Herbatę konopną przygotowuje się jak każdą inną herbatę ziołową. Do kubka gorącej, ale nie wrzącej wody wrzucamy jedną łyżeczkę stołową suszu. Następnie pozostawiamy ją pod przykryciem do zaparzenia, na czas od pięciu do ośmiu minut. Do jej parzenia nie zaleca się używania metalowych przyrządów. Dla osób, które nie przepadają za smakiem suszonych kwiatów konopi siewnych, poleca się wykonanie mieszanki z innymi ziołami, na przykład z miętą. Na rynku dostępne są także specjalne mieszanki suszu konopnego z suszonymi owocami, co szczególnie przypadnie do gustu osobom lubującym się w naparach nieco słodszych.
</p>
        </div>
        </div>

        <div class="irow">
        <div class="icol-sm-8"><h2>Selekcja herbat z konopii najwyższej jakości</h2>
        <p>Dostępne w naszym sklepie internetowym Bionateo herbaty z konopi to produkty od renomowanych producentów. Gwarantujemy ich najwyższą jakość i wydajność. Konopie, z których wykonuje się susz na napar, są ręcznie zbierane, z ekologicznych upraw z europejskich, certyfikowanych plantacji. Kupując nasze produkty, masz pewność, że będą bezpieczne i korzystne dla twojego zdrowia i organizmu. Najwyższa jakość idzie u nas w parze z korzystną ceną - zapraszamy do zapoznania się z naszym szerokim asortymentem!</p>
        </div>
        <div class="icol-sm-4">
        <img src="https://bionateo.com/img/herbata3.jpg" />
        </div>
        </div>';
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/waporyzacja/susz-cbd/") !== false) {
    $filtr_product_cat = "susz-cbd";
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/waporyzacja/liquidy-cbd/") !== false) {
    $filtr_product_cat = "liquidy-cbd";
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/produkty-do-dezynfekcji/") !== false) {
    $filtr_product_cat = "produkty-do-dezynfekcji";
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/artykuly-spozywcze/") !== false) {
    $filtr_product_cat = "artykuly-spozywcze";
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/waporyzacja/") !== false) {
    $filtr_product_cat = "waporyzacja";
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/pozostale-suplementy/") !== false) {
    $filtr_product_cat = "pozostale-suplementy";
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/olejek-cbd/medihemp/") !== false) {
    $filtr_brand_cat = "medihemp";
    $desc1 = '<div class="icol-sm-8"><h2>Wysoka jakość najczystszych produktów</h2>
        <p>Jeżeli poszukujesz naturalnych i innowacyjnych rozwiązań dla swojego ciała i zdrowia, to trafiłeś pod właściwy adres. Sklep internetowy Bionateo posiada w swojej ofercie szereg produktów wyprodukowanych z konopi, w tym renomowane olejki CBD Medihemp. Nasz sklep powstał z myślą o najbardziej wymagających klientach, którzy postanowili zaufać niezwykłej mocy płynącej z natury. Dzięki nam masz możliwość dostępu do najlepszych artykułów oferowanych na rynkach zagranicznych. I to bez wychodzenia z domu. Sprawdź naszą atrakcyjną ofertę!</p>
        </div>
        <div class="icol-sm-4">
        <img src="https://bionateo.com/img/cg1-1.jpg" />
        </div>';
    $desc2 = '<div class="irow">
        <div class="icol-sm-4">
        <img src="https://bionateo.com/img/cg2-1.jpg" />
        </div>
        <div class="icol-sm-8"><h2>Poznaj moc olejków Medihemp</h2>
        <p>W asortymencie sklepu Bionateo znajdziesz wyłącznie całkowicie bezpieczne produkty wysokiej jakości. Doskonałym tego przykładem są dostępne w naszej ofercie olejki Medihemp, które pokochały już tysiące klientów na całym świecie. Producent jest dostawcą jednych z najczystszych artykułów wyprodukowanych z konopi siewnej, a do ich wytworzenia stosuje całe rośliny. Dzięki temu dostarczane przez niego rozwiązania są bogactwem składników odżywczych, mikroelementów i witamin, których potrzebuje Twoje ciało. Czyste zdrowie!</p>
        <p>W celu zapewnienia najwyższej jakości produktów i zagwarantowania Ci bezpieczeństwa, wszystkie nasze produkty przechodzą szereg różnorodnych i zaawansowanych badań laboratoryjnych. Dodatkowo, proponowane olejki CBD Medihemp pochodzą wyłącznie z ekologicznych upraw, gdzie są hodowane na wysokich klasach glebach, a to wszystko bez użycia pestycydów. Ściśle określone normy upraw i produkcji sprawiają, że możesz być pewien, że dokonując zakupu w sklepie internetowym Bionateo nabywasz produkt najwyższej jakości.</p>
        <p>Coraz większa popularność artykułów wyprodukowanych przy użyciu konopi siewnej sprawia, że z dnia na dzień rośnie liczba konsumentów poszukujących właśnie tych naturalnych rozwiązań. Odpowiedzią na ich potrzeby są dostępne u nas olejki CBD Medihemp. Zastanawiasz się, czym właściwie jest CBD? To kannabidiol - substancja, która jest pozyskiwana z kwiatostanów konopi, a jej wpływ na układ endokannabinoidowy został potwierdzony przez przeprowadzone zaawansowanie badania naukowe na całym świecie. Przekonaj się sam!</p>
        </div>
        </div>

        <div class="irow">
        <div class="icol-sm-8"><h2>CBD Medihemp - remedium na Twoje problemy zdrowotne</h2>
        <p>Gdy borykamy się z jakimikolwiek problemami zdrowotnymi, bardzo często naszym pierwszym odruchem jest sięgnięcie po antybiotyki bądź inne leki dostępne w aptekach. Okazuje się, że nieprzetworzone dary natury, w tym olejki CBD Medihemp, są skutecznym remedium na wiele dolegliwości. Ich właściwości prozdrowotne zostały docenione na całym świecie, a każdego dnia rośnie liczba osób, które wykorzystują je w trakcie codziennego leczenia. A to wszystko za sprawą czystych składów i niezwykłego działania na Twój organizm.</p>
        <p>Zastanawiasz się, w czym mogą pomóc Ci naturalne produkty wytworzone z nasion konopi? Są one doskonałym antydepresantem i odpowiednikiem leków przeciwbólowych. Dodatkowo, olejki CBD mają działanie przeciwzapalne, wspomagają pracę układu pokarmowego oraz pomagają w obniżeniu poziomu złego cholesterolu. Wyciąg z konopi wspomaga koncentrację, wzmacnia odporność organizmu i reguluję gospodarkę energetyczną. Jego stosowanie jest całkowicie bezpieczne, ponieważ nie uzależnia i nie posiada działania psychoaktywnego.</p>
        </div>
        <div class="icol-sm-4">
        <img src="https://bionateo.com/img/mh1.jpg" />
        </div>
        </div>';
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/olejek-cbd/cibdol/") !== false) {
    $filtr_product_cat = "olejek-cbd";
    $filtr_brand_cat = "cibdol";
    $desc1 = '<div class="icol-sm-8"><h2>Bezpieczna pomoc dla Twojego zdrowia</h2>
        <p>Niesamowita moc natury od zawsze stanowiła nie tylko ciekawostkę dla najlepszych badaczy na całym świecie, ale także była niezwykle ceniona w leczeniu niektórych chorób. Dlatego też, jeżeli poszukujesz zdrowotnych rozwiązań, to olejki Cibdol proponowane przez sklep internetowy Bionateo.com zdecydowanie powinny przykuć Twoją uwagę. Te innowacyjne i naturalne produkty z każdym dniem zdobywają coraz większa popularność na całym świecie, i nic w tym dziwnego! Okazuje się, że konopie skrywają w sobie ogromną moc.</p>
        </div>
        <div class="icol-sm-4">
        <img src="https://bionateo.com/img/cibdol1.jpg" />
        </div>';
    $desc2 = '<div class="irow">
        <div class="icol-sm-4">
        <img src="https://bionateo.com/img/cibdol2.jpg" />
        </div>
        <div class="icol-sm-8"><h2>Zaufaj wysokiej jakości olejkom Cibdol</h2>
        <p>W ofercie Bionateo znajdziesz produkty pochodzące wyłącznie z ekologicznych upraw, do których produkcji wykorzystano specjalne odmiany konopi przemysłowej. Proponowany CBD Cibdol zawiera naturalnie występujący kannabinoid, którego niesamowite działanie jest nieustannie poddawane zaawansowanym i długotrwałym badaniom naukowym. Nasi holenderscy, hiszpańscy, węgierscy czy austriaccy dostawcy uprawiają konopie na wysokiej klasy glebach, gwarantując tym samym produkty najwyższej jakości, które spełniają wszelkie standardy.</p>
        <p>Zastanawiasz się, czym tak naprawdę jest CBD, które znajduje się w oferowanych przez nas produktach? Jest to substancja, którą w składzie posiadają oczywiście także proponowane olejki Cibdol, a pozyskuje się ją z konopi siewnych. Nieustanne badania i testy każdego dnia udowadniają, że jej wpływ na zdrowie człowieka jest naprawdę zaskakujący! Pamiętaj także, że wszystkie artykuły dostępne w naszym asortymencie nie posiadają działania odurzającego i psychoaktywnego oraz w żaden sposób nie uzależniają.</p>
        <p>Nasz zespół tworzą pasjonaci innowacyjnych i skutecznych naturalnych rozwiązań, którzy dodatkowo są specjalistami w dziedzinie konopi. Dzięki obszernej wiedzy i doświadczeniu, możemy zagwarantować Ci, że proponowany olej CBD Cibdol jest produktem nie tylko wysokiej jakości, ale także całkowicie bezpiecznym dla Twojego organizmu i zdrowia, w szczególności układu endokannabinoidowego. Przed wprowadzeniem go do naszej atrakcyjnej oferty zadbaliśmy o to, aby przeszedł wiele zaawansowanych testów laboratoryjnych.</p>
        </div>
        </div>

        <div class="irow">
        <div class="icol-sm-8"><h2>CBD Cibdol - moc natury</h2>
        <p>Sklep internetowy Bionateo i jego asortyment to odpowiedź na potrzeby konsumentów, którzy poszukują alternatywnych rozwiązań leczniczych. Jak się okazuje - dostępne w naszej ofercie olejki Cibdo posiadają szereg zaskakujących właściwości zdrowotnych. Mają one działanie antydepresyjne, przeciwbólowe, wspomagające koncentrację oraz dostarczające energii. Co więcej, silnie wpływają na odporność Twojego organizmu i redukują istniejące w nim stany zapalne, które do tej pory mogły skutecznie utrudniać Ci codzienne życie.</p>
        <p>Twoje zdrowie i dobre samopoczucie to priorytet naszego zespołu, dlatego też zadbaliśmy o to, aby wszystkie oferowane przez nas artykuły były dostępne w niezwykle atrakcyjnych cenach. Dzięki oferowanym przez nas olejkom w łatwy sposób dostarczysz swojemu organizmowi wiele cennych składników odżywczych, a wśród nich: flawonoidy, terpeny, białko, nienasycone kwasy tłuszczowe Omega-3 i Omega-6 oraz wiele witamin z grupy A, B czy C. Dodatkowo, są one bogate w takie mikroelementy, jak żelazo, wapń, cynk czy magnez. Poznaj niesamowitą moc natury już dziś i wybierz produkt idealnie dopasowany do Twoich potrzeb.</p>
        </div>
        <div class="icol-sm-4">
        <img src="https://bionateo.com/img/cibdol3.jpg" />
        </div>
        </div>';
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/olejek-cbda/") !== false) {
    $filtr_product_cat = "olejek-cbda";
    $desc1 = '<div class="icol-sm-8"><h2>Kannabinoid o wielu korzystnych właściwościach zdrowotnych.</h2>
        <p>CBDa to jeden z wielu unikatowych związków pozyskiwanych z konopii. Choć CBD jest tym o wiele bardziej rozpoznawalnym, to CBDa zaczyna powoli zyskiwać na popularności. Olejek CBDa posiada liczne właściwości prozdrowotne, które stawiają go na równi z innymi produktami powstającymi w wyniku ekstrakcji legalnych konopi. Czym w zasadzie jest CBDa i czym różni się od CBD? Otóż CBDa to kwas kanabidiolowy, jedna z postaci CBD, substancja, z której CBD powstaje w wyniku przemiany. CBDa przemienia się w CBD, gdy konopie są podgrzewane - podczas palenia, gotowania, parowania. Substancja ta dominuje w produktach pozyskiwanych z konopii występujących pod postacią płynną - na przykład w formie herbaty czy soku. CBDa posiada liczne właściwości, dlatego zaczęto produkować olejek, korzystny dla zdrowia pod wieloma względami.
</p>
        </div>
        <div class="icol-sm-4">
        <img src="https://bionateo.com/img/cbda1.jpg" />
        </div>';
    $desc2 = '<div class="irow">
        <div class="icol-sm-4">
        <img src="https://bionateo.com/img/cbda2.jpg" />
        </div>
        <div class="icol-sm-8"><h2>Olejki CBDa o różnym stężeniu</h2>
        <p>Olejek CBDa ma wiele wyjątkowych właściwości prozdrowotnych. Przede wszystkim cechuje się niezwykle silnym działaniem przeciwzapalnym, pozwalając zwalczyć stany zapalne odpowiadające za rozwój wielu niebezpiecznych chorób: między innymi miażdżycy, Alzheimera, astmy. CBDa jako substancja pochodna pozyskiwana z konopii odznacza się także silnym działaniem antyproliferacyjnym, hamującym rozwój komórek rakowych, zwłaszcza raka piersi. CBDa ma także silne działanie antybakteryjne. Kannabinoid ten łagodzi również nudności i wspomaga hamowanie wymiotów. Niezwykłe działanie tego naturalnego remedium czyni go coraz bardziej popularnym.
</p>
        <p>W naszej, bogatej ofercie znajdują się produkty pozyskiwane z legalnych konopi tylko najwyższej jakości. Posiadamy olejki CBDa o różnych stężeniach kannabinoidów, co pozwala dobrać je do indywidualnych potrzeb i wymagań. Dostępne w naszym sklepie internetowym produkty są bezpieczne dla zdrowia. Nie wywołują skutków ubocznych, nie działają na człowieka w sposób psychoaktywny, są w stu procentach legalne. Oferowane przez nas produkty są certyfikowane, organiczne i bio. Nie uzależniają.  Zapraszamy do zapoznania się z szeroką ofertą olejków i innych produktów pozyskiwanych z legalnych konopi. Gwarantujemy korzystne ceny, terminową realizację zlecenia oraz najwyższą jakość produktów.
</p>
        </div>
        </div>';
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/olej-cbd-5/") !== false) {
        $filtr_product_cat = "olej-cbd-5";
        $desc1 = '';
        $desc2 = '';
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/olej-cbd-10/") !== false) {
        $filtr_product_cat = "olej-cbd-10";
        $desc1 = '';
        $desc2 = '';
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/olej-cbd-15/") !== false) {
        $filtr_product_cat = "olej-cbd-15";
        $desc1 = '';
        $desc2 = '';
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/olejek-cbd/olejki-biobloom/") !== false) {
    $filtr_product_cat = "olejek-cbd";
    $filtr_brand_cat = "biobloom";

    $desc1 = '<div class="icol-sm-8"><h2>Innowacyjne olejki od renomowanego producenta z Austrii</h2>
        <p>BioBloom to firma z Austrii specjalizująca się w produktach pozyskiwanych z konopii. Konopie, na których bazują olejki BioBloom, uprawiane są w sposób ekologiczny, zrównoważony i przyjazny środowisku. Uprawy zajmują 150 hektarów, na najlepszej jakości glebie. Nasiona są zbierane i przetwarzane ręcznie. Rodzinne przedsiębiorstwo kreuje produkty na bazie konopi we współpracy z lekarzami, ekspertami w dziedzinie farmacji oraz specjalistami od konopi. Są one zatem wynikiem szeroko zakrojonej kooperacji, połączonej z bogatym doświadczeniem zbieranym od kilkudziesięciu lat. Misją marki jest dostarczanie produktów najwyższej jakości, czystych, wartościowych i posiadających liczne prozdrowotne właściwości. Liczy się dla nich też zrównoważona, ekologiczna produkcja, która odbywa się w zgodzie ze środowiskiem naturalnym.

</p>
        </div>
        <div class="icol-sm-4">
        <img src="https://bionateo.com/img/cbda1.jpg" />
        </div>';
    $desc2 = '<div class="irow">
        <div class="icol-sm-4">
        <img src="https://bionateo.com/img/biobloom2.jpg" />
        </div>
        <div class="icol-sm-8"><h2> Olejki BioBloom najwyższej jakości</h2>
        <p>W naszej bogatej ofercie znajduje się wiele produktów od renomowanego producenta Bio Bloom. Znajdziecie w niej między innymi olejki BioBloom CBD/CBDa oraz herbaty z czystych kwiatów konopi, zarówno te sypane, jak i w torebkach. Produkty od BioBloom są najwyższej jakości a właściciele firmy wierzą w najwyższy poziom czystości. Olejki BioBloom są zatem nieprzetworzone i zawierają wszystkie naturalnie występujące kannabinoidy, flawonoidy oraz terpeny.

</p>
        <p>Oferowane przez nas olejki dostępne są w dwóch pojemnościach, różniących się od siebie dodatkowo poziomem stężenia kannabinoidów. Posiadamy te o stężeniu cztero procentowym, sześcio procentowym oraz ośmio procentowym. Wybór konkretnego produktu zależy od Twoich indywidualnych potrzeb i wymagań. Producent zaleca, aby nie przekraczać dziennej dawki olejku BioBloom wynoszącej 10 kropli (5 kropli 2 razy dziennie) Krople aplikujemy pod język. Olejki od tego producenta nie zawierają sztucznych dodatków, są nieuzależniające oraz nie wykazują działań psychoaktywnych. Są zatem w pełni bezpieczne dla zdrowia.

</p>
        </div>
        </div>';
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/kapsulki-cbd/") !== false) {

    $filtr_product_cat = "kapsulki-cbd";



    $desc1 = '<div class="icol-sm-8"><h2>Różne postacie CBD</h2>
        <p>Jednym z produktów, z branży medycznej marihuany, który zyskuje sobie coraz większą popularność, są kapsułki CBD. CBD to kannabinoid niewykazujący działania psychoaktywnego, pozyskiwany z w pełni legalnych konopi, z ekologicznych, zrównoważonych upraw. Posiada wiele prozdrowotnych właściwości - jest między innymi przeciwzapalny, przeciwrakowy, wykazuje silne działanie antybakteryjne, reguluje poziom stresu, pomaga w problemach z nudnościami i wymiotami. CBD w postaci kapsułek jest jednym z najbezpieczniejszych sposobów jego przyjmowania. Nie uzależnia, nie można go przedawkować, korzystnie wpływa na zdrowie i ludzki organizm.


</p>
        </div>
        <div class="icol-sm-4">
        <img src="https://bionateo.com/img/guma1.png" />
        </div>';
    $desc2 = '<div class="irow">
        <div class="icol-sm-4">
        <img src="https://bionateo.com/img/guma2.jpg" />
        </div>
        <div class="icol-sm-8"><h2> Liczne korzyści z przyjmowania kapsułek CBD</h2>
        <p>Kapsułki CBD to optymalne, idealne i wygodne rozwiązanie dla osób, które nie czują się komfortowo, przyjmując czysty olej lub popijając herbatę z kwiatów konopnych. Być może nie lubią smaku oleju czy naparu lub też wolną przyjmować CBD pod inną postacią. Kapsułki są niezwykle wygodne w użytkowaniu - zawierają konkretną dawkę oleju, są łatwe w połknięciu i dobrze się przechowują. Co więcej, są też bezsmakowe i bezglutenowe.

</p>
        <p>Oferowane przez nas produkty pozwalają uzyskać wszystkie liczne korzyści płynące z kannabinoidów. W naszej ofercie znajdują się różne rodzaje kapsułek i gum CBD od kilku renomowanych producentów. Bogactwo dostępnych produktów to gwarancja, że każdy świadomy konsument znajdzie tu coś dla siebie.  Posiadamy także kapsułki z suszem konopnym. Wszystkie nasze produkty są odpowiednie dla wegetarian, wyprodukowane z najwyższej jakości składników, które są bezpieczne dla zdrowia i nieinwazyjne dla organizmu. Wszystkich zainteresowanych klientów zapraszamy do dokładnego zapoznania się z naszą pełną ofertą. W przypadku pytań, pozostajemy do Państwa dyspozycji!


</p>
        </div>
        </div>';
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/hemptouch/") !== false) {
    $filtr_brand_cat = "hemptouch";
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/jointbox/") !== false) {
    $filtr_brand_cat = "jointbox";
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/kombinat/") !== false) {
    $filtr_brand_cat = "kombinat";
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/india/") !== false) {
    $filtr_brand_cat = "india";
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/invex-remedies/") !== false) {
    $filtr_brand_cat = "invex-remedies";
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/cibiday/") !== false) {
    $filtr_brand_cat = "cibiday";
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/euphoria/") !== false) {
    $filtr_brand_cat = "euphoria";
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/epiderma/") !== false) {
    $filtr_brand_cat = "epiderma";
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/annabis/") !== false) {
    $filtr_brand_cat = "annabis";
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/altaio/") !== false) {
    $filtr_brand_cat = "altaio";
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/cibdol-2/") !== false) {
    $filtr_brand_cat = "cibdol";
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/cannabigold-2/") !== false) {
    $filtr_brand_cat = "cannabigold";
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/medi-wiet/") !== false) {
    $filtr_brand_cat = "medi-wiet";
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/harmony/") !== false) {
    $filtr_brand_cat = "harmony";
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/canabidol/") !== false) {
    $filtr_brand_cat = "canabidol";
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/biobloom/") !== false) {
    $filtr_brand_cat = "biobloom";
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/olejek-cbd/cannabigold/") !== false) {
    $filtr_product_cat = "olejek-cbd";
    $filtr_brand_cat = "cannabigold";


    $desc1 = '<div class="icol-sm-8"><h2>Zainwestuj w swoje zdrowie</h2>
        <p>Jeżeli poszukujesz innowacyjnych i w stu procentach naturalnych rozwiązań, które pomogą Ci w poprawie Twojego zdrowia bądź samopoczucia, to trafiłeś pod właściwy adres. Znajdujące się w sklepie Bionateo olejki CannabiGold to jedne z wielu najwyższej jakości produktów, które znajdziesz w naszym atrakcyjnym asortymencie. Wszystkie oferowane przez nas artykuły zostały wyprodukowane z konopi przemysłowej, dlatego też są całkowicie legalne i nie posiadają żadnych działań psychoaktywnych. Poznaj naszą atrakcyjną ofertę!</p>
        </div>
        <div class="icol-sm-4">
        <img src="https://bionateo.com/img/cannabigold1.jpg" />
        </div>';
    $desc2 = '<div class="irow">
        <div class="icol-sm-4">
        <img src="https://bionateo.com/img/cannabigold2.jpg" />
        </div>
        <div class="icol-sm-8"><h2>Poznaj właściwości olejków Cannabigold</h2>
        <p>Nasz sklep internetowy to odpowiedź na potrzeby i oczekiwania konsumentów poszukujących innowacyjnych i naturalnych rozwiązań, których skuteczność została potwierdzona. Mamy dla Ciebie dobrą wiadomość - dostępne u nas olejki CBD CannabiGold to ogromna moc natury zamknięta w płynnej postaci. Dzięki Bionateo.com możesz z łatwością sprawić, że znajdzie się ona również w Twoim domu. Pamiętaj, że kupując w naszym sklepie masz całkowitą gwarancję wysokiej jakości oraz bezpieczeństwa nabywanych produktów.</p>
        <p>Zastanawiasz się, co jest takiego wyjątkowego w oferowanych przez nas produktach? Spieszymy z wyjaśnieniem! Przede wszystkim są to właściwości zdrowotne, jakie posiadają dostępne w Bionateo olejki CannabiGold. Mają one działanie antydepresyjne, przeciwbólowe czy poprawiające koncentrację i dostarczające energii. Co więcej, konopie mają zbawienny wpływ na stan Twojej skóry - znacząco zwiększa jej elastyczność, dzięki czemu Ty wyglądasz młodziej, a Twoja cera zdrowiej. Dodatkowo jest ona pomocna w trakcie leczenia różnych zmian skórnych.</p>
        <p>Jednak to jeszcze nie wszystko! Jeżeli należysz do tej sporej części populacji, która ma różnego rodzaju problemy z układem trawiennym, np. IBS, chorobę Leśniowskiego-Crohna czy nerwicę wegetatywną, która objawia się poprzez bóle brzucha, to CBD CannabiGold może być dla Ciebie doskonałym rozwiązaniem! Jak się okazuje - bogactwo składników odżywczych w nich zawartych ma ogromny wpływ na zmniejszanie stanów zapalnych układu pokarmowego oraz wsparcie ogólnego metabolizmu. Zamów wybrany produkt już dziś i przekonaj się sam!</p>
        </div>
        </div>

        <div class="irow">
        <div class="icol-sm-8"><h2>CBD CannabiGold - bezpiecznie i legalnie</h2>
        <p>Zdajemy sobie sprawę, że temat konopi może budzić kontrowersję, jednak liczba ich zwolenników rośnie z dnia na dzień. Nic w tym dziwnego - wiele osób znalazło ukojenie, którego tradycyjna medycyna i niszczące antybiotyki czy inne leki nie potrafiły zapewnić. Proponowane olejki są polecane jako środki posiadające pozytywny wpływ na ludzki układ endokannabinoidowy, co zostało potwierdzone poprzez wiele zaawansowanych i długotrwałych badań naukowych. Natura od zawsze miała największą moc, a dzięki Bionateo masz do niej łatwy dostęp.</p>
        <p>Bezpieczeństwo i zadowolenie klientów to nasz absolutny priorytet, dlatego też każdy wprowadzany do asortymentu sklepu produkt przechodzi wiele zaawansowanych testów laboratoryjnych. Dodatkowo, produkcja olejków CBD CannabiGold musi być prowadzona zgodnie ze ściśle określonymi normami. Nasi dostawcy pochodzą z Holandii, Hiszpanii, Słowenii, Austrii czy Węgrzech, a ich uprawy są prowadzone w stu procentach ekologiczne, bez użycia pestycydów i innych chemikaliów. Poznaj moc natury już dziś i złóż swoje pierwsze zamówienie. Zapraszamy!</p>
        </div>
        <div class="icol-sm-4">
        <img src="https://bionateo.com/img/cannabigold3.jpg" />
        </div>
        </div>';
} elseif (function_exists('is_product_category') && is_product_category('olejek-cbd')) {
    $filtr_product_cat = "olejek-cbd";
    $desc1 = '<div class="icol-sm-8"><h2>Zaufaj mocy legalnej natury</h2>
        <p>Witaj w sklepie internetowym Bionateo.com, który powstał w celu dostarczenia Ci nowoczesnych i naturalnych rozwiązań opartych na coraz bardziej popularnej, legalnej konopi siewnej (włóknistej). W naszym asortymencie znajdziesz między innymi olej konopny CBD, ekstrakty i pasty, kapsułki, kosmetyki, krople, żywność czy produkty do waporyzacji, które są artykułami niezwykle bogatymi w wartościowe składniki odżywcze. Dzięki tak bogatej i różnorodnej ofercie, każdy z naszych klientów z łatwością wybierze artykuł, który całkowicie spełni jego oczekiwania. </p>
        </div>
        <div class="icol-sm-4">
        <img src="https://bionateo.com/img/ok1-1.jpg" />
        </div>';
    $desc2 = '<div class="irow">
        <div class="icol-sm-4">
        <img src="https://bionateo.com/img/ok2-1.jpg" />
        </div>
        <div class="icol-sm-8"><h2>Olej konopny - poznaj jego sekrety</h2>
        <p>Bionateo.com powstało z miłości do natury oraz jej niesamowitego i dobroczynnego wpływu na ludzki organizm. Każdy z oferowanych przez nas produktów przeszedł wiele skomplikowanych testów oraz badań laboratoryjnych po to, aby zapewnić Tobie i pozostałym naszym klientom bezpieczeństwo i wysoką jakość. Proponowany olej konopny jest bogactwem cennych składników, które znajdziemy w coraz bardziej popularnych konopiach. Został on wyekstrahowany za pomocą sprawdzonej i bezpiecznej metody CO2. Kupując w sklepie Bionateo możesz mieć pewność, że nabywasz produkt najwyższej jakości.
</p>
        <p>Choć konopie nadal często budzą kontrowersje, to coraz więcej osób na całym świecie potwierdza ich niezwykle zbawienny wpływ na ludzkie ciało. Są to nie tylko osoby, które ją stosowały, ale także cenieni w środowisku lekarze czy farmakolodzy. Olej CBD proponowany przez sklep Bionateo został wyprodukowany przy użyciu specjalnych odmian konopi, które pochodzą z bezpiecznych i ekologicznych upraw hodowanych na glebie wysokiej klasy. Nasi dostawcy pochodzą między innymi ze Słowenii, Austrii, Hiszpanii, Holandii, Anglii a także Polski.
</p>
        <p>Z uwagi na to, że bezpieczeństwo naszych klientów jest dla nas najważniejsze, nieustannie sprawdzamy, czy wszystkie nasze produkty spełniają określone normy bezpieczeństwa. Dzięki zaawansowanym analizom laboratoryjnym, olejki CBD oraz krople proponowane w naszym sklepie są produktami całkowicie sprawdzonymi. Co więcej, nasz polsko-holenderski zespół to grupa pasjonatów zdrowego żywienia i nauki, będącymi jednocześnie specjalistami w danej branży. To właśnie ta pasja zainspirowała nas do stworzenia miejsca, dzięki któremu zyskasz łatwy dostęp do innowacyjnych rozwiązań prozdrowotnych.
</p>
        </div>
        </div>

        <div class="irow">
        <div class="icol-sm-8"><h2>Właściwości oleju konopnego CBD</h2>
        <p>Sklep internetowy Bionateo każdego dnia dostarcza swoim klientom skuteczne rozwiązania dla ich stanu zdrowia, które są odpowiedzią na ich potrzeby i oczekiwania. Jednym z nich są proponowane krople CBD, których popularność rośnie każdego dnia. Oferowany produkt zawiera naturalnie występujące kannabinoidy. Choć możesz się obawiać, że konopie są w Polsce zakazane, to produkty wytwarzane na bazie ich przemysłowej odmiany są całkowicie legalne, ponieważ nie uzależniają oraz nie działają psychoaktywnie.
</p>
        <p>Z pewnością zastanawiasz się, co jest takiego wyjątkowego w proponowanych przez nas produktach. Spieszymy z wyjaśnieniem! Olejek CBD oferowany przez sklep Bionateo dzięki produkcji, do której wykorzystano najwyższej jakości rośliny, ma wiele właściwości. Może działać jako antydepresant, lek przeciwbólowy, jest także doskonałym wsparciem w leczeniu nawracającej łuszczycy czy innych zmian skórnych, np. atopowego zapalenia skóry. Stosowanie olejku umożliwia również obniżenie ciśnienia krwi oraz redukcję poziomu złego cholesterolu. Odpowiednio dobrane dawkowanie pozwala zmniejszyć ryzyko wystąpienia zawału serca.
</p>
        </div>
        <div class="icol-sm-4">
        <img src="https://bionateo.com/img/ok3.jpg" />
        </div>
        </div>

        <div class="irow">
        <div class="icol-sm-4">
        <img src="https://bionateo.com/img/ok4.jpg" />
        </div>
        <div class="icol-sm-8"><h2>Krople CBD - legalne konopie</h2>
        <p>Tak jak wspominaliśmy - wszystkie produkty dostępne w naszym sklepie internetowym są w pełni legalne, ponieważ zostały wyprodukowane przy użyciu konopi przemysłowych, a zawartość THC nie przekracza dopuszczalnej normy 0.2%. Konopny olej CBD to naturalne bogactwo cennych składników, zwanych kannabinoidami. Do ich grupy zaliczamy między innymi CBD/CBDA, THC/THCA czy CBN/CBNA. Dodatkowo, znajdziesz w nim także flawonoidy, których przeciwutleniające właściwości doskonale wpływają detoksykująco na Twoje ciało. Pozostałe składniki odżywcze to terpeny, białko, nienasycone kwasy tłuszczowe oraz błonnik.
</p>
        <p>Tak bogata w składniki odżywcze substancja nie może pozostać niezauważona. Coraz więcej osób odkrywa magiczną moc, jaką posiada proponowany przez Bionateo olej konopny. W naturalny i łatwy sposób dostarcza organizmowi nie tylko naturalne i cenione związki organiczne, ale dodatkowo ma ogromny wpływ na Twoje samopoczucie i zdrowie. Oczyszcza Twoje ciało, pozwala Ci się uspokoić, ukoić ból lub nabrać odrobinę potrzebnej energii. Konopie to nadzieja medycyny, a Ty już dziś masz dostęp do najlepszych produktów. Nasze krople CBD to dobry wybór!
</p>
        </div>
        </div>

        <div class="irow">
        <div class="icol-sm-8"><h2>Olej CBD w atrakcyjnej cenie - sklep Bionateo.com</h2>
        <p>Sklep internetowy Bionateo powstał z pasji do innowacyjnych rozwiązań, a także po to, aby móc dzielić się nimi z każdym z naszych klientów. Co więcej, proponowany olej konopny to produkt, którego cena z pewnością Cię zaskoczy. Chcemy, aby wszystkie osoby zainteresowane alternatywną medycyną miały dostęp do bezpiecznych i wysokiej jakości artykułów konopnych, bez względu na budżet, którym dysponują. Nasz asortyment to wyjście naprzeciw oczekiwaniom i potrzebom wszystkich konsumentów, którzy poszukują skutecznych i naturalnych rozwiązań.
</p>
        <p>Konopie siewne uważane są za naprawdę obiecujące odkrycie, a badania nad możliwościami ich zastosowania w wielu przewlekłych i ciężkich chorobach ciągle trwają. Najwyższej jakości olej konopny proponowany przez sklep Bionateo to rozwiązanie w stu procentach bezpieczne i sprawdzone, które spełni oczekiwania nawet najbardziej wymagających klientów. Serdecznie zapraszamy do zapoznania się z naszą atrakcyjną i szczegółową ofertą. W razie jakichkolwiek pytań nasi specjaliści pozostają do Twojej dyspozycji.
</p>
        </div>
        <div class="icol-sm-4">
        <img src="https://bionateo.com/img/ok5.jpg" />
        </div>
        </div>';
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/pasty-cbd-cbda/") !== false) {
    $filtr_product_cat = "pasty-cbd-cbda";
    $desc1 = '<div class="icol-sm-8"><h2>Naturalna pasta konopna, z ekstraktem z konopii CBD</h2>
        <p>Postanowiłeś zaufać cenionej od wieków mocy natury i nie wiesz od czego zacząć? Sklep internetowy Bionateo ma dla Ciebie coś wyjątkowego. W naszym asortymencie znajdziesz wyłącznie wysokiej jakości produkty pozyskiwane z konopi, takie jak pasta CBD, olejki o szerokim spektrum zastosowania, ekstrakty, kapsułki, kosmetyki służące ochronie skóry czy żywność. Wszystkie oferowane przez nas produkty są oczywiście wyprodukowane z w pełni legalnej konopi siewnej, pochodzącej z ekologicznych i zaufanych upraw w Polsce, Holandii, Hiszpanii, Austrii czy Słowenii. Tak szeroki asortyment to odpowiedź na potrzeby klientów, którzy zdecydowali się zamienić chemiczne preparaty na te o pochodzeniu naturalnym, które są w pełni bezpieczne. Zapoznaj się z naszą atrakcyjną ofertą!
</p>

        </div>
        <div class="icol-sm-4">
        <img src="https://bionateo.com/img/pasta1.jpg" />
        </div>';
    $desc2 = '<div class="irow">
        <div class="icol-sm-4">
        <img src="https://bionateo.com/img/pc2-1.jpg" />
        </div>
        <div class="icol-sm-8"><h2> Pasta konopna tylko od zaufanych producentów</h2>
        <p>W ciągu ostatnich lat można zaobserwować gwałtowny wzrost popularności konopi. Nic w tym dziwnego! Jak się okazuje, roślina ta posiada szereg właściwości prozdrowotnych, które zostały docenione przez coraz liczniejsze grono przedstawicieli środowiska medycznego. Znajdujące się w naszej ofercie pasta konopna to jeden z przykładów produktów, który może okazać się remedium na Twoje dolegliwości. Jest to najbardziej naturalna postać oleju konopnego, która została wyprodukowana z wiech tej rośliny. Na rynku dostępne są pasty CBD w różnych wersjach, które różnią się między sobą konsystencją czy smakiem.
</p>
        <p>ZKupując w sklepie internetowym Bionateo masz pewność, że nabywasz produkt nie tylko najwyższej jakości, ale też całkowicie oryginalny i legalny. Zdajemy sobie sprawę, że możesz obawiać się tego, że oferowane przez nas produkty są nielegalne bądź niebezpieczne dla Twojego zdrowia. Nic bardziej mylnego! Wszystkie artykuły dostępne w naszym asortymencie to towary w stu procentach legalne i bezpieczne dla Ciebie i Twojego ciała. Przeprowadzamy dziesiątki testów laboratoryjnych przed wprowadzeniem ich do sprzedaży, aby móc zagwarantować Ci, że nasz asortyment nie uzależnia oraz nie ma działania psychoaktywnego i odurzającego.
</p>

<p>Zgodnie z prawem stężenie THC we wszystkich naszych produktach wynosi mniej niż 0,2%. Z kolei, dzięki najbardziej zaawansowanemu procesowi ekstrakcji, gwarantujemy najwyższą jakość i czystość ekstraktu konopnego, wchodzącego w skład pasty CBD. Najwyższa jakość dostarczanych produktów jest naszym priorytetem, dlatego też współpracujemy wyłącznie z renomowanymi i zaufanymi producentami z Europy. Każda dostępna w sklepie internetowym Bionateo pasta konopna została wyprodukowana z najwyższej jakości roślin, których uprawa oraz proces produkcji spełniają ściśle określone normy. Co więcej, nasz zespół składa się nie tylko z pasjonatów innowacyjnych, naturalnych rozwiązań, ale także specjalistów, którzy dzięki zdobytej wiedzy i doświadczeniu czuwają nad bezpieczeństwem dostarczanych przez nas produktów.
</p>
        </div>
        </div>

        <div class="irow">
        <div class="icol-sm-8"><h2>Ekstrakt z konopi CBD - najlepszy sposób na poprawę ogólnego stanu zdrowia, ale i walkę z wieloma chorobami</h2>
        <p>
Zaufaj prozdrowotnym właściwościom pasty konopnej! Sklep internetowy Bionateo oferuje Ci wyłącznie czyste i nieprzetworzone dary natury, których właściwości prozdrowotne zostały udowodnione i docenione na całym świecie. Znajdująca się w naszym asortymencie pasta CBD to produkt, który może mieć naprawdę zbawienny wpływ na Twój układ endokannabinoidowy. Dzięki zawartości kannabidiolu, czyli substancji, którą pozyskuje się z konopi siewnej, pozwala on nie tylko na regulację, ale także na widoczną poprawę szeregu procesów fizjologicznych, które każdego dnia zachodzą w Twoim organizmie.
</p>
        <p>Na podstawie przeprowadzonych badań laboratoryjnych udowodniono, że konopie posiadają właściwości antydepresyjne, przeciwbólowe czy pomagające w redukcji stanów zapalnych w Twoich ciele. Proponowana pasta konopna dodatkowo pomoże Ci wzmocnić odporność, poprawić koncentrację i zapewni Ci energię na każdy dzień. Tego typu produkty, których ekstrakt CBD odznacza się wysokim stężeniem kannabinoidów, zaleca się jako wsparcie leczenia Alzheimera, raka, jaskry, stwardnienia rozsianego, epilepsji, padaczki lekoopornej, depresji czy różnorodnych zmian skórnych. Już dziś dokonaj zakupu naturalnego i organicznego daru natury, najwyższej jakości ekstraktu z konopi CBD, aby móc cieszyć się zdrowiem!
</p>
        </div>
        <div class="icol-sm-4">
        <img src="https://bionateo.com/img/pc3.jpg" />
        </div>
        </div>';
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/dermokosmetyki-cbd-ochrona-skory/") !== false) {
    $filtr_product_cat = "dermokosmetyki-cbd-ochrona-skory";
} elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/kapsulki-cbd/") !== false) {
    $filtr_product_cat = "kapsulki-cbd";
} elseif ((function_exists('is_product_category') && is_product_category()) || (function_exists('is_product_tag') && is_product_tag())){
  $qo = get_queried_object();
  if(isset($qo->slug)){
    $filtr_product_cat = $qo->slug;
    $desc1 = $qo->description;

    if(function_exists('get_field')){
      $desc2 = get_field('drugi_opis', $qo->term_id);
    }
  }
} elseif (isset($_GET['filtr_product_cat'])) {
    $filtr_product_cat = explode(",", $_GET['filtr_product_cat']);
} else {
    $filtr_product_cat = false;
}
if (isset($_GET['filtr_size_cat'])) {
    $filtr_size_cat = explode(",", $_GET['filtr_size_cat']);
} else {
    $filtr_size_cat = false;
}
if (isset($_GET['filtr_concentration_cat'])) {
    $filtr_concentration_cat = explode(",", $_GET['filtr_concentration_cat']);
} else {
    $filtr_concentration_cat = false;
}
if (isset($_GET['filtr_order'])) {
    $filtr_order = explode(",", $_GET['filtr_order']);
} else {
    $filtr_order = false;
}

$filtr_upper = 10000;
$filtr_lower = 0;
if (isset($_GET['filtr_upper'])) {
    $filtr_upper = $_GET['filtr_upper'];
}
if (isset($_GET['filtr_lower'])) {
    $filtr_lower = $_GET['filtr_lower'];
}
if (isset($_GET['page'])) {
    $page = $_GET['page'];
    $page2 = $_GET['page'];
}

function add_tax_query($value1, $key) {
    if (!empty($value1) && !empty($value1[0])) {
        $ret = array(
            'taxonomy' => $key,
            'field' => 'slug',
            'terms' => $value1
        );
    } else {
        $ret = '';
    }
    return $ret;
}

;

function add_meta_between($val1, $val2, $key) {
    if (!empty($val1)) {
        $ret = array(
            'key' => $key,
            'value' => array($val1, $val2),
            'compare' => 'BETWEEN',
            'type' => 'DECIMAL(6, 2)'
        );
    } else {
        $ret = '';
    }
    return $ret;
}

;

function get_product_prices() {
    global $wpdb, $wp_the_query;

    $args = $wp_the_query->query_vars;
    $tax_query = isset($args['tax_query']) ? $args['tax_query'] : array();
    $meta_query = isset($args['meta_query']) ? $args['meta_query'] : array();

    if (!is_post_type_archive('product') && !empty($args['taxonomy']) && !empty($args['term'])) {
        $tax_query[] = array(
            'taxonomy' => $args['taxonomy'],
            'terms' => array($args['term']),
            'field' => 'slug',
        );
    }

    foreach ($meta_query + $tax_query as $key => $query) {
        if (!empty($query['price_filter']) || !empty($query['rating_filter'])) {
            unset($meta_query[$key]);
        }
    }

    $meta_query = new WP_Meta_Query($meta_query);
    $tax_query = new WP_Tax_Query($tax_query);

    $meta_query_sql = $meta_query->get_sql('post', $wpdb->posts, 'ID');
    $tax_query_sql = $tax_query->get_sql($wpdb->posts, 'ID');

    $sql = "SELECT min( FLOOR( price_meta.meta_value ) ) as min_price, max( CEILING( price_meta.meta_value ) ) as max_price FROM {$wpdb->posts} ";
    $sql .= " LEFT JOIN {$wpdb->postmeta} as price_meta ON {$wpdb->posts}.ID = price_meta.post_id " . $tax_query_sql['join'] . $meta_query_sql['join'];
    $sql .= " 	WHERE {$wpdb->posts}.post_type IN ('" . implode("','", array_map('esc_sql', apply_filters('woocommerce_price_filter_post_type', array('product')))) . "')
					AND {$wpdb->posts}.post_status = 'publish'
					AND price_meta.meta_key IN ('" . implode("','", array_map('esc_sql', apply_filters('woocommerce_price_filter_meta_keys', array('_price')))) . "')
					AND price_meta.meta_value > '' ";
    $sql .= $tax_query_sql['where'] . $meta_query_sql['where'];

    if ($search = WC_Query::get_main_search_query_sql()) {
        $sql .= ' AND ' . $search;
    }

    return $wpdb->get_row($sql);
}

$query_args = [];
$query_args['post_type'] = 'product';
$query_args['posts_per_page'] = 16;
$query_args['paged'] = $page;
$query_args['tax_query'] = array(
    'relation' => 'AND',
    add_tax_query($filtr_product_cat, 'product_cat'),
    add_tax_query($filtr_brand_cat, 'brand_cat'),
    add_tax_query($filtr_size_cat, 'size_cat'),
    add_tax_query($filtr_concentration_cat, 'concentration_cat'),
);
$query_args['meta_query'] = array(
    'relation' => 'AND',
    add_meta_between($filtr_lower, $filtr_upper, '_price'),
);
if ($filtr_order[0] == 'newest') {
    $query_args['order'] = 'ASC';
    $query_args['orderby'] = 'date';
} elseif ($filtr_order[0] == 'oldest') {
    $query_args['order'] = 'DESC';
    $query_args['orderby'] = 'date';
} elseif ($filtr_order[0] == 'asc') {
    $query_args['order'] = 'ASC';
    $query_args['orderby'] = 'title';
} elseif ($filtr_order[0] == 'desc') {
    $query_args['order'] = 'DESC';
    $query_args['orderby'] = 'title';
} elseif ($filtr_order[0] == 'lower') {
    $query_args['order'] = 'ASC';
    $query_args['orderby'] = 'meta_value_num';
    $query_args['meta_key'] = '_price';
} elseif ($filtr_order[0] == 'upper') {
    $query_args['order'] = 'DESC';
    $query_args['orderby'] = 'meta_value_num';
    $query_args['meta_key'] = '_price';
}

$the_query_args = new WP_Query($query_args);
$args = array(
    'post_type' => 'product',
    'posts_per_page' => -1,
);
$the_query = new WP_Query($args);

$array = array();
if (have_posts()) :
    $filter_prices = get_product_prices();
    $filtr_current = $filter_prices->min_price;
    $filtr_end = $filter_prices->max_price;
    ?>
    <script>
        jQuery('body').attr('filtr_lower_min',<?php echo $filtr_current; ?>);
        jQuery('body').attr('filtr_upper_max',<?php echo $filtr_end; ?>);
        jQuery('body').attr('filtr_lower',<?php echo $filtr_current; ?>);
        jQuery('body').attr('filtr_upper',<?php echo $filtr_end; ?>);
    </script>
<?php endif;
?>
<main class="main-wrap-cms product-list-page">
    <div class="container">
        <div class="row expanded">
            <div class="small-12 columns">
                <button class="button-b" id="product-filters-show">Pokaż/Ukryj filtry</button>
            </div>
            <?php if(0): ?>
            <div class="large-3 columns mobile-is">
                <div class="row expanded">
                    <div class="large-12 medium-6 columns">
                        <h2 class="product-list-page__title first">Kategorie</h2>
                        <div class="product-list-page__filters">
                            <?php
                            $product_cat = get_terms(array(
                                'taxonomy' => 'product_cat',
                                'hide_empty' => true,
                            ));
                            foreach ($product_cat as $p) {
                                $class = '';

                                if ($filtr_product_cat) {
                                    foreach ($filtr_product_cat as $u) {
                                        if ($p->slug == $u) {
                                            $class = 'active';
                                        }
                                    }
                                }
                                if ($p->slug == 'cannabigold' || $p->slug == 'cibdol' || $p->slug == 'invex-remedies' || $p->slug == 'medihemp' || $p->slug == 'biobloom' || $p->slug == 'cannabidol' || $p->slug == 'harmony' || $p->slug == 'hemptouch' || $p->slug == 'medi-wiet' || $p->slug == 'canabidol' || $p->slug == 'cibiday' || $p->slug == 'euphoria' || $p->slug == 'epiderma' || $p->slug == 'annabis' || $p->slug == 'altaio' || $p->slug == 'jointbox' || $p->slug == 'kombinat' || $p->slug == 'india')
                                    continue;

                                echo '<div class="product-list-page__filters__filtr" >
									<span class="' . $class . ' product-list-page__filters__filtr__input t-product_cat" data-name="product_cat"
									value="' . $p->slug . '"></span>
									<span class="' . $class . ' product-list-page__filters__filtr__name">' . $p->name . '</span></div>';
                            }
                            ?>
                        </div>
                    </div>
                    <div class="small-12 columns">
                        <button class="button-b product-filters" id="product-filters">Zastosuj filtry</button>
                    </div>
                    <div class="large-12 medium-6 columns">
                        <h2 class="product-list-page__title second">Filtry</h2>
                        <h3 class="product-list-page__subtitle">Cena</h3>
                        <div class="product-list-page__slider">
                            <div class="product-list-page__slider__wrap">
                                <div class="product-list-page__slider__wrap__price left">
                                    <span id="slider-snap-value-lower"><?php echo $filtr_current; ?></span> <?php echo get_woocommerce_currency_symbol(); ?>
                                </div>
                                <div class="product-list-page__slider__wrap__price right">
                                    <span id="slider-snap-value-upper"><?php echo $filtr_end; ?></span> <?php echo get_woocommerce_currency_symbol(); ?>
                                </div>
                            </div>
                            <div id="priceSlider"></div>
                        </div>
                    </div>
                    <div class="large-12 medium-6 columns">
                        <h3 class="product-list-page__subtitle">Marka</h3>
                        <div class="product-list-page__filters">
                            <?php
                            $product_cat = get_terms(array(
                                'taxonomy' => 'brand_cat',
                                'hide_empty' => false,
                            ));
                            foreach ($product_cat as $p) {
                                $class = '';
                                if ($filtr_brand_cat) {
                                    foreach ($filtr_brand_cat as $u) {
                                        if ($p->slug == $u) {
                                            $class = 'active';
                                        }
                                    }
                                }

                                echo '<div class="product-list-page__filters__filtr" >
									<span class="' . $class . ' product-list-page__filters__filtr__input t-brand_cat" data-name="brand_cat"
									value="' . $p->slug . '"></span>
									<span class="' . $class . ' product-list-page__filters__filtr__name">' . $p->name . '</span></div>';
                            }
                            ?>
                        </div>
                    </div>

                    <div class="large-12 medium-6 columns">
                        <h3 class="product-list-page__subtitle">Stężenie</h3>
                        <div class="product-list-page__filters">
                            <?php
                            $product_cat = get_terms(array(
                                'taxonomy' => 'concentration_cat',
                                'hide_empty' => false,
                                //'order' => 'ASC',
                                'orderby' => 'id',
                            ));
                            foreach ($product_cat as $p) {
                                $class = '';
                                if ($filtr_concentration_cat) {
                                    foreach ($filtr_concentration_cat as $u) {
                                        if ($p->slug == $u) {
                                            $class = 'active';
                                        }
                                    }
                                }

                                echo '<div class="product-list-page__filters__filtr" >
									<span class="' . $class . ' product-list-page__filters__filtr__input t-concentration_cat" data-name="concentration_cat"
									value="' . $p->slug . '"></span>
									<span class="' . $class . ' product-list-page__filters__filtr__name">' . $p->name . '</span></div>';
                            }
                            ?>
                        </div>
                    </div>
                    <div class="small-12 columns">
                        <button class="button-b product-filters" id="product-filters">Zastosuj filtry</button>
                    </div>
                </div>
            </div>
          <?php endif; ?>
            <div class="large-12 columns">
                <?php
                /* OPIS GÓRNY ///////////////////////////////////// */
                if (isset($desc1)):
                    ?>
                    <div class="row fullWidth" style="margin-bottom:30px;"><?php echo $desc1; ?></div>
                <?php endif; ?>
                <div class="row fullWidth product-list-page__sort">
                    <div class="medium-6 columns">
                        <h4 class="product-list-page__sort__title">Sortowanie</h4>
                        <div class="product-list-page__sort__wrap">
                            <select class="product-list-page__sort__wrap__select" name="" id="filtr_order">
                                <?php
                                foreach ($filtr_order_array as $f) {
                                    $selected = '';
                                    if ($filtr_order) {
                                        foreach ($filtr_order as $u) {
                                            if ($u == $f['slug']) {
                                                $selected = 'selected="selected"';
                                            }
                                        }
                                    }

                                    echo '<option ' . $selected . ' value="' . $f['slug'] . '">' . $f['name'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <p class="product-list-page__sort__count">Ilość produktów: <?php echo $the_query_args->found_posts; ?></p>
                    </div>
                    <div class="medium-6 columns">
                        <div class="pagination">
                            <?php
                            echo paginate_links(array(
                                'base' => preg_replace('/\?.*/', '/', get_pagenum_link(1)) . '%_%',
                                'current' => max(1, $page),
                                'format' => '?page=%#%',
                                'total' => $the_query_args->max_num_pages,
                                'prev_text' => '',
                                'next_text' => '',
                            ));
                            ?>
                        </div>
                    </div>
                </div>
                <div class="product-list-page__wrap_posts">
                    <?php
                    if ($the_query_args->have_posts()) :
                        while ($the_query_args->have_posts()) :
                            $the_query_args->the_post();
                            ?>
                            <article id="post-<?php the_ID(); ?>" <?php post_class('large-3 medium-6 columns end single--product customHeight'); ?> role="article">

                            <?php get_template_part('parts/loop', 'archive'); ?>
                            </article>
                        <?php endwhile; ?>
<?php endif; ?>
                </div>

                <div class="small-12 columns">
                    <div class="pagination">
                        <?php
                        echo paginate_links(array(
                            'base' => preg_replace('/\?.*/', '/', get_pagenum_link(1)) . '%_%',
                            'current' => max(1, $page2),
                            'format' => '?page=%#%',
                            'total' => $the_query_args->max_num_pages,
                            'prev_text' => '',
                            'next_text' => '',
                        ));
                        ?>
                    </div>
                </div>
                <?php
                /* OPIS DOLNY ////////////////////////////////////////////////// */
                if (isset($desc2)):
                    ?>
                    <div>
                        <div class="columns">
                            <div class="">
    <?php echo $desc2; ?>
                            </div>
                        </div>
                    </div>

<?php endif; ?>
            </div>



<?php if ($archive_text): ?>
                <div class="product-list-page__bottom_text">
                    <div class="large-8 large-offset-3 columns">
                        <div class="">
    <?php echo $archive_text; ?>
                        </div>
                    </div>
                </div>

<?php endif; ?>
        </div>
    </div>
</main>

<?php get_footer('shop'); ?>
