<?php
get_header();
?>
<?php
    if (have_posts()) : while (have_posts()) : the_post();
        $images = get_field('delivery_methods_b2b');
        $brands = get_field('brands');
        $content = get_field('content');
    endwhile; ?>
    <?php endif; ?>
    <main class="main-wrap-cms">
        <div class="">
            <div class="row expanded">
                <div class="large-12 columns page-template-home__about">
                    <div class="container">
                        <section class="row expanded">
                            <div class="large-4 columns page-template-home__left">
                                <h3 class="page-template-home__left__subtitle">Bionateo</h3>
                                <h2 class="page-template-home__left__title">o nas</h2>
                                <a class="page-template-b2b__left__url button-b" href="<?php echo get_the_permalink(670); ?>">Zobacz więcej</a>
                            </div>
                            <div class="large-8 columns page-template-home__about__list">
                                <div class="content">
                                    <?php echo $content;?>
                                </div>
                                <div class="about-list">
                                    <?php
                                    if( have_rows('list') ):
                                        echo '<ul class="row about-list__main" data-equalizer data-equalize-by-row="true"> ';
                                        while ( have_rows('list') ) : the_row();
                                            $img = get_sub_field('img');
                                            $title = get_sub_field('title');
                                            $content = get_sub_field('content');
                                        ?>
                                        <li class="medium-6 columns end about-list__main__li" data-equalizer-watch="">
                                            <div class="about-list__main__img">
                                                <img src="<?php echo $img['url'];?>" alt="<?php echo $img['alt']; ?>"/>
                                            </div>
                                            <div class="about-list__main__wrap">
                                                <h2 class="about-list__main__wrap__title"><?php echo $title; ?></h2>
                                                <p class="about-list__main__wrap__content">
                                                    <?php echo $content;?>
                                                </p>
                                            </div>
                                        </li>
                                        <?php
                                        endwhile;
                                        echo '</ul>';
                                    endif;
                                    ?>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                <div class="large-12 columns page-template-home__products">
                    <div class="container">
                        <section class="row expanded">
                            <div class="large-4 columns page-template-home__left">
                                <h3 class="page-template-home__left__subtitle">Poznaj nasze</h3>
                                <h2 class="page-template-home__left__title">produkty</h2>
                                <a class="page-template-b2b__left__url button-b" href="<?php echo get_the_permalink(773); ?>">Zobacz wszystkie</a>
                            </div>
                            <div class="large-8 columns page-template-home__products__list">
                            <?php
                                if( have_rows('product') ): $i =0;
                                    while ( have_rows('product') ) : the_row();
                                        $icon = get_sub_field('icon');
                                        $title = get_sub_field('title');
                                        $description = get_sub_field('description');
                                        $img = get_sub_field('img');
                                        $link = get_sub_field('link');
                                        $sort ='';
                                        if($i%2===0){
                                            $sort = 'sort2';
                                        }else{
                                            $sort = 'sort1';
                                        }
                                        ?>
                                        <div class="page-template-home__products__list__single <?php echo $sort;?>">
                                            <div class="page-template-home__products__list__single__col1">
                                                <img class="page-template-home__products__list__single__col1__img" src="<?php echo $img['url'] ?>"/>
                                                <div class="page-template-home__products__list__single__col1__icon">
                                                    <img src="<?php echo $icon['url'];?>"/>
                                                </div>
                                            </div>
                                            <div class="page-template-home__products__list__single__col2">
                                                <h3 class="page-template-home__products__list__single__col2__title"><?php echo $title;?></h3>
                                                <div class="page-template-home__products__list__single__col2__description">
                                                    <?php echo $description; ?>
                                                </div>
                                                <a class="button-b page-template-home__products__list__single__col2__url" href="<?php echo get_the_permalink(773)?>?filtr_product_cat=<?php echo $link->slug; ?>">Kup w sklepie</a>
                                            </div>

                                        </div>
                                        <?php
                                        $i++;
                                    endwhile;
                                endif;
                                ?>
                            </div>
                        </section>
                    </div>
                </div>
                <div class="large-12 columns page-template-home__education">
                    <div class="container">
                        <section class="row expanded">
                            <div class="large-4 columns page-template-home__left">
                                <h3 class="page-template-home__left__subtitle">Kilka słow o konopii</h3>
                                <h2 class="page-template-home__left__title">edukacja</h2>
                                <a class="page-template-b2b__left__url button-b button-white" href="<?php echo get_the_permalink(674); ?>">Zobacz więcej</a>
                            </div>
                            <div class="large-8 columns page-template-home__education__list">
                                <?php
                                if( have_rows('edu_fields') ):
                                $ac = 0;
                                    echo '<ul class="accordion" data-accordion data-allow-all-closed="true">';
                                    while ( have_rows('edu_fields') ) : the_row();
                                            $name = get_sub_field('name');
                                            $content = get_sub_field('content');
                                    ?>
                                    <li class="accordion-item <?php if($ac==0):?>is-active<?php endif;?>" data-accordion-item>
                                        <a href="#" class="accordion-title"><?php echo $name;?> <button class="accordion-cross"><span class="nt1"></span><span class="nt2"></span><span class="nt3"></span></button></a>
                                        <div class="accordion-content" data-tab-content>
                                            <?php echo $content;?>
                                        </div>
                                    </li>
                                    <?php
                                    $ac++;
                                    endwhile;

                                    echo '</ul>';
                                endif;
                                ?>
                            </div>
                        </section>
                    </div>
                </div>
                <div class="large-12 columns page-template-home__brands">
                    <div class="container">
                        <section class="row expanded">
                            <div class="large-5 columns page-template-home__left">
                                <h3 class="page-template-home__left__subtitle">U nas znajdziesz</h3>
                                <h2 class="page-template-home__left__title">marki</h2>
                            </div>
                            <div class="large-7 columns page-template-home__brands__loop">
                            <?php
                            $size = 'full'; // (thumbnail, medium, large, full or custom size)

                            if( $brands ): ?>
                                <ul class="row large-up-4 small-up-2  page-template-home__brands__loop__list">
                                    <?php foreach( $brands as $image ): ?>
                                        <li class="column column-block page-template-home__brands__loop__list__li organic">
                                            <?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                            </div>
                        </section>
                    </div>
                </div>
                <div class="large-12 columns page-template-home__contact">
                    <div class="container">
                        <section class="row expanded">
                            <div class="large-5 columns page-template-home__left">
                                <h3 class="page-template-home__left__subtitle">Masz pytania?</h3>
                                <h2 class="page-template-home__left__title">Napisz do nas</h2>
                            </div>
                            <div class="large-7 columns page-template-home__contact__contact_form">
                                <div class="main_contactform">
                                <?php
                                    echo do_shortcode('[contact-form-7 id="732" title="Kontakt"]');
                                ?>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                <div class="large-12 columns">
                    <div class="container">
                        <section class="row expanded">
                            <div class="large-4 columns page-template-home__left">
                                <h3 class="page-template-home__left__subtitle">Metody dostawy i</h3>
                                <h2 class="page-template-home__left__title">płatności</h2>
                            </div>
                            <div class="large-8 columns page-template-home__delivery">

                            <?php
                            $size = 'full'; // (thumbnail, medium, large, full or custom size)

                            if( $images ): ?>
                                <ul class="page-template-home__delivery__list">
                                    <?php foreach( $images as $image ): ?>
                                        <li>
                                            <?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php
get_footer();
