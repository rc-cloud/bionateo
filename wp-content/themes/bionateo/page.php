<?php
$main_editor = get_field('main_editor');
 get_header(); ?>
	<main class="main-wrap-cms">
        <div class="container">
            <div class="row expanded">
                <div class="large-8 columns large-offset-2 content-wrap">
					<?php echo $main_editor;?>
				</div>
			</div>
		</div>
	</main>
<?php get_footer(); ?>
