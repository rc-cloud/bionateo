<?php get_header();
	$args = array(
		'post_type' => 'product',
		'posts_per_page' => -1,
		's' => $_GET['s'],
	);
	$the_query = new WP_Query( $args );
 ?>
	<main class="main-wrap-cms">
        <div class="container">
            <div class="row expanded">
                <div class="large-12 columns">
					<div class="row expanded">
						<div class="large-6 columns">
							<div class="search__left">
								<h2 class="search__left__title">Wyniki wyszukiwania</h2>
								<h3 class="search__left__subtitle">Dla frazy “<?php echo $_GET['s']; ?>”</h3>
								<p class="search__left__info">Znaleziono produktów: <?php echo $the_query->post_count; ?></p>
							</div>
						</div>
						<div class="large-6 columns">
							<form role="search" method="get" class="search__form" action="<?php echo home_url( '/' ); ?>">
								<div class="search__form__left">
									<label>
									Zmień frazę wyszukiwania
									</label>
									<div class="search__form__left__field">
										<input type="search" class="search__form__left__field__input"
											placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>"
											value="<?php echo get_search_query() ?>" name="s"
											title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
									</div>
								</div>

								<input type="submit" class="search__form__submit"
									value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
							</form>
						</div>
					</div>
					<div class="row expanded">
						<?php

						?>
						<?php if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post(); ?>
							<article id="post-<?php the_ID(); ?>" <?php post_class('large-3 columns end single--product customHeight'); ?> role="article">					
								<?php get_template_part( 'parts/loop', 'archive' ); ?>
							</article>
						<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</main>
<?php get_footer(); ?>
