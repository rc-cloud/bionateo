<?php $page_id = 913; ?>
<footer class="footer" itemscope itemtype="http://schema.org/WPFooter">
    <div class="container">
        <div class="row expanded footer__main">
            <div class="medium-6 large-4 columns fot1">
                <div class="footer__main__menu">
                    <?php dynamic_sidebar('footer1'); ?>
                </div>
            </div>
            <div class="medium-6 large-4 columns fot2">
                <div class="footer__main__menu">
                    <?php dynamic_sidebar('footer2'); ?>
                </div>
            </div>
            <div class="large-4 columns fot3">
                <div class="footer-delivery">
                    <h2 class="footer--title">
                        <?php the_field('footer_text1', $page_id) ?>
                    </h2>

                    <div class="delivery-cont">
                        <?php
                        $images = get_field('delivery_methods_b2b', $page_id);
                        $size = 'full'; // (thumbnail, medium, large, full or custom size)

                        if ($images) :
                            ?>
                            <ul class="delivery-li">
                                <?php foreach ($images as $image) : ?>
                                    <li>
                                        <?php echo wp_get_attachment_image($image['ID'], $size); ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </div>

                </div>
                <h2 class="footer--title">
                    <?php the_field('footer_text2', $page_id) ?>
                </h2>
                <div class="footer__main__social">
                    <?php
                    if (have_rows('social_media', 'options')) :
                        echo '<ul>';
                        while (have_rows('social_media', 'options')) : the_row();
                            $fa = get_sub_field('fa');
                            $url = get_sub_field('url');
                            echo '<li><a target="_blank" href="' . $url . '"><i class="fa ' . $fa . '"></i></a></li>';
                        endwhile;
                        echo '</ul>';
                    endif;
                    ?>
                </div>
            </div>
        </div>
        <div class="row expanded margin-top-70">
            <div class="large-6 columns">
                <div class="footer__bottom-info">
                    <?php echo date('Y'); ?> © Bionateo | Strona korzysta z plików cookies | Realizacja: <a class="footer__bottom-info__tense" target="_blank" href="https://grupa-tense.pl" rel="noopener">Grupa TENSE</a>
                </div>
            </div>
            <div class="large-6 columns">
                <div class="footer__bottom-info text-right">

                </div>
            </div>
        </div>
    </div>
    <div id="scrollToTop">
        <i class="fa fa-arrow-up" aria-hidden="true"></i>
    </div>
</footer>
</div>
</div>
</div>
<?php get_template_part('parts/modal', 'search'); ?>
<?php get_template_part('parts/modal', 'login'); ?>
<?php wp_footer(); ?>
<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
<script>
    WebFont.load({
        google: {
            families: ['Lato:100,300,400,400i,700,900&subset=latin-ext']
        }
    });
</script>
<script>
		jQuery(function($) {
		$('h1').each(function() {
			if ($(this).text() === 'Promocje') {
				$('h3').each(function(){
			if($(this).text() === 'Cena') {
				$(this).css('display', 'none');
				$('.product-list-page__slider').css('display', 'none');
			}
		});
			}
		});
		});
		</script>
</body>

</html>