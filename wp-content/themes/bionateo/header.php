<!doctype html>

<html class="no-js"  <?php language_attributes(); ?>>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta class="foundation-mq">

        <!-- If Site Icon isn't set in customizer -->
        <?php if (!function_exists('has_site_icon') || !has_site_icon()) : ?>
            <!-- Icons & Favicons -->
            <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
            <link href="<?php echo get_template_directory_uri(); ?>/assets/images/apple-icon-touch.png" rel="apple-touch-icon" />
            <!--[if IE]>
                    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
            <![endif]-->
            <meta name="msapplication-TileColor" content="#f01d4f">
            <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/images/win8-tile-icon.png">
            <meta name="theme-color" content="#121212">
        <?php endif; ?>

        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">




        <?php wp_head(); ?>

        <!-- Drop Google Analytics here -->
        <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-10576593-39"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-10576593-39');
</script>

        <!-- end analytics -->

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <!-- end analytics -->
    </head>

    <body <?php body_class(); ?> >

        <?php $page_id = 913; ?>

        <style>
            .top-bar .slick-slider .slick-slide__left {
                background: none;
            }

            @media (max-width: 950px) {
                .top-bar .slick-slider .slick-slide__content {
                    background: none;
                }
            }
            .single-product__top__gallery__thumbnails {
                margin-right: 0.2rem;
            }
        </style>

        <div class="off-canvas-wrapper">

            <?php get_template_part('parts/content', 'offcanvas'); ?>

            <div class="off-canvas-content" data-off-canvas-content>

                <header id="banner" class="header" role="banner" data-sticky-container>
                    <div class="lang-header">
                        <div class="row-big">
                            <div class="columns medium-12">Wersje językowe
                            </div>
                        </div>
                    </div>
                    <div class="top-bar top-bar-mobile">
                        <div class="top-bar-right menu-menu">
                            <div style="display:inline-block;width:100%;">
                                <button class="menu-menu__close">Zamknij <img src="<?php echo get_template_directory_uri() ?>/assets/images/cross.svg"/></button>
                            </div>
                            <div class="menu-menu__menu">
                                      <?php
                                wp_nav_menu(array(
                                    'theme_location' => 'header_menu'
                                ));
                                ?>
                            </div>

                            <?php if (is_user_logged_in() == 1): ?>
                                <p class="header--title" style='font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;'>
                                    Konto
                                </p>
                                <?php
                                wp_nav_menu(array(
                                    'container' => 'false',
                                    'menu' => __('Footer Links', 'jointswp'),
                                    'menu_class' => 'menu-menu__menu',
                                    'theme_location' => 'main-nav',
                                    'depth' => 0,
                                    'fallback_cb' => ''
                                ));
                                ?>
                            <?php endif; ?>

                            <div>
                                <?php $profile_url = get_permalink(get_option('woocommerce_myaccount_page_id')); ?>
                                <a class="menu-menu__login button-b" <?php if (is_user_logged_in() != 1): ?> <?php endif; ?> href="<?php
                                if (is_user_logged_in() == 1): echo wp_logout_url();
                                else: echo $profile_url;
                                endif;
                                ?>">
                                    <?php if (is_user_logged_in() == 1): ?> Wyloguj <?php else: ?>zaloguj/zarejestruj<?php endif; ?>
                                </a>
                            </div>
                        </div>

                    </div>
                    <div class="logo-header">
                        <div class="row-big">
                            <div class="columns medium-12">
                                <a href="<?php echo home_url(); ?>"><img class="logo-img" src="<?php the_field('bionateo_logo', 'option'); ?>" alt="BIONATEO - sklep z legalnymi produktami CBD" /></a>

                                <div class="float-right small-menu">
                                    <a href="#" data-open="search-modal" class="header-search-loop">&nbsp;</a>
									                  <a href="<?php echo home_url('/my-account/'); ?>" class="header__top__profile">&nbsp;</a>
								                    <a class="shiping" href="<?php echo wc_get_cart_url(); ?>"><span><?php echo WC()->cart->get_cart_contents_count(); ?></span>&nbsp;</a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="menu-header">
                        <div class="row-big">
                            <div class="columns medium-12">
                                <?php
                                wp_nav_menu(array(
                                    'theme_location' => 'header_menu'
                                ));
                                ?>

                                <button class="burger mobile-burger">
                                    <span class="burger__line1"></span>
                                    <span class="burger__line2"></span>
                                    <span class="burger__line3"></span>
                                </button>

                                <?php if (get_field('facebook_link', 'option')) { ?>
                                    <a class="facebook-icon" target="_blank" rel="noopener nofollow" href="<?php the_field('facebook_link', 'option'); ?>">&nbsp;</a>
                                <?php } ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="icons-header">
                        <div class="row-big">
                            <div class="columns medium-12">
                                <?php
                                if (have_rows('icons_heder_rep', $page_id)):
                                    while (have_rows('icons_heder_rep', $page_id)) : the_row();
                                        ?>
                                        <div class="single-icon">
                                            <img src="<?php the_sub_field('icons_heder_rep_icon', $page_id); ?>" alt="icon" />
                                            <?php the_sub_field('icons_heder_rep_text', $page_id); ?>
                                        </div>
                                        <?php
                                    endwhile;
                                endif;
                                ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <?php get_template_part('parts/nav', 'offcanvas-topbar'); ?>
                </header>
                <div class="maska">
                </div>
