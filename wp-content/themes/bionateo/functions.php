<?php
/* Ajax files */
foreach( glob(dirname(__FILE__) . '/ajax/*.php') as $class_path ){
	require_once( $class_path );
}

// Theme support options
require_once(get_template_directory().'/assets/functions/theme-support.php');

// WP Head and other cleanup functions
require_once(get_template_directory().'/assets/functions/cleanup.php');

// Add custom widgets to theme
require_once(get_template_directory().'/assets/functions/custom-widgets.php');


// Shortcodes
require_once(get_template_directory().'/assets/functions/shortcodes.php');

// Register scripts and stylesheets
require_once(get_template_directory().'/assets/functions/enqueue-scripts.php');

// Register custom menus and menu walkers
require_once(get_template_directory().'/assets/functions/menu.php');

// Register sidebars/widget areas
require_once(get_template_directory().'/assets/functions/sidebar.php');

// Makes WordPress comments suck less
require_once(get_template_directory().'/assets/functions/comments.php');

// Replace 'older/newer' post links with numbered navigation
require_once(get_template_directory().'/assets/functions/page-navi.php');

// Adds support for multiple languages
//require_once(get_template_directory().'/assets/translation/translation.php');

// Remove 4.2 Emoji Support
require_once(get_template_directory().'/assets/functions/disable-emoji.php');

//SEO tweaks
require_once(get_template_directory().'/assets/functions/seo.php');

//acf initial fields
require_once(get_template_directory().'/assets/functions/acf.php');

// Custom contact page
require_once(get_template_directory().'/assets/functions/custom-contact.php');

// Custom logo support
function logo_theme_customizer( $wp_customize ) {

    $wp_customize->add_section( 'logo_logo_section' , array(
    	'title'       => __( 'Logo', 'elban' ),
    	'priority'    => 30,
    	'description' => 'Upload a logo for this theme',
	) );

	$wp_customize->add_setting( 'logo_logo', array(
		'default' => get_bloginfo('template_directory') . '/images/default-logo.png',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo_logo', array(
	
    	'label'    => __( 'Current logo', 'elban' ),
		'section'  => 'logo_logo_section',
		'settings' => 'logo_logo',
	) ) );	

}
add_action('customize_register', 'logo_theme_customizer');

// Adds Woocommerce Support
require_once(get_template_directory().'/assets/functions/woo.php');

// Adds site styles to the WordPress editor
//require_once(get_template_directory().'/assets/functions/editor-styles.php');

// Related post function - no need to rely on plugins
// require_once(get_template_directory().'/assets/functions/related-posts.php');

// Use this as a template for custom post types
 require_once(get_template_directory().'/assets/functions/custom-post-type.php');

// Customize the WordPress login menu
// require_once(get_template_directory().'/assets/functions/login.php');

// Customize the WordPress admin
// require_once(get_template_directory().'/assets/functions/admin.php');

// Ads custom widget template
//require_once(get_template_directory().'/assets/functions/widget.php');

function my_custom_available_payment_gateways( $gateways ) {
	$chosen_shipping_rates = ( isset( WC()->session ) ) ? WC()->session->get( 'chosen_shipping_methods' ) : array();
		
		if ( in_array( 'flat_rate:3', $chosen_shipping_rates ) ) :
		unset( $gateways['cod'] );
		
		elseif ( in_array( 'flat_rate:5', $chosen_shipping_rates ) ) :
		unset( $gateways['bacs'] );
		unset( $gateways['paypal'] );
		unset( $gateways['payu'] );
		unset( $gateways['transferuj'] );
		unset( $gateways['tpaycards'] );
		
		elseif ( in_array( 'flat_rate:4', $chosen_shipping_rates ) ) :
		unset( $gateways['cod'] );

	endif;
	return $gateways;
}
add_filter( 'woocommerce_available_payment_gateways', 'my_custom_available_payment_gateways' );


// run AFTER ACF saves the $_POST['fields'] data
add_action('woocommerce_after_shop_loop_item', 'drugiopisik');

function drugiopisik(){

echo get_field('drugi_opis');

}

function wc_customer_details( $fields, $sent_to_admin, $order ) {
	if ( empty( $fields ) ) {
		if ( $order->get_billing_email() ) {
			$fields['billing_email'] = array(
				'label' => __( 'Adres e-mail', 'woocommerce' ),
				'value' => wptexturize( $order->get_billing_email() ),
			);
		}
		if ( $order->get_billing_phone() ) {
			$fields['billing_phone'] = array(
				'label' => __( 'Numer telefonu', 'woocommerce' ),
				'value' => wptexturize( $order->get_billing_phone() ),
			);
		}
	}
	return $fields;
}
add_filter( 'woocommerce_email_customer_details_fields', 'wc_customer_details', 10, 3 );

/**
 * Remove Categories from WooCommerce Product Category Widget
 *
 * @author   Ren Ventura
 */
 
//* Used when the widget is displayed as a dropdown
add_filter( 'woocommerce_product_categories_widget_dropdown_args', 'rv_exclude_wc_widget_categories' );
//* Used when the widget is displayed as a list
add_filter( 'woocommerce_product_categories_widget_args', 'rv_exclude_wc_widget_categories' );
function rv_exclude_wc_widget_categories( $cat_args ) {
	$cat_args['exclude'] = array('104'); // Insert the product category IDs you wish to exclude
	return $cat_args;
}

function tense_wpseo_json_ld_output($data) {
 return [];
}
add_filter("wpseo_json_ld_output", "tense_wpseo_json_ld_output", 99);

if ( ! function_exists( 't5_add_page_number' ) )
{
    function t5_add_page_number( $s )
    {
        global $page;
        $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
        ! empty ( $page ) && 1 < $page && $paged = $page;

        $paged > 1 && $s .= ' - ' . sprintf( __( 'Page: %s' ), $paged );

        return $s;
    }

    add_filter( 'wpseo_replacements', 't5_add_page_number', 100, 1 );
}
