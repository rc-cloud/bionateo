<?php
/**
*   Template name: O nas
*
*   main_paragraph - text area
*   content - wysiwyg
*   list - repeater
*       + img - image,array
*       + title - text
*       + content - text area
*/
get_header();
$main_paragraph = get_field('main_paragraph');
$content = get_field('content');
?>
    <main class="main-wrap-cms">
        <div class="container">
            <div class="row expanded">
                <div class="large-8 columns large-offset-2">
                    <div class="main_paragraph">
                        <?php echo $main_paragraph;?>
                    </div>
                    <div class="content">
                        <?php echo $content;?>
                    </div>
                    <div class="about-list">
                        <?php
                        if( have_rows('list') ):
                            echo '<ul class="row about-list__main" data-equalizer data-equalize-by-row="true">';
                            while ( have_rows('list') ) : the_row();
                                $img = get_sub_field('img');
                                $title = get_sub_field('title');
                                $content = get_sub_field('content');
                            ?>
                            <li class="medium-6 columns end about-list__main__li" data-equalizer-watch>
                                <div class="about-list__main__img">
                                    <img src="<?php echo $img['url'];?>" alt="<?php echo $img['alt']; ?>"/>
                                </div>
                                <div class="about-list__main__wrap">
                                    <h2 class="about-list__main__wrap__title"><?php echo $title; ?></h2>
                                    <p class="about-list__main__wrap__content">
                                        <?php echo $content;?>
                                    </p>
                                </div>
                            </li>
                            <?php
                            endwhile;
                            echo '</ul>';
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php
get_footer();
