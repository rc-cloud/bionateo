<?php
/**
*   Template name: Porównaj produkty
*
*/
get_header();
$user_id = get_current_user_id();
$user_compare = get_user_meta( $user_id, 'u_compare');
?>
    <main class="main-wrap-cms">
        <div class="container">
            <div class="row expanded">
                <div class="small-12 columns">
                    <div class="row expanded">
                        <div class="small-12 columns">
                            <h2 class="page-template-compare__title">
                                Porównaj produkty:
                            </h2>
                        </div>
                    </div>
                    <?php
                    $args = array(
                        'post_type' => 'product',
                        'posts_per_page' => -1,
                        'post__in' => $user_compare[0],
                    );
                    $the_query = new WP_Query( $args );
                    ?>
                    <div class="row expanded">
                        <div class="wrap-fav">
                        <?php if ($the_query->have_posts()) : $i=0; while ($the_query->have_posts()) : $the_query->the_post();
                            $extra_class = 'large-4 columns end single--product';
                            if($i != 0):
                                $extra_class .= ' first-col';
                            endif;    
                            ?>
                                <article id="post-<?php the_ID(); ?>" <?php post_class($extra_class); ?> role="article">					
                                    <?php get_template_part( 'parts/loop', 'archive-compare' ); ?>
                                </article>
                            <?php $i++; endwhile; ?>	
                        <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php
get_footer(); 