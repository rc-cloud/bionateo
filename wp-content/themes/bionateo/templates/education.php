<?php
/**
* Template name: Edukacja
*
*
*
*
*
**/
get_header();
?>
    <main class="main-wrap-cms">
        <div class="container">
            <div class="row expanded">
                <div class="large-4 columns">
                    <h2 class="edu-suggest">Czy masz pytania bądź<br/><span>sugestię?</span></h2>
                    <a class="button-b edu-url" href="<?php echo get_the_permalink(666); ?>">Skontaktuj się z nami</a>
                </div>
                <div class="large-8 columns">
                <?php
                    if( have_rows('edu_fields') ):
                    $ac = 0;
                        echo '<ul class="accordion" data-accordion data-allow-all-closed="true">';
                        while ( have_rows('edu_fields') ) : the_row();
                                $name = get_sub_field('name');
                                $content = get_sub_field('content');
                        ?>
                        <li class="accordion-item <?php if($ac==0):?>is-active<?php endif;?>" data-accordion-item>
                            <a href="#" class="accordion-title"><?php echo $name;?> <button class="accordion-cross"><span class="nt1"></span><span class="nt2"></span><span class="nt3"></span></button></a>
                            <div class="accordion-content" data-tab-content>
                                <?php echo $content;?>
                            </div>
                        </li>
                        <?php
                        $ac++;
                        endwhile;
                        
                        echo '</ul>';
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </main>
<?php
get_footer();