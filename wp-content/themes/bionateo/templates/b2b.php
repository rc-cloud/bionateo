<?php
/**
*   Template name: B2B
*
*/
get_header();
?>
    <main class="main-wrap-cms">
        <div class="">
            <div class="row expanded">
                <div class="large-12 columns page-template-b2b__bottom">
                    <div class="container">
                        <section class="row expanded">
                            <div class="large-3 columns page-template-b2b__left">
                                <h3 class="page-template-b2b__left__subtitle">Produkty</h3>
                                <h2 class="page-template-b2b__left__title">Nowości</h2>
                                <a class="page-template-b2b__left__url button-b" href="">Zobacz więcej</a>
                            </div>
                            <div class="large-9 columns  page-template-b2b__new">
                                <div class="slick slick--custom">
                                    <?php
                                    $args = array(
                                        'post_type' => 'product',
                                        'posts_per_page' => 9,
                                        //'post__in' => $user_compare[0],
                                    );
                                    $the_query = new WP_Query( $args );
                                    ?>
                                    <?php if ($the_query->have_posts()) : $i=0; while ($the_query->have_posts()) : $the_query->the_post();
                                        $extra_class = ' single--product';
                                        if($i != 0):
                                            $extra_class .= ' first-col';
                                        endif; 
                                        if(get_field('new_checkbox')== 1):   
                                        ?>
                                            <article id="post-<?php the_ID(); ?>" <?php post_class($extra_class); ?> role="article">					
                                                <?php get_template_part( 'parts/loop', 'archive' ); ?>
                                            </article>
                                        <?php
                                        endif;
                                        $i++; endwhile;
                                         ?>	
                                    <?php endif; ?>
                            </div>
                        </section>
                    </div>
                </div>    
                <div class="large-12 columns page-template-b2b__bottom">
                    <div class="container"> 
                        <section class="row expanded">
                            <div class="large-3 columns page-template-b2b__left">
                                <h3 class="page-template-b2b__left__subtitle">Produkty</h3>
                                <h2 class="page-template-b2b__left__title">Promocyjne</h2>
                                <a class="page-template-b2b__left__url button-b" href="">Zobacz więcej</a>
                            </div>
                            <div class="large-9 columns  page-template-b2b__promo">
                                <div class="slick slick--custom">
                                    <?php
                                    $args = array(
                                        'post_type' => 'product',
                                        'posts_per_page' => 9,
                                        //'post__in' => $user_compare[0],
                                    );
                                    $the_query = new WP_Query( $args );
                                    ?>
                                    <?php if ($the_query->have_posts()) : $i=0; while ($the_query->have_posts()) : $the_query->the_post();
                                        $extra_class = ' single--product';
                                        if($i != 0):
                                            $extra_class .= ' first-col';
                                        endif; 
                                        if(get_field('promo_checkbox')== 1):      
                                        ?>
                                            <article id="post-<?php the_ID(); ?>" <?php post_class($extra_class); ?> role="article">					
                                                <?php get_template_part( 'parts/loop', 'archive' ); ?>
                                            </article>
                                        <?php
                                        endif;
                                        $i++; endwhile; ?>	
                                    <?php endif; ?>
                            </div>
                        </section>
                    </div>
                </div>   
                <div class="large-12 columns page-template-b2b__bottom">
                    <div class="container">     
                        <section class="row expanded">
                            <div class="large-3 columns page-template-b2b__left">
                                <h3 class="page-template-b2b__left__subtitle">Produkty</h3>
                                <h2 class="page-template-b2b__left__title">dla ciebie</h2>
                                <a class="page-template-b2b__left__url button-b" href="">Zobacz więcej</a>
                            </div>
                            <div class="large-9 columns page-template-b2b__featured">
                                <div class="slick slick--custom">
                                    <?php
                                    $meta_query[] = array(
                                        'key'   => '_featured',
                                        'value' => 'yes'
                                    );
                                    $args = array(
                                        'post_type' => 'product',
                                        'posts_per_page' => 9,
                                        'tax_query' => array(
                                            array(
                                                'taxonomy' => 'product_visibility',
                                                'field'    => 'name',
                                                'terms'    => 'featured',
                                                'operator' => 'IN'
                                            ),
                                    ),
                                    );
                                    $the_query = new WP_Query( $args );
                                    ?>
                                    <?php if ($the_query->have_posts()) : $i=0; while ($the_query->have_posts()) : $the_query->the_post();
                                        $extra_class = ' single--product';
                                        if($i != 0):
                                            $extra_class .= ' first-col';
                                        endif;    
                                        ?>
                                            <article id="post-<?php the_ID(); ?>" <?php post_class($extra_class); ?> role="article">					
                                                <?php get_template_part( 'parts/loop', 'archive' ); ?>
                                            </article>
                                        <?php $i++; endwhile; ?>	
                                    <?php endif; ?>
                            </div>
                        </section>
                    </div>
                </div>
                <div class="large-12 columns">
                    <div class="container">    
                        <section class="row expanded">
                            <div class="large-3 columns page-template-b2b__left">
                                <h3 class="page-template-b2b__left__subtitle">Metody dostawy i</h3>
                                <h2 class="page-template-b2b__left__title">płatności</h2>
                            </div>
                            <div class="large-9 columns page-template-b2b__delivery">
                            <?php 
                             if (have_posts()) : while (have_posts()) : the_post();
                             $images = get_field('delivery_methods_b2b');
                             endwhile; ?>	
                            <?php endif; 
                            
                            $size = 'full'; // (thumbnail, medium, large, full or custom size)
                        
                            if( $images ): ?>
                                <ul class="page-template-b2b__delivery__list">
                                    <?php foreach( $images as $image ): ?>
                                        <li>
                                            <?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php
get_footer();