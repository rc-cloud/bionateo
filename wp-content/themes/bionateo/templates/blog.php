<?php
/**
 *   Template name: Blog
 *
 */
get_header();
?>
<main class="main-wrap-cms">
    <div class="container">
        <div class="row row2">
            
                <div class="home-categories home-blog">
                    <div class="blog-cont">
                        <?php
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        $args = array(
                            'post_type' => 'post',
                            'paged' => $paged,
                            'posts_per_page' => 12
                        );

                        $query1 = new WP_Query($args);

                        if ($query1->have_posts()) {
                            while ($query1->have_posts()) {
                                $query1->the_post();
                                ?>

                                <div class="columns medium-4 eq-height-blog">
                                    <div class="box">
                                        <div class="img" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID(), 'foundation-small'); ?>)"></div>
                                        <div class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
                                        <div class="excerpt"><?php the_excerpt(); ?></div>
                                        <a href="<?php the_permalink(); ?>" class="box-link"></a>
                                        <div class="more-cont">
                                            <a class="button-b" href="<?php the_permalink(); ?>"><?php the_field('home_blog_link_text_box', 913); ?></a>
                                        </div>
                                    </div>
                                </div>

                                <?php
                            }
                        }
                        ?>
                        <div class="clearfix"></div>

                        <div class="columns medium-12">
                            <div class="pagination">
                                <?php
                                global $query1;
                                $pager = 999999999; // need an unlikely integer

                                echo paginate_links(array(
                                    'base' => str_replace($pager, '%#%', esc_url(get_pagenum_link($pager))),
                                    'format' => '?paged=%#%',
                                    'current' => max(1, get_query_var('paged')),
                                    'total' => $query1->max_num_pages,
                                    'prev_text' => __(''),
                                    'next_text' => __(''),
                                ));
                                ?>
                            </div>
                        </div>

                    </div>
                </div>

        </div>
    </div>
</main>



<?php
get_footer();
