<?php
/**
*   Template name: Kontakt
*   
*   main_paragraph - text area
*   form - rel
**/
get_header();

$form = get_field('form');
$main_paragraph = get_field('main_paragraph');
?>
    <main class="main-wrap-cms">
        <div class="container">
            <div class="row expanded">
                <div class="large-8 columns large-offset-2">
                    <div class="main_paragraph">
                        <?php echo $main_paragraph;?>
                    </div>
                    <div class="main_contactform">
                        <?php echo do_shortcode( '[contact-form-7 id="'.$form[0].'"]' ) ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
	
	
<style>
    .checkbox-156:after {
        display: none !important;
    }
    .checkbox-156 span.wpcf7-list-item {
        margin: 0;
    }
    input[type="checkbox"] {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        width: 24px;
        height: 24px !important;
        border: 2px solid #8eb81f !important;
        display: inline-block !important;
        padding: 0 !important;
        float: left;
        z-index: 9;
        position: relative;
        background: #fefefe;
        margin-bottom: 0;
        margin-right: 5px;
        cursor: pointer;
    }
    .checkbox-156 .wpcf7-list-item-label {
        position: relative;
		font-size: 16px;
		display: block;
		padding-left: 30px;
    }
    .checkbox-156 .wpcf7-list-item-label:after {
        width: 24px;
        height: 24px !important;
        border: 2px solid #8eb81f !important;
        opacity: 0.5;
        position: absolute;
        content: '';
        top: 5px;
		left: -6px;
        transition: .3s;
    }
    .checkbox-156 .wpcf7-list-item-label:before {
        width: 24px;
        height: 24px;
        background-image: url(<?php echo get_template_directory_uri() ?>/assets/images/tick.png);
        background-position: center;
        background-repeat: no-repeat;
        content: '';
        display: block;
		left: 0;
		top: 0;
        z-index: 2;
        opacity: 0;
        transition: .3s;
        position: absolute;
    }
    input[type="checkbox"]:checked + span:before {
        opacity: 1;
    }
    input[type="checkbox"]:checked {
        background: transparent;
    }
    .checkbox-156 input[type="checkbox"]:checked + .wpcf7-list-item-label:after {
		left: 0;
		top: 0;
    }
    .main_contactform .checkbox-156 .wpcf7-not-valid-tip {
        display: block;   
        border: 2px solid #e74c3c;
        color: #e74c3c;
        padding: 0.4375rem;
        margin-top: 20px;
    }
	@media (max-width: 1023px) {
		.checkbox-156 .wpcf7-list-item-label {
			line-height: 20.8px;
		}
	}
	@media (max-width: 640px) {
		.checkbox-156 .wpcf7-list-item-label {
			line-height: 18.2px;
		}
	}
</style>

	
<?php
get_footer();