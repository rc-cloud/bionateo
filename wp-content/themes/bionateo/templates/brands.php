<?php 

/**
*   Template name: Marki
*   
*   main_paragraph - text area
*   brands - repeater
*       + logo - image
*       + title - text
*       + content - text area
*       + url_name - text
*       + url - text
**/
get_header();
$main_paragraph = get_field('main_paragraph');
?>
    <main class="main-wrap-cms">
        <div class="container">
            <div class="row expanded">
                <div class="large-8 columns large-offset-2">
                    <div class="main_paragraph">
                        <?php echo $main_paragraph;?>
                    </div>
                    <div class="brand-list">
                    <?php
                        if( have_rows('brands') ):
                            while ( have_rows('brands') ) : the_row();
                                $logo = get_sub_field('logo');
                                $title = get_sub_field('title');
                                $content = get_sub_field('content');
                                $url_name = get_sub_field('url_name');
                                $url = get_sub_field('url');
                            ?>
                            <div class="brand-list__single row expanded">
                                <div class="large-3 columns brand-list__single__img">
                                    <img src="<?php echo $logo['sizes']['logo_brands']; ?>"/>
                                </div>
                                <div class="large-9 columns end">
                                    <div class="brand-list__single__wrap">
                                        <h2 class="brand-list__single__wrap__title">
                                            <?php echo $title;?>
                                        </h2>
                                        <div class="brand-list__single__wrap__content">
                                            <?php echo $content;?>
                                        </div>
                                        <?php if($url_name):?>
                                        <a class="brand-list__single__wrap__url" href="<?php echo $url;?>" target="_blank">
                                            <?php echo $url_name; ?>
                                        </a>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                            <?php
                            endwhile;
                        endif;
                        ?>
                    </div>
                    
                </div>
            </div>
        </div>
    </main>
<?php
get_footer();