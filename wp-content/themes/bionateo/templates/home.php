<?php
/**
 *   Template name: Home
 *
 */
get_header();
?>
<?php
if (have_posts()) : while (have_posts()) : the_post();
        $images = get_field('delivery_methods_b2b');
        $brands = get_field('brands');
        $content = get_field('content');
        $home_text_cat = get_field('home_text_cat');
    endwhile;
    ?>
<?php endif; ?>
<main class="main-wrap-cms">

    <div class="home-categories home-best">
        <div class="row-big">
            <div class="columns medium-12">
                <div class="home-text-cat">
                    <?php the_field('home_text_bestseller'); ?>
                </div><div class="clearfix"></div>
            </div>

            <div class="best-sellers-home">

                <?php
                $posts = get_field('home_bestseller');
                get_template_part('parts/home', 'products');
                ?>
                <div class="clearfix"></div>
                <!--<div class="more-cont">
                    <a class="button-b" href="<?php the_field('home_bestseller_more_link'); ?>"><?php the_field('home_bestseller_more_text'); ?></a>
                </div>-->

                <div class="clearfix"></div>
            </div>


        </div>
    </div>

    <div class="home-categories">
        <div class="row">
            <div class="columns medium-12">
                <div class="home-text-cat">
                    <?php echo $home_text_cat; ?>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="page-template-home__products__list" data-equalizer>
                <?php
                if (have_rows('product')): $i = 0;
                    while (have_rows('product')) : the_row();
                        $icon = get_sub_field('icon');
                        $title = get_sub_field('title');
                        $description = get_sub_field('description');
                        $img = get_sub_field('img');
                        $link = get_sub_field('link');
                        $sort = '';
                        if ($i % 2 === 0) {
                            $sort = 'sort2';
                        } else {
                            $sort = 'sort1';
                        }
                        ?>
                        <div class="columns medium-4" data-equalizer-watch>

                            <div class="home-cat-box">
                                <a class="home-cat-box__url" href="<?php echo get_the_permalink(773) ?>?filtr_product_cat=<?php echo $link->slug; ?>"></a>
                                <div class="home-cat-box__icon">
                                    <img src="<?php echo $icon['url']; ?>"/>
                                </div>
                                <h3 class="home-cat-box__title"><?php echo $title; ?></h3>
                                <img class="home-cat-box__img" src="<?php echo $img['url'] ?>"/>
                            </div>
                        </div>
                        <?php
                        $i++;
                    endwhile;
                endif;
                ?>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="home-categories home-best home-sale">
        <div class="row-big">
            <div class="columns medium-12">
                <div class="home-text-cat">
                    <?php the_field('home_text_sale'); ?>
                </div><div class="clearfix"></div>
            </div>

            <div class="best-sellers-home">

                <?php
                $posts = get_field('home_sale');
                get_template_part('parts/home', 'products');
                // echo do_shortcode('[sale_products limit="6" columns="6"]');
                ?>
                <div class="clearfix"></div>
                <div class="more-cont">
                    <a class="button-b" href="<?php the_field('home_sale_more_link'); ?>"><?php the_field('home_sale_more_text'); ?></a>
                </div>

                <div class="clearfix"></div>
            </div>

        </div>
    </div>

    <?php if (get_field('home_ask_on_off')) { ?>

        <div class="home-categories home-questions">
            <div class="row-big">
                <div class="columns medium-12">
                    <div class="home-text-cat">
                        <?php the_field('home_ask_title'); ?>
                    </div><div class="clearfix"></div>
                </div>

                <?php
                if (have_rows('home_ask_rep')):
                    while (have_rows('home_ask_rep')) : the_row();
                        ?>
                        <div class="columns medium-4">
                            <div class="box">
                                <div class="icon"><img src="<?php the_sub_field('home_ask_rep_icon'); ?>" alt="" /></div>
                                <div class="question"><?php the_sub_field('home_ask_rep_question'); ?></div>
                                <div class="answer"><?php the_sub_field('home_ask_rep_answer'); ?></div>
                            </div>
                        </div>
                        <?php
                    endwhile;
                endif;
                ?>
                <div class="clearfix"></div>
            </div>
        </div>

    <?php } ?>

    <?php if (get_field('home_blog_on_off')) { ?>

        <div class="home-categories home-blog">
            <div class="row-big">
                <div class="columns medium-12">
                    <div class="home-text-cat">
                        <?php the_field('home_blog_title'); ?>
                    </div><div class="clearfix"></div>
                </div>
            </div>

            <div class="row row2" data-equalizer>
                <?php
                $posts = get_field('home_blog_posts');

                if ($posts):
                    ?>
                    <?php foreach ($posts as $post): ?>
                        <?php setup_postdata($post); ?>
                        <div class="columns medium-4" data-equalizer-watch>
                            <div class="box">
                                <div class="img" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID(), 'foundation-small'); ?>)"></div>
                                <div class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
                                <div class="excerpt"><?php the_excerpt(); ?></div>
                                <a href="<?php the_permalink(); ?>" class="box-link"></a>
                                <div class="more-cont">
                                    <a class="button-b" href="<?php the_permalink(); ?>"><?php the_field('home_blog_link_text_box', 913); ?></a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>
                <div class="clearfix"></div>
                <div class="columns medium-12">
                    <div class="more-cont main">
                        <a class="button-b" href="<?php the_field('home_blog_link'); ?>"><?php the_field('home_blog_link_text'); ?></a>
                    </div>
                </div>
            </div>

        </div>

    <?php } ?>

    <div class="about-us-cont">
        <div class="row-big">
            <div class="columns medium-12">
                <div class="content">
                    <?php echo $content; ?>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="home-newsletter">
        <div class="row-small">
            <div class="columns medium-12">
                <div class="home-newsletter__title1">
                    <?php the_field('home_newsletter_title1'); ?>
                </div>
                <div class="home-newsletter__title2">
                    <?php the_field('home_newsletter_title2'); ?>
                </div>
                <div class="home-newsletter__desc">
                    <?php the_field('home_newsletter_desc'); ?>
                </div>

                <div class="home-newsletter__form main_contactform">

                    <div class="freshmail-cont">
                        <?php echo do_shortcode('[FM_form id="1"]'); ?>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

</main>
<?php
get_footer();
