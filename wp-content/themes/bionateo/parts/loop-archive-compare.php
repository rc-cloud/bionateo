<?php
global $post;
global $product;
if ($product):
    $post_id = get_the_ID();
    $user_id = get_current_user_id();
    $thumb = get_the_post_thumbnail_url($post_id, 'full');
    $_product = wc_get_product($post_id);
    $sizes = $_product->get_variation_attributes()['Rozmiar'];
    $con = $_product->get_variation_attributes()['Stężenie'];
    $regular_price = $_product->get_regular_price();
    $sale_price = $_product->get_sale_price();
    $price = wc_price($_product->get_price());
    $currency = get_woocommerce_currency_symbol();
    $promo = $sale_price - $regular_price;
    $u_fav = get_user_meta($user_id, 'u_fav');
    $data_in_fav = 0;
    $fa_heart = 'fa-heart-o';
    $fav_text = 'Dodaj do ulubionych';
    if ($u_fav) {
        foreach ($u_fav[0] as $u) {
            if ($u == $post_id) {
                $data_in_fav = 1;
                $fa_heart = 'fa-heart';
                $fav_text = 'Usuń z ulubionych';
                break;
            }
        }
    }
    ?>
    <div class="single--product__img">
        <img class="single--product__img__image" alt="<?php the_title(); ?>" src="<?php echo $thumb; ?>">
    </div>
    <a href="<?php the_permalink(); ?>"><h2 class="single--product__title"><?php the_title(); ?></h2></a>
    <div class="single--product__prices">
        <span class="single--product__prices__regular"><?php if ($product->get_price_html() && $product->is_type('variable')) {
        echo 'od';
    } ?>
            <?php echo $price; ?>
    <?php echo $product->get_price_html(); ?>
        </span>
    </div>
    <div class="single--product__compare-remove">
        <button data-id="<?php echo $post_id; ?>" data-user="<?php echo $user_id; ?>" class="single--product__compare-remove__button">Usuń z porównania</button>
    </div>
    <div class="single--product__rating">
    <?php get_template_part('woo/single-product/rating'); ?>
    </div>
    <div class="single--product__description">
        <h3 class="single--product__description__title">
            Opis
        </h3>
        <div class="single--product__description__content">
    <?php echo apply_filters('woocommerce_short_description', $post->post_excerpt); ?>
        </div>
    </div>
    <div class="single--product__sizes">
        <h3 class="single--product__sizes__title">
            Dostępne rozmiary
        </h3>
        <div class="single--product__sizes__content single--product-list-sizes">
            <?php
            $size_cat = wp_get_post_terms($post_id, 'size_cat', array(
                'orderby' => 'id',
                    ));
            if ($sizes):
                echo '<ul>';
                foreach ($sizes as $s):
                    echo '<li>' . $s . '</li>';
                endforeach;
                echo '</ul>';
            endif;
            ?>
        </div>
    </div>
    <div class="single--product__sizes">
        <h3 class="single--product__sizes__title">
            Dostępne stężenia
        </h3>
        <div class="single--product__sizes__content single--product-list-concentration">
            <?php
            $concentration_cat = wp_get_post_terms($post_id, 'concentration_cat', array(
                'orderby' => 'id',
                    ));
            if ($con):
                echo '<ul>';
                foreach ($con as $s):
                    echo '<li>' . $s . '</li>';
                endforeach;
                echo '</ul>';
            endif;
            ?>
        </div>
    </div>
    <a href="<?php the_permalink(); ?>" class="button-b single--product__add-to-cart-bottom" data-id="<?php echo $post_id; ?>">
        <span class="single--product__add-to-cart-bottom__text" >Zobacz produkt</span>
        <span class="single--product__add-to-cart-bottom__spinner"><i class="fa fa-spinner" aria-hidden="true"></i><span>
                </a>
<?php endif; ?>