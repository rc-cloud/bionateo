<!-- By default, this menu will use off-canvas for small
         and a topbar for medium-up -->

<div class="top-bar" id="top-bar-menu">
    <?php if (is_search()): ?>
        <div class="top-bar__bread-title">
            <div class="top-bar__bread-title__breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
                <?php
                if (function_exists('bcn_display')) {
                    bcn_display();
                }
                ?>
            </div>
            <h1 class="top-bar__bread-title__title">Wyniki wyszukiwania</h1>
        </div>
    <?php elseif (is_page_template('templates/b2b.php') || is_page_template('templates/home.php')):
        /*
          header_slider
          +bg
          +img
          +title
          +category
          +description
          +button_url
         */
        ?>
        <div class="slick-header slick--custom">
            <?php
            if (have_rows('header_slider')):
                $ifek = 0;
                while (have_rows('header_slider')) : the_row();
                    $bg = get_sub_field('bg');
                    $img = get_sub_field('img');
                    $title = get_sub_field('title');
                    $category = get_sub_field('category');
                    $button_url = get_sub_field('button_url');
                    $button2_text = get_sub_field('button2_text');
                    $button1_url = get_sub_field('button1_link');
                    $button1_text = get_sub_field('button1_text');
                    $description = get_sub_field('description');
                    ?>
                    <div class="slick-slide" style="background-image:url(<?php echo $bg['url']; ?>)">
                        <div class="container">
                            <div class="row expanded">
                                <div class="large-5 columns slick-slide__content">
                                    <div class="slick-slide__content__wrap">

                                        <h3 class="slick-slide__content__wrap__title"><?php echo $title; ?></h3>
                                        <h2 class="slick-slide__content__wrap__subtitle"><?php echo $category; ?></h2>
                                        <p class="slick-slide__content__wrap__content"><?php echo $description; ?></p>
                                        <?php if ($button1_url && $button1_text) { ?>
                                            <a class="button-b button-white" href="<?php echo $button1_url; ?>"><?php echo $button1_text; ?></a>
                                        <?php } ?>
                                        <?php if ($button_url && $button2_text) { ?>
                                            <a class="button-b button-white" href="<?php echo $button_url; ?>"><?php echo $button2_text; ?></a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="slick-slide__left">
                        </div>
                        <img class="slick-slide__img" src="<?php echo $img['url']; ?>"/>
                    </div>
                    <?php
                endwhile;
            endif;
            ?>
        </div>
    <?php elseif (is_404()): ?>
        <div class="black-hover">
            <div class="top-bar__error">
                <h1 class="top-bar__error__title">404</h1>
                <h2 class="top-bar__error__subtitle">strona nie istnieje</h2>
                <a class="button-b button-white top-bar__error__url " href="/">Wróć na stronę główną</a>
            </div>
        </div>
    <?php else: ?>
        <div class="top-bar__bread-title">
            <div class="top-bar__bread-title__breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
                <?php
                if (is_singular('post')) {
                    ?>
                    <span property="itemListElement" typeof="ListItem">
                        <a property="item" typeof="WebPage" title="Go to Bionateo - sklep konopie siewne, olejki CBD." href="<?php echo home_url('/'); ?>" class="home">
                            <span property="name">Strona główna</span>
                        </a>
                        <meta property="position" content="1"></span> /
                    <span property="itemListElement" typeof="ListItem">
                        <a property="item" typeof="WebPage" title="Go to the Uncategorized category archives."
                           href="<?php echo home_url('/blog/'); ?>" class="taxonomy category">
                            <span property="name">Blog</span>
                        </a>
                        <meta property="position" content="2"></span> /
                    <span property="itemListElement" typeof="ListItem">
                        <span property="name"><?php echo single_post_title(); ?></span>
                        <meta property="position" content="3">
                    </span>
                    <?php
                } else {
                    if (function_exists('bcn_display')) {
                        bcn_display();
                    }
                }
                ?>
            </div>

        </div>
    <?php endif; ?>
</div>

<?php if (!is_page_template('templates/b2b.php') && !is_page_template('templates/home.php') && !is_404()) { ?>
    <?php
    if (is_archive()):
        $archive_name = get_field('archive_name', 'option');
        ?>
        <h1 class="top-bar__bread-title__title" style="color: #909090;text-align: center;font-size: 23px;line-height:2rem;"><?php
            if (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/olejek-cbd/medihemp/") !== false) {
                echo 'Olejki CBD Medihemp';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/olejek-cbd/cibdol/") !== false) {
                echo 'Olejki CBD Cibdol';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/kapsulki-i-gumy-cbd/") !== false) {
                echo 'Kapsułki i gumy CBD';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/olejek-cbda/") !== false) {
                echo 'Olejek CBDA';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/olej-cbd-5/") !== false) {
                echo 'Olej CBD 5%';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/olej-cbd-10/") !== false) {
                echo 'Olej CBD 10%';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/olej-cbd-15/") !== false) {
                echo 'Olej CBD 15%';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/olejek-cbd/olejki-biobloom/") !== false) {
                echo 'Olejki CBD BioBloom';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/olejek-cbd/cannabigold/") !== false) {
                echo 'Olejki CBD Cannabigold';
            } elseif (function_exists('is_product_category') && is_product_category('olejek-cbd')) {
                echo 'Olej CBD / Krople CBD - Atrakcyjne ceny';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/pasty-cbd-cbda/") !== false) {
                echo 'Pasta konopna CBD - ekstrakt z konopi CBD';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/artykuly-spozywcze/herbata/") !== false) {
                echo 'Herbata konopna - Herbata z konopi od Bionateo';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/ekstrakty-cbd/") !== false) {
                echo 'Ekstrakty CBD';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/kapsulki/") !== false) {
                echo 'Kapsułki';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/ochrona-skory-kosmetyki/") !== false) {
                echo 'Ochrona skóry / Kosmetyki';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/zywnosc/") !== false) {
                echo 'Żywność';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/cbd-vapping/") !== false) {
                echo 'Waporyzacja CBD';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/shop/plyn-cbd-meladol-30ml/") !== false) {
                echo 'Olej z konopi na sen CDB Meladol 30 ML';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/waporyzacja/susz-cbd/") !== false) {
                echo 'Susz CBD';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/waporyzacja/liquidy-cbd/") !== false) {
                echo 'Liquidy CBD';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/waporyzacja/") !== false) {
                echo 'Waporyzacja';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/artykuly-spozywcze/herbata/") !== false) {
                echo 'Herbata z konopi';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/artykuly-spozywcze/") !== false) {
                echo 'Artykuły spożywcze';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/dermokosmetyki-cbd-ochrona-skory/") !== false) {
                echo 'Dermokosmetyki z CBD';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/kapsulki-cbd/") !== false) {
                echo 'Kapsułki i gumy CBD';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/pozostale-suplementy/") !== false) {
                echo 'Pozostałe suplementy';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/produkty-do-dezynfekcji/") !== false) {
                echo 'Produkty do dezynfekcji';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/altaio/") !== false) {
                echo 'ALTAIO';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/annabis/") !== false) {
                echo 'Annabis';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/biobloom/") !== false) {
                echo 'BioBloom';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/cannabigold-2/") !== false) {
                echo 'CanabiGold';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/cibdol-2/") !== false) {
                echo 'Cibdol';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/cibiday/") !== false) {
                echo 'Cibiday';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/harmony/") !== false) {
                echo 'Harmony';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/hemptouch/") !== false) {
                echo 'Hemptouch';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/india/") !== false) {
                echo 'India Cosmetics';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/invex-remedies/") !== false) {
                echo 'Invex Remedies';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/jointbox/") !== false) {
                echo 'JointBOX';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/kombinat/") !== false) {
                echo 'Kombinat Konopny';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/kategoria-produktu/medi-wiet/") !== false) {
                echo 'Medi-Wiet';
            } elseif (strpos($_SERVER['REQUEST_URI'], "/shop/") !== false) {
                echo 'Przegląd wszystkich produktów z konopi';
            } elseif ((function_exists('is_product_category') && is_product_category()) || (function_exists('is_product_tag') && is_product_tag())) {
              $qo = get_queried_object();
              if(isset($qo->name)){
                echo $qo->name;
              }
              else {
                echo $archive_name;
              }
            } else {
                echo $archive_name;
            }
            ?></h1>
    <?php else: ?>
        <h1 class="top-bar__bread-title__title" style="color: #909090;text-align: center;font-size: 23px;line-height:2rem;"><?php
            if (strpos($_SERVER['REQUEST_URI'], "/shop/olej-z-konopi-na-sen-cbd-meldol-30ml/") !== false)
                echo 'Olej z konopi na sen CDB Meladol 30 ML';
            elseif (strpos($_SERVER['REQUEST_URI'], "/shop/cbd-10/") !== false)
                echo 'Olejek Cibdol CBD 10% - mocny';
            else
                the_title();
            ?></h1>
    <?php endif; ?>
    <div class="breadcrumbs-cont">
        <div class="container">
            <div class="columns medium-12">
                <div class="top-bar__bread-title__breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
                    <?php
                    if (is_singular('post')) {
                        ?>
                        <span property="itemListElement" typeof="ListItem">
                            <a property="item" typeof="WebPage" title="Go to Bionateo - sklep konopie siewne, olejki CBD." href="<?php echo home_url('/'); ?>" class="home">
                                <span property="name">Strona główna</span>
                            </a>
                            <meta property="position" content="1"></span> /
                        <span property="itemListElement" typeof="ListItem">
                            <a property="item" typeof="WebPage" title="Go to the Uncategorized category archives."
                               href="<?php echo home_url('/blog/'); ?>" class="taxonomy category">
                                <span property="name">Blog</span>
                            </a>
                            <meta property="position" content="2"></span> /
                        <span property="itemListElement" typeof="ListItem">
                            <span property="name"><?php echo single_post_title(); ?></span>
                            <meta property="position" content="3">
                        </span>
                        <?php
                    } else {
                        if (function_exists('bcn_display')) {
                            bcn_display();
                        }
                    }
                    ?>
                </div>
            </div><div class="clearfix"></div>
        </div>
    </div>
<?php } ?>
