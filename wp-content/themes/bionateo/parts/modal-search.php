<div class="reveal full" id="search-modal" data-reveal data-animation-in="fade-in" data-animation-out="fade-out">
    <div class="modal__content">
    <form role="search" method="get" class="woocommerce-product-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
        <h3 class="page-template-b2b__left__subtitle">Czego</h3>
        <h2 class="page-template-b2b__left__title">szukasz?</h2>
        <label class="screen-reader-text" for="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>">Słowa kluczowe lub frazy</label>
        <div class="woocommerce-product-search__wrap">
            <input type="search" id="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>" class="search-field woocommerce-product-search__wrap__input" placeholder="Np. konopia" value="<?php echo get_search_query(); ?>" name="s" />
        </div>
        <button type="submit button-b button-white">Szukaj</button>
    </form>
    <button class="modal-close-button close-button" data-close aria-label="Close reveal" type="button">
        
    </button>
    </div>
</div>