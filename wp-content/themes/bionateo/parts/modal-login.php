<div class="reveal full" id="login-modal" data-reveal data-animation-in="fade-in" data-animation-out="fade-out">
    <div class="modal__content">
        <div class="modal__login">
            <h3 class="page-template-b2b__left__subtitle">Moje konto</h3>
            <h2 class="page-template-b2b__left__title">zaloguj się</h2>
            <div class="separator"><i class="icon-sign"></i></div>
            <?php if (function_exists("woocommerce_login_form")) {
                woocommerce_login_form();
            }
            ?>
        </div>
    <button class="close-button modal-close-button" data-close aria-label="Close reveal" type="button">
        <span aria-hidden="true"><i class="icon-cross-alt"></i></span>
    </button>
    </div>
</div>