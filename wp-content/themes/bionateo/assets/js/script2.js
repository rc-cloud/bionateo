(function ($) {

    equalheight = function (container) {

        var currentTallest = 0,
                currentRowStart = 0,
                rowDivs = new Array(),
                $el,
                topPosition = 0;
        jQuery(container).each(function () {

            $el = jQuery(this);
            jQuery($el).height('auto')
            topPostion = $el.position().top;

            if (currentRowStart != topPostion) {
                for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                    rowDivs[currentDiv].height(currentTallest);
                }
                rowDivs.length = 0; // empty the array
                currentRowStart = topPostion;
                currentTallest = $el.height();
                rowDivs.push($el);
            } else {
                rowDivs.push($el);
                currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
            }
            for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
        });
    }
    jQuery(window).load(function () {
        equalheight('.eq-height-blog');
    });


    jQuery(window).resize(function () {
        equalheight('.eq-height-blog');
    });


    $(document).ready(function () {
        $('.freshmail-cont input[type="text"], .freshmail-cont input[type="email"]').each(function () {
            $(this).parent().addClass('input-parent');
        });
        $('.freshmail-cont input[type="checkbox"]').parent().addClass('checkbox-parent');

        $('.checkbox-parent span').on('click', function () {
            if ($(this).parent().hasClass('is-active')) {
                $(this).parent().removeClass('is-active');
                $(this).parent().find('input[type="checkbox"]').prop('checked', false);
            } else {
                $(this).parent().addClass('is-active');
                $(this).parent().find('input[type="checkbox"]').prop('checked', true);
            }
        });

        var num = 64; //number of pixels before modifying styles

        $(window).bind('scroll', function () {
            if ($(window).scrollTop() > num) {
                $('.menu-header').addClass('fixed');
            } else {
                $('.menu-header').removeClass('fixed');
            }
        });


    });

    $(window).on("load", function () {
        $(".top-bar .menu-menu").mCustomScrollbar();
    });


})(jQuery);