/*
These functions make sure WordPress
and Foundation play nice together.
*/
var Bionateo = Bionateo || {};
  Bionateo.vars = {
    homeProductSingleClasses : '.page-template-home__products__list__single',
    slickClasses: '.slick',
    slickHeaderClasses: '.slick-header',    
    compareShortDescriptionClasses : '.single--product__description__content',
    compareSizesClasses : '.single--product-list-sizes',
    compareConcentrationClasses : '.single--product-list-concentration',
    compareRemoveButtonClasses : '.single--product__compare-remove__button',
    showFilterClasses : '#product-filters-show',
    burgerClasses : '.burger,.menu-menu__close',
    inputClasses : '.wpcf7-text,.wpcf7-email,.wpcf7-textarea,.search__form__left__field__input,.woocommerce-Input,.product-list-page__sort__wrap__select,.comment-form #comment,.comment-form #author,.comment-form #email,.input-text,.woocommerce-product-search__wrap__input', 
    buyClasses : '.single--product__img__add_to_cart,.single--product__add-to-cart-bottom',
    favClasses : '.single--product__img__add_to_favorities',
    compareClasses : '.single--product__img__compare',
    loopArchiveHeightImgClasses : '.single--product__img,.page-template-home__brands__loop__list__li',
    borderBrandsClasses: '.page-template-home__brands__loop__list__li',
    filtersClass : '.product-list-page__filters__filtr__input',
    filterButtonId : '.product-filters',
    filterSelectClasses : '#filtr_order',
    ajaxUrl : '/wp-admin/admin-ajax.php',
    ajaxType : 'POST',
    ajaxData : 'json',
  }
  Bionateo.functions = {
    body : function(){
      return jQuery('body');
    },
    /* Single - zmiana galerii */
    galleyChangeImg : function(){
      try{
        jQuery('.single-product__top__gallery__thumbnails__single').click(function(){
          var a = jQuery(this);
          var b = a.attr('data-url');
          jQuery('.single-product__top__gallery__img img').attr('src',b);
        });
      }
      catch(e){
        console.error(e);
      }
      
    },
    /* Single - Dopasowanie galerii */
    gallerySize : function(){
      try{
        var body = Bionateo.functions.body();
        if(body.hasClass('single-product')){
          var a = jQuery('.single-product__top__gallery');
          var b = jQuery('.single-product__top__gallery__thumbnails');
          var c = jQuery('.single-product__top__gallery__img');
          var d = a.css('width');
          d = d.replace('px','');
          var e = b.css('width');
          e = e.replace('px','');
          var g = body.css('width');
          g = g.replace('px',''); 
          
          if(g>640){
            var f = d -e-15+"px";
            var h = d -e-15+"px";
          }else{
            var f = 100+"%"
            var h = c.css('width');
            h = h.replace('px','');
          }
          
          c.css({'width':f,'height':h});
          //Bionateo.functions.loopArchiveHeightImgClasses();
        } 
      }
      catch(e){
        console.error(e);
     }
      
    },
    /* Single - Customowy wybór variantów */
    getVariations : function(){
      try{
        jQuery('.get-col').click(function(){
          var a = jQuery(this);
          var b = a.attr('meta-col');
          var c = a.attr('meta-name');
          var d = a.attr('meta-select');
          jQuery('.'+b).removeClass('active');
          a.addClass('active');
          jQuery("#"+d).val(c).change();
          Bionateo.functions.getPrice(false);
        });
      }
      catch(e){
         console.error(e);
      }
      
    },
    /* Single - Kopia ceny i wstawienie do miejsca zgodnie z projetem PSD */
    getPrice : function($time){
      try{
        var body = Bionateo.functions.body();
        if(body.hasClass('single-product')){
          if($time == true){
            setTimeout(function() {
              var a = jQuery('.price_woo_edit');
              var b = jQuery('.single_variation_wrap .woocommerce-Price-amount').html();      
              console.log(b);
              jQuery(a).html(b);
            },2000);
          }else{
            var a = jQuery('.price_woo_edit');
            var b = jQuery('.single_variation_wrap .woocommerce-Price-amount').html();
            jQuery(a).html(b);
          }
        } 
      }
      catch(e){
         console.error(e);
      }
      
    },
    /* SIngle - zwiększanie po mniejszanie ilości sztuk */
    singleQt : function(){
      try{
        var a  = jQuery('.quantity');
        if(a){
            a.addClass('active');
            a.find('.qty').attr('type','text');
           
          jQuery('.add-qt').on('click',function(){
            var b = jQuery(this).prev().val();
            console.log(b);
            b++;
            jQuery(this).prev().val(b);
          });
          jQuery('.remove-qt').on('click',function(){
            var b = jQuery(this).next().val();
            b--;
            if(b > 0 ){
              jQuery(this).next().val(b);
            }
          });
          jQuery('.button-update-cart').removeAttr('disabled');
          jQuery('.button-update-cart,.product-remove__url').click(function(){
            setInterval(function(){ 
              var k  = jQuery('.quantity');
              if(k.hasClass('active')){
                
              }else{
                k.addClass('active');
                k.find('.qty').attr('type','text');
                jQuery('.add-qt').on('click',function(){
                  var b = jQuery(this).prev().val();
                  console.log(b);
                  b++;
                  jQuery(this).prev().val(b);
                });
                jQuery('.remove-qt').on('click',function(){
                  var b = jQuery(this).next().val();
                  b--;
                  if(b > 0 ){
                    jQuery(this).next().val(b);
                  }
                });
                jQuery('.button-update-cart').removeAttr('disabled');
              }
             }, 500);
          })
          
        }
      }
      catch(e){
        console.error(e);
      }
      
    },
    /*  */
    homeProductSingle : function(){
      try{
        jQuery(Bionateo.vars.homeProductSingleClasses).each(function(){
          var a = jQuery(this);
          var b = a.find('.page-template-home__products__list__single__col1');
          var c = a.find('.page-template-home__products__list__single__col2');
          var d = c.css('height');
          d = d.replace('px','');
          b.css('height',d+'px');
        });  
      }
      catch(e){
        console.error(e);
      }
      
    },
    /* Dodanie pustych elementów HOME z logoami producenta */
    borderBrands : function(){
      try{
        var  i =0;
        jQuery(Bionateo.vars.borderBrandsClasses).each(function(){
          i++;
        });
        var c = i/4;
        var d = 4;
        var u = 4;
        if(i%4 != 0){
          for(u;u<=i;u = u +4){
            d = d + 4;
          }
          d = d-i;
          for(var h = 0; h<d;h++){
            jQuery('.page-template-home__brands__loop__list').append('<li class="column column-block page-template-home__brands__loop__list__li custom"></li>');
          }
          Bionateo.functions.loopArchiveHeightImg();
        }
      }
      catch(e){
        console.error(e);
      }
      
    },
    /* Podstrona porównywarki dopasowanie wysokości*/
    compareHeight: function(){
      try{
        var temp0 = 0,temp1 = 0,temp2 = 0;
        jQuery(Bionateo.vars.compareShortDescriptionClasses ).each(function( index ) {
          var a = jQuery(this).css('height');
          a = a.replace('px','');
          if(a > temp0){
            temp0 = a;
          }
        });
        jQuery(Bionateo.vars.compareSizesClasses ).each(function( index ) {
          var a = jQuery(this).css('height');
          a = a.replace('px','');
          if(a > temp1){
            temp1 = a;
          }
        });
        jQuery(Bionateo.vars.compareConcentrationClasses ).each(function( index ) {
          var a = jQuery(this).css('height');
          a = a.replace('px','');
          if(a > temp2){
            temp2 = a;
          }
        });
        jQuery(Bionateo.vars.compareShortDescriptionClasses).css('height',temp0+'px');
        jQuery(Bionateo.vars.compareSizesClasses).css('height',temp1+'px');
        jQuery(Bionateo.vars.compareConcentrationClasses).css('height',temp2+'px');   
      }
      catch(e){
        console.error(e);
      }
           
    },
    /* Pokazywanie ukrywanie filtów mobile shop*/ 
    showHideFilters : function(){
      try{
        jQuery(Bionateo.vars.showFilterClasses).on('click',function(){
          var a = jQuery(this);
          var b = jQuery('.mobile-is');
          if(a.hasClass('open')){
              a.removeClass('open');
              b.removeClass('active');
          }else{
              a.addClass('open');
              b.addClass('active');
          }
        }); 
      }
      catch(e){
        console.error(e);
      }
      
    },
    /* Przesuwanie slidera z cenami na shop */
    noUiSliderInit : function(){
      try{
        var body = Bionateo.functions.body();
        var lower = body.attr('filtr_lower');
        var upper = body.attr('filtr_upper');
        var lower_min = body.attr('filtr_lower_min');
        var upper_max = body.attr('filtr_upper_max');
        var handlesSlider = document.getElementById('priceSlider');
        if(handlesSlider){
          noUiSlider.create(handlesSlider, {
            start: [ lower, upper ],
            range: {
              'min': [ parseFloat(lower_min) ],
              'max': [ parseFloat(upper_max) ]
            }
          });
          var snapValues = [
            document.getElementById('slider-snap-value-lower'),
            document.getElementById('slider-snap-value-upper')
          ];
          handlesSlider.noUiSlider.on('update', function( values, handle ) {
            snapValues[handle].innerHTML = values[handle];
            body.attr('filtr_lower',values[0]);
            body.attr('filtr_upper',values[1]);
          });
        }  
      }
      catch(e){
        console.error(e);
      }
        
        
    },
    /* Dodawanie z url atrybutów do body SHOP */
    filtrBodyData : function(){
      try{
        var a = window.location.href;
        var body = Bionateo.functions.body();
        var url = new URL(a);

        if (url.searchParams) {
            var filtr_product_cat = url.searchParams.get("filtr_product_cat");
            var filtr_brand_cat = url.searchParams.get("filtr_brand_cat");
            var filtr_size_cat = url.searchParams.get("filtr_size_cat");
            var filtr_concentration_cat = url.searchParams.get("filtr_concentration_cat");
            var filtr_order = url.searchParams.get("filtr_order");
            var filtr_upper = url.searchParams.get("filtr_upper");
            var filtr_lower = url.searchParams.get("filtr_lower");
        }

        if(filtr_product_cat){
          body.attr('filtr_product_cat',filtr_product_cat);
        }
        if(filtr_brand_cat){
          body.attr('filtr_brand_cat',filtr_brand_cat);
        }
        if(filtr_size_cat){
          body.attr('filtr_size_cat',filtr_size_cat);
        }
        if(filtr_concentration_cat){
          body.attr('filtr_concentration_cat',filtr_concentration_cat);
        }
        if(filtr_order){
          body.attr('filtr_order',filtr_order);
        }
        if(filtr_upper){
          body.attr('filtr_upper',filtr_upper);
        }
        if(filtr_lower){
          body.attr('filtr_lower',filtr_lower);
        }  
      }
      catch(e){
        console.error(e);
      }
      
    },
    /* Buduj link do filtrowania SHOP */
    filtrFunction : function(){
      try{
        var filet_order_val = jQuery(Bionateo.vars.filterSelectClasses).val();
        var body = Bionateo.functions.body();
        body.attr('filtr_order',filet_order_val);
        var filtr_product_cat = body.attr('filtr_product_cat');
        var filtr_brand_cat = body.attr('filtr_brand_cat');
        var filtr_size_cat = body.attr('filtr_size_cat');
        var filtr_concentration_cat = body.attr('filtr_concentration_cat');
        var filtr_order = body.attr('filtr_order');
        var filtr_lower = body.attr('filtr_lower');
        var filtr_upper = body.attr('filtr_upper');
        var params = jQuery.param({
          filtr_product_cat : filtr_product_cat,
          filtr_brand_cat : filtr_brand_cat,
          filtr_size_cat : filtr_size_cat,
          filtr_concentration_cat : filtr_concentration_cat,
          filtr_order : filtr_order,
          filtr_upper : filtr_upper,
          filtr_lower : filtr_lower,
        });
        
         window.location.href = window.location.pathname+'?'+params;
      }
      catch(e){
        console.error(e);
      }
      
    },
    /* Opdalenie budowy url po kliku w przycisk */
    filtrButton : function(){
      try{
        jQuery(Bionateo.vars.filterButtonId).click(function(){
          Bionateo.functions.filtrFunction();
        });
      }
      catch(e){
        console.error(e);
      }
      
    },
    /* Opdalenie budowy url select w przycisk */
    filtrSelect : function(){
      try{
        jQuery(Bionateo.vars.filterSelectClasses).change(function(){
          Bionateo.functions.filtrFunction();
        });
      }
      catch(e){
        console.error(e);
      }
      
    },
    /* Zanzacanie wybranych atrybutów filtry z lewej SHOP - oraz dodawanie do data danych */
    filtersAddAttr : function(){
      try{
        jQuery(Bionateo.vars.filtersClass).on('click',function(){
          var body = Bionateo.functions.body();
          var a = jQuery(this);
          var b = a.attr('data-name');
          if(a.hasClass('active')){
            a.removeClass('active');
            a.next().removeClass('active');
          }else{
            a.addClass('active');
            a.next().addClass('active');
          }
          var tab = [];
          var i = 0;
          jQuery('.t-'+b).each(function(index){
            var a = jQuery(this);
            if(a.hasClass('active')){
              tab[i] = jQuery(this).attr('value');
              i++;
            }
          });
          body.attr('filtr_'+b,tab);
        });
      }
      catch(e){
        console.error(e);
      }
      
    },
    /* Dopasowanie szerokości szukajki na search */
    searchSize : function(){
      try{
        var a = jQuery('.search__form').css('width');
        if(a){
          a = a.replace('px','');
          var b = jQuery('.search__form__submit').css('width');
          b = b.replace('px','');
          var c = a-b;
          jQuery('.search__form__left').css('width',c+'px');
        }
      }
      catch(e){
        console.error(e);
      }
      
    },
    /* Dopasowanie wysokości do szerokości elementów */
    loopArchiveHeightImg : function(){
      try{
        jQuery( Bionateo.vars.loopArchiveHeightImgClasses ).each(function( index ) {
          var body = Bionateo.functions.body();
          body = body.css('width');
          body = body.replace('px','');
          var a = jQuery(this).css('width');
          a = a.replace('px','');
          if(body < 640){
            a = a/2;
          }
          console.log(a);
          jQuery(this).css('height',a+'px');
        });
      }
      catch(e){
        console.error(e);
      }
      
    },
    /* Menu rozwijanie */
    menu : function(){
      try{
        jQuery(Bionateo.vars.burgerClasses).on('click',function(){
          var a = jQuery('.burger');
          var b = jQuery('.menu-menu');
          var c = jQuery('.menu-menu__close');
          var d = jQuery('.maska');
          if(a.hasClass('open')){
              a.removeClass('open');
              b.removeClass('active');
              c.removeClass('open');
              d.removeClass('active');
          }else{
              a.addClass('open');
              b.addClass('active');
              c.addClass('open');
              d.addClass('active');
          }
        });
        jQuery('.maska').click(function(){
          var a = jQuery('.burger');
          var b = jQuery('.menu-menu');
          var c = jQuery('.menu-menu__close');
          var d = jQuery('.maska');
            if(jQuery(this).hasClass('active')){
              a.removeClass('open');
              b.removeClass('active');
              c.removeClass('open');
              d.removeClass('active');
            }else{
              a.addClass('open');
              b.addClass('active');
              c.addClass('open');
              d.addClass('active');
            }
        });
      }
      catch(e){
        console.error(e);
      }
      
    },
    /* Efekty na inpucie - Bionateo */
    inputEffect : function(){
      try{
        jQuery(Bionateo.vars.inputClasses).on('focusin',function(){
          var a = jQuery(this);
          a.parent().addClass('hideAfter');
        });
        jQuery(Bionateo.vars.inputClasses).on('focusout',function(){
          var a = jQuery(this);
          a.parent().removeClass('hideAfter');
        });
    }
    catch(e){
      console.error(e);
    }
      
    },
    /* Dodanie do koszyka - DO USUNIECIA */
    addToCart : function(){
      try{
        jQuery(Bionateo.vars.buyClasses).click(function() {
          var a = jQuery(this);
          var b = a.attr('data-id');
          var c = a.css('width');
          c = c.replace('px','');
          a.css('width',c+'px');
          a.addClass('spinner'); 
          jQuery.get('/?post_type=product&add-to-cart=' + b, function(data,status) {
            if(status== 'success'){
              a.css('width','');
              a.removeClass('spinner');
            }
          });
        }); 
      }
      catch(e){
        console.error(e);
      }
         
    },
    /* Dodanie do ulubinych */
    addToFav : function(){
      try{
        jQuery(Bionateo.vars.favClasses).click(function(){
          var a = jQuery(this);
          var b = a.attr('data-id');
          var c = a.attr('data-in');
          var d = a.attr('data-user');
          jQuery.ajax({
            type: Bionateo.vars.ajaxType,
            url: Bionateo.vars.ajaxUrl,
            dataType: Bionateo.vars.ajaxData,
            data: ({action:'addToFav',b:b,c:c,d:d}),
            success: function(data) {
              a.attr('data-in',data['c']);
              a.find('.fav_text').text(data['add_text']);
              a.find('.fav_icon .fa').removeClass('fa-heart fa-heart-o')
              a.find('.fav_icon .fa').addClass(data['add_fa']);
            }
          });
        });
      }
      catch(e){
        console.error(e);
      }
      
    },
    /* Usuwanie z porównywarki */
    removeFromCompare : function(){
      try{
        jQuery(Bionateo.vars.compareRemoveButtonClasses).click(function(){
          console.log('a');
          var a = jQuery(this);
          var b = a.attr('data-id');
          var c = a.attr('data-user');
          jQuery.ajax({
            type: Bionateo.vars.ajaxType,
            url: Bionateo.vars.ajaxUrl,
            dataType: Bionateo.vars.ajaxData,
            data: ({action:'removeFromCompare',b:b,c:c}),
            success: function(data) {
              console.log(data);
              if(data['success']){
                jQuery('#post-'+b).remove();
              }
            }
          });
        });
      }
      catch(e){
        console.error(e);
      }
      
    },
    /* Dodawanie do porównywarki */
    addToCompare : function(){
      try{
        jQuery(Bionateo.vars.compareClasses).click(function(){
          var a = jQuery(this);
          var b = a.attr('data-id');
          var c = a.attr('data-user');
          jQuery.ajax({
            type: Bionateo.vars.ajaxType,
            url: Bionateo.vars.ajaxUrl,
            dataType: Bionateo.vars.ajaxData,
            data: ({action:'addToCompare',b:b,c:c}),
            success: function(data) {
              if(data['success']){
                window.location.replace(data['url']);
              }
            }
          });
        });
      }
      catch(e){
        console.error(e);
      }
      
    },
    /* Slidery */
    slick : function(){
      try{
        jQuery(Bionateo.vars.slickClasses).on('init', function(event, slick){
          Bionateo.functions.loopArchiveHeightImg();
        });
        jQuery(Bionateo.vars.slickClasses).slick({
          dots: true,
          infinite: false, 
          speed: 300,
          slidesToShow: 3,
          slidesToScroll: 3,
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                dots: true
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
        });
        jQuery(Bionateo.vars.slickHeaderClasses).slick({
          dots: true,
          infinite: false, 
          speed: 300,
          slidesToShow: 1,
          slidesToScroll: 1,
        });
      }
      catch(e){
        console.error(e);
      }
      
    },
    cookies : function(){
      try{
        function NoweCookie(e, i, t) {
          var o = new Date;
          o.setTime(o.getTime() + 24 * t * 60 * 60 * 1e3);
          var s = "; expires=" + o.toGMTString();
          document.cookie = e + "=" + i + s + "; path=/"
      }
      
      function CzytajCookie(e) {
          for (var i = e + "=", t = document.cookie.split(";"), o = 0; o < t.length; o++) {
              for (var s = t[o];
                  " " == s.charAt(0);) s = s.substring(1, s.length);
              if (0 == s.indexOf(i)) return s.substring(i.length, s.length)
          }
          return null
      }
      
      function SprawdzCookie() {
          if ("T" != CzytajCookie("cookies_ok")) {
              var e = document.createElement("div");
              e.id = "cookies-wiadomosc-box";
              var i = '<div id="cookies-wiadomosc" >Witryna korzysta z plików cookies w celach statystycznych i reklamowych oraz w celu dostosowania serwisu do indywidualnych potrzeb klienta. Więcej informacji w <a href="/polityka-prywatnosci">POLITYCE PRYWATNOŚCI</a>. Każdy użytkownik ma możliwość zmiany ustawień plików cookies, w tym wyłączenia plików cookies w ustawieniach swojej przeglądarki. Korzystanie z witryny bez zmiany ustawień dotyczących cookies oznacza, że będą one zapisane w pamięci urządzenia. <a id="acceptcheck" class="button-b" name="accept">Akceptuję</a></div>';
              e.innerHTML = i, document.body.appendChild(e)
          }
      }
      jQuery('body').on('click','#acceptcheck',function(){
        console.log('a');
        NoweCookie("cookies_ok", "T", 365), document.getElementById("cookies-wiadomosc-box").removeChild(document.getElementById("cookies-wiadomosc"))
      });
      function CloseCookie() {
          NoweCookie("cookies_ok", "T", 365), document.getElementById("cookies-wiadomosc-box").removeChild(document.getElementById("cookies-wiadomosc"))
      }
      window.onload = SprawdzCookie
      }
      catch(e){
        console.error(e);
      }
    },
    height : function(){
      var temp1 = 0;
      jQuery('.customHeight').each(function(){
        var a = jQuery(this).css('height');
        a = a.replace('px','');
        if(a > temp1){
          temp1 = a;
        }
        console.log(temp1);
      });
      jQuery('.customHeight').css('height',temp1+"px");
    },
    scroll : function(){
      jQuery("#scrollToTop").click(function() {
        jQuery("html, body").animate({ scrollTop: 0 }, 500);
        return false;
      });
    }
  }



jQuery(window).resize(function(){
  Bionateo.functions.searchSize();
  Bionateo.functions.loopArchiveHeightImg();
  Bionateo.functions.borderBrands();
  Bionateo.functions.homeProductSingle();
  Bionateo.functions.gallerySize();
  Bionateo.functions.height();
});
jQuery(document).ready(function(){
  Bionateo.functions.searchSize();
  Bionateo.functions.loopArchiveHeightImg();
  Bionateo.functions.menu();
  Bionateo.functions.inputEffect();
  //Bionateo.functions.addToCart();
  Bionateo.functions.addToFav();
  Bionateo.functions.addToCompare();
  Bionateo.functions.filtrBodyData();
  Bionateo.functions.filtersAddAttr(); 
  Bionateo.functions.filtrButton();
  Bionateo.functions.noUiSliderInit();
  Bionateo.functions.filtrSelect();
  Bionateo.functions.showHideFilters();
  Bionateo.functions.compareHeight();
  Bionateo.functions.removeFromCompare();
  Bionateo.functions.slick();
  Bionateo.functions.borderBrands();
  Bionateo.functions.homeProductSingle();
  Bionateo.functions.singleQt();
  Bionateo.functions.getPrice(true);
  Bionateo.functions.getVariations();
  Bionateo.functions.gallerySize();
  Bionateo.functions.galleyChangeImg();
  Bionateo.functions.cookies();
  Bionateo.functions.height();
  Bionateo.functions.scroll();
});


		




