<?php

add_action( 'widgets_init', 'custom_widget_init' );

function custom_widget_init() {
    register_widget( 'company_widget' );
}

class company_widget extends WP_Widget {


// constructor

                function __construct () {
                	 parent::__construct('company_widget', 'Informacje kontaktowe' );
                }

                // widget form creation

                function form($instance) {
                	if ( isset( $instance[ 'name' ]) && isset ($instance[ 'ulica' ]) && isset($instance[ 'kod_pocztowy' ])
                	&& isset($instance[ 'miasto' ])	&& isset($instance[ 'telefon' ])	&& isset($instance[ 'mail' ]) ) {
						$name = $instance[ 'name' ];
						$ulica = $instance[ 'ulica' ];
						$kod_pocztowy = $instance[ 'kod_pocztowy' ];
						$miasto = $instance[ 'miasto' ];
						$telefon = $instance[ 'telefon' ];
						$mail = $instance[ 'mail' ];
					} else {
						$name 				= '';
						$kod_pocztowy 		= 'Kod pocztowy';
						$miasto 			= 'Miasto';
						$ulica 				= 'Ulica';
						$telefon 			= '';
						$mail 				= '';
					} ?>
						<p>
							<label for="company_name">Nazwa firmy:</label>
							<input name="<?php echo $this->get_field_name( 'name' ); ?>" id='company_name' class='widefat' type="text" value="<?php echo esc_attr( $name );?>" />
						</p>

						<p>
						<label>Adres firmy:</label> <br>
						<input name="<?php echo $this->get_field_name( 'ulica' ); ?>" type="text" size="14" value="<?php echo esc_attr( $ulica ); ?>" />
						<input name="<?php echo $this->get_field_name( 'kod_pocztowy' ); ?>" type="text" size="14" value="<?php echo esc_attr( $kod_pocztowy ); ?>" />
						<input name="<?php echo $this->get_field_name( 'miasto' ); ?>" type="text" size="14" value="<?php echo esc_attr( $miasto ); ?>" />
						</p>
						<p>
							<label for="company_tel">Telefon:</label>
							<input name="<?php echo $this->get_field_name( 'telefon' ); ?>" id='company_tel' class='widefat' type="text" value="<?php echo esc_attr( $telefon );?>" />
						</p>
						<p>
							<label for="company_mail">Mail:</label>
							<input name="<?php echo $this->get_field_name( 'mail' ); ?>" id='company_mail' class='widefat' type="text" value="<?php echo esc_attr( $mail );?>" />
						</p>
                <?php }

                // widget update

                function update($new_instance, $old_instance) {

					$instance = $old_instance;
					$instance['name'] = ( ! empty( $new_instance['name'] ) ) ? strip_tags( $new_instance['name'] ) : '';
					$instance['ulica'] = ( ! empty( $new_instance['ulica'] ) ) ? strip_tags( $new_instance['ulica'] ) : '';
					$instance['kod_pocztowy'] = ( ! empty( $new_instance['kod_pocztowy'] ) ) ? strip_tags( $new_instance['kod_pocztowy'] ) : '';
					$instance['miasto'] = ( ! empty( $new_instance['miasto'] ) ) ? strip_tags( $new_instance['miasto'] ) : '';
					$instance['telefon'] = ( ! empty( $new_instance['telefon'] ) ) ? strip_tags( $new_instance['telefon'] ) : '';
					$instance['mail'] = ( ! empty( $new_instance['mail'] ) ) ? strip_tags( $new_instance['mail'] ) : '';
					return $instance;

                }

                // widget display

                function widget($args, $instance) {

                	extract($args);
					echo $before_widget; //Widget starts to print information
					$name = apply_filters( 'widget_title', $instance['name'] );
					$domain = empty( $instance['domain'] ) ? '&nbsp;' : $instance['domain'];
					$designation = empty( $instance['designation'] ) ? '&nbsp;' : $instance['designation'];

					$name 			= empty( $instance[ 'name' ] ) ? '&nbsp;' : $instance[ 'name' ];
					$ulica 			= empty( $instance['ulica'] ) ? '&nbsp;' : $instance['ulica'];
					$kod_pocztowy 	= empty( $instance['kod_pocztowy'] ) ? '&nbsp;' : $instance['kod_pocztowy'];
					$miasto 		= empty( $instance['miasto'] ) ? '&nbsp;' : $instance['miasto'];
					$telefon 		= empty( $instance['telefon'] ) ? '&nbsp;' : $instance['telefon'];
					$mail 			= empty( $instance['mail'] ) ? '&nbsp;' : $instance['mail'];


					echo "<div class='widget-company' itemscope itemtype=\"http://schema.org/Organization\">";
					echo "<span class='widget-company-name' itemprop=\"name\">".$name."</span>";
					echo "<div class='widget-company-adress' itemprop=\"address\" itemscope itemtype=\"http://schema.org/PostalAddress\">";
					echo "<span class='widget-company-street' itemprop=\"streetAddress\">".$ulica."</span>";
					echo "<span class='widget-company-code' itemprop=\"postalCode\">".$kod_pocztowy."</span>";
					echo "<span class='widget-company-city' itemprop=\"addressLocality\">".$miasto."</span></div>";
					if (!empty($telefon)) echo "<span class='widget-company-phone' itemprop=\"telephone\">".$telefon."</span>";
					if (!empty($mail)) echo "<span class='widget-company-mail' itemprop=\"email\">".$mail."</span></div>";
					echo $after_widget; //Widget ends printing information

                }
}