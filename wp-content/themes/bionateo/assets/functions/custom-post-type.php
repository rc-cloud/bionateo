<?php
	register_taxonomy( 'size_cat', 
		array('product'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
		array('hierarchical' => true,     /* if this is true, it acts like categories */             
			'labels' => array(
				'name' => __( 'Rozmiar', 'jointswp' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Rozmiar', 'jointswp' ), /* single taxonomy name */
				'search_items' =>  __( 'Szukaj rozmiarów', 'jointswp' ), /* search title for taxomony */
				'all_items' => __( 'Wszystkie rozmiary', 'jointswp' ), /* all title for taxonomies */
				'parent_item' => __( 'Rodzic', 'jointswp' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Rodzic:', 'jointswp' ), /* parent taxonomy title */
				'edit_item' => __( 'Edytuj', 'jointswp' ), /* edit custom taxonomy title */
				'update_item' => __( 'Update', 'jointswp' ), /* update title for taxonomy */
				'add_new_item' => __( 'Dodaj nowy', 'jointswp' ), /* add new title for taxonomy */
				'new_item_name' => __( 'New Custom Category Name', 'jointswp' ) /* name title for taxonomy */
			),
			'show_admin_column' => true, 
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'rozmiar' ),
		)
	);   
	register_taxonomy( 'concentration_cat', 
	array('product'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
	array('hierarchical' => true,     /* if this is true, it acts like categories */             
		'labels' => array(
			'name' => __( 'Stężenie', 'jointswp' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Stężenie', 'jointswp' ), /* single taxonomy name */
			'search_items' =>  __( 'Szukaj Stężeń', 'jointswp' ), /* search title for taxomony */
			'all_items' => __( 'Wszystkie Stężenia', 'jointswp' ), /* all title for taxonomies */
			'parent_item' => __( 'Rodzic', 'jointswp' ), /* parent title for taxonomy */
			'parent_item_colon' => __( 'Rodzic:', 'jointswp' ), /* parent taxonomy title */
			'edit_item' => __( 'Edytuj', 'jointswp' ), /* edit custom taxonomy title */
			'update_item' => __( 'Update', 'jointswp' ), /* update title for taxonomy */
			'add_new_item' => __( 'Dodaj nowy', 'jointswp' ), /* add new title for taxonomy */
			'new_item_name' => __( 'New Custom Category Name', 'jointswp' ) /* name title for taxonomy */
		),
		'show_admin_column' => true, 
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'stezenie' ),
	)
);  
register_taxonomy( 'brand_cat', 
array('product'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
array('hierarchical' => true,     /* if this is true, it acts like categories */             
	'labels' => array(
		'name' => __( 'Marka', 'jointswp' ), /* name of the custom taxonomy */
		'singular_name' => __( 'Marka', 'jointswp' ), /* single taxonomy name */
		'search_items' =>  __( 'Szukaj Marek', 'jointswp' ), /* search title for taxomony */
		'all_items' => __( 'Wszystkie Marki', 'jointswp' ), /* all title for taxonomies */
		'parent_item' => __( 'Rodzic', 'jointswp' ), /* parent title for taxonomy */
		'parent_item_colon' => __( 'Rodzic:', 'jointswp' ), /* parent taxonomy title */
		'edit_item' => __( 'Edytuj', 'jointswp' ), /* edit custom taxonomy title */
		'update_item' => __( 'Update', 'jointswp' ), /* update title for taxonomy */
		'add_new_item' => __( 'Dodaj nowy', 'jointswp' ), /* add new title for taxonomy */
		'new_item_name' => __( 'New Custom Category Name', 'jointswp' ) /* name title for taxonomy */
	),
	'show_admin_column' => true, 
	'show_ui' => true,
	'query_var' => true,
	'rewrite' => array( 'slug' => 'marka' ),
)
);  