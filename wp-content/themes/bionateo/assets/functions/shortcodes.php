<?php 

function main_paragraph_shortcode( $atts, $content = null ) {
	return '<div class="main_paragraph">' . $content . '</div>';
}
add_shortcode( 'main_paragraph', 'main_paragraph_shortcode' );

function row_shortcode( $atts, $content = null ) {
	return '<div class="row expanded">' . do_shortcode($content) . '</div>';
}
add_shortcode( 'row', 'row_shortcode' );

function col_4_shortcode( $atts, $content = null ) {
	return '<div class="large-4 columns">' . do_shortcode($content) . '</div>';
}
add_shortcode( 'col_4', 'col_4_shortcode' );
function col_8_shortcode( $atts, $content = null ) {
	return '<div class="large-8 columns">' . do_shortcode($content) . '</div>';
}
add_shortcode( 'col_8', 'col_8_shortcode' );


function heading_1_subtitle_shortcode( $atts, $content = null ) {
    $a = shortcode_atts( array(
        'subtitle' => '',
        'title' => '',
    ), $atts );
    $html = '<div class="heading_1_subtitle"><h2 class="heading_1_subtitle__subtitle">'.$a['subtitle'].'</h2><h1 class="heading_1_subtitle__title">'.$a['title'].'</h1></div>';
    return $html;
}
add_shortcode( 'heading_1_subtitle', 'heading_1_subtitle_shortcode' );

function heading_2_subtitle_shortcode( $atts, $content = null ) {
    $a = shortcode_atts( array(
        'subtitle' => '',
        'title' => '',
    ), $atts );
    $html = '<div class="heading_2_subtitle"><h2 class="heading_2_subtitle__subtitle">'.$a['subtitle'].'</h2><h1 class="heading_2_subtitle__title">'.$a['title'].'</h1></div>';
    return $html;
}
add_shortcode( 'heading_2_subtitle', 'heading_2_subtitle_shortcode' );