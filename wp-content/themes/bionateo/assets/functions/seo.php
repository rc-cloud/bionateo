<?php


add_action('wp_head', 'disable_index_for_tenseo');
if (!detect_seo_plugins() ) add_action('wp_head', 'enable_seo');


function detect_seo_plugins() {

	return detect_plugin(
		// Use this filter to adjust plugin tests.
		apply_filters(
			'detect_seo_plugins',
			//* Add to this array to add new plugin checks.
			array(

				// Classes to detect.
				'classes' => array(
					'All_in_One_SEO_Pack',
					'All_in_One_SEO_Pack_p',
					'HeadSpace_Plugin',
					'Platinum_SEO_Pack',
					'wpSEO',
					'SEO_Ultimate',
				),

				// Functions to detect.
				'functions' => array(),

				// Constants to detect.
				'constants' => array( 'WPSEO_VERSION', ),
			)
		)
	);

}

function detect_plugin( array $plugins ) {

	//* Check for classes
	if ( isset( $plugins['classes'] ) ) {
		foreach ( $plugins['classes'] as $name ) {
			if ( class_exists( $name ) )
				return true;
		}
	}

	//* Check for functions
	if ( isset( $plugins['functions'] ) ) {
		foreach ( $plugins['functions'] as $name ) {
			if ( function_exists( $name ) )
				return true;
		}
	}

	//* Check for constants
	if ( isset( $plugins['constants'] ) ) {
		foreach ( $plugins['constants'] as $name ) {
			if ( defined( $name ) )
				return true;
		}
	}

	//* No class, function or constant found to exist
	return false;

}

function enable_seo() {
	//When any type of Archive page is being displayed. Category, Tag, other Taxonomy Term, custom post type archive, Author and Date-based pages are all types of Archives.
	//https://codex.wordpress.org/Conditional_Tags#Any_Archive_Page
	if (is_archive()) {
		echo "<meta name='robots' content='noindex,nofollow,noodp'>";
	}
	//When a search result page archive is being displayed.
	//https://codex.wordpress.org/Conditional_Tags#A_Search_Result_Page
	if (is_search()) {
		echo "<meta name='robots' content='noindex,nofollow,noodp'>";
	}
}

function disable_index_for_tenseo(){
	if (strpos($_SERVER['SERVER_NAME'], 'tenseo')) echo "<meta name='robots' content='noindex,nofollow,noodp'>";
}




function addSitemap($inputString =''){
	if (!empty($input)) $actualRobotsContent = '';
	else $actualRobotsContent = $inputString;

	if(class_exists('WPSEO_Options')){
		if (!(strpos($inputString, 'Sitemap') !== false)) {
			$protocol = isset($_SERVER["HTTPS"]) ? 'https://' : 'http://';
			$actualRobotsContent .="

Sitemap: ".$protocol.$_SERVER['SERVER_NAME']."/sitemap_index.xml";
		}
}
return $actualRobotsContent;
}


//Generowanie odpowiedniego pliku robots.txt
if (strpos($_SERVER['SERVER_NAME'], 'tenseo')){
if (file_exists(ABSPATH.'robots.txt')) {
	//Plik istnieje, można go edytować
	$actualRobotsContent = file_get_contents(ABSPATH.'robots.txt');
	file_put_contents(ABSPATH.'/robots.txt', addSitemap($actualRobotsContent));
   }else{
	//Jeżeli nie ma pliku robots.txt
	$actualRobotsContent = 'User-agent: *
';
	$actualRobotsContent .= 'Disallow: /cgi-bin
Disallow: /wp-admin
Disallow: /wp-includes
Disallow: /wp-content/plugins
Disallow: /wp-content/cache
Disallow: /trackback
Disallow: /feed
Disallow: /comments
Disallow: */trackback
Disallow: */feed
Allow: /wp-content/uploads';
	file_put_contents(ABSPATH.'/robots.txt', addSitemap($actualRobotsContent));
}
}